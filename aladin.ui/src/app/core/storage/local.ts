import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { CartService } from '..';
@Injectable({
  providedIn: 'root'
})
export class LocalService {

  activatecart=new Subject<boolean>();
  constructor(private cartInfo: CartService) { 

  }
  items = (() => {
    const fieldValue = localStorage.getItem('designs');
    return fieldValue === null
      ? []
      : JSON.parse(fieldValue);
  })();
  
  cart_items = (() => {
    const fieldValue = localStorage.getItem('cart');
    return fieldValue === null
      ? []
      : JSON.parse(fieldValue);
  })();

  save () {
    localStorage.setItem("designs",JSON.stringify(this.items));
  }

  add(dataset:any) {
    try{
      this.items.push(dataset);
      this.save();
      this.cartInfo.dignUpdated.next(this.items.length)
    }catch(e:any){
      console.log(e)
    }
  }

  adtocart(data:any){
    this.cart_items.push(data);
    try{
      this.savec();
      this.cartInfo.cartUpdated.next(this.cart_items.length)
    }catch(e:any){
      console.log(e)
    }
  }

  savec(){
    localStorage.setItem("cart",JSON.stringify(this.cart_items));
    

  }

  removeItem(id:any){
    try{
      this.cart_items.splice(+id,1);
      this.savec();
      console.log(this.cart_items.length)
      this.cartInfo.cartUpdated.next(this.cart_items.length)
    }catch(e:any){
      console.log(e)
    }
  }

update(data:any){
  this.cart_items.push(data)
  this.savec()
}

removeOne(id:any){
  try{
    this.items.splice(+id,1);
    this.save();
    this.cartInfo.dignUpdated.next(this.items.length)
  }catch(e:any){
    console.log(e)
  }
}

deleteAll(key:any){
  localStorage.removeItem(key);

}
}
