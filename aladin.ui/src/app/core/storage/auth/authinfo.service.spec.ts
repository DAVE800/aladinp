import { TestBed } from '@angular/core/testing';

import { AuthinfoService } from './authinfo.service';

describe('AuthinfoService', () => {
  let service: AuthinfoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuthinfoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
