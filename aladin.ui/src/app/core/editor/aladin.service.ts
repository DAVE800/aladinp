import { Injectable } from '@angular/core';
import { fabric } from 'fabric';
import { Observable,Subject } from 'rxjs';
declare var FontFace:any;
declare var require: any
var myalert=require('sweetalert2')

@Injectable({
  providedIn: 'root'
})
export class AladinService {

subject= new Subject<boolean>();
changeproduct=new Subject<boolean>();
changeItem=new Subject <any>();
addedText=new Subject<any>();
ShowEditor=new Subject<any>();
faceupdated=new Subject<any>();
  constructor() { }

  makeImage=(url:any,canvas:any)=>{
    var img = new Image();
    img.src=url
    img.crossOrigin ='anonymous';
    img.onload = ()=> {
    var Img = new fabric.Image(img);
    canvas.setBackgroundImage(Img,canvas.renderAll.bind(canvas), {
            backgroundImageOpacity: 0.1,
            backgroundImageStretch: true,
            left:0,
            top:5,
            right:5,
            bottom:0,    
            scaleX: canvas.width / img.width,
            scaleY: canvas.height /img.height
            
        });
        this.subject.next(true)
        canvas.centerObject(Img);
        canvas.renderAll(Img);
        canvas.requestRenderAll(); 
        
      }
  }
  
  addText=(canvas:any,message:any=null)=>{
    if(message){
      let text= new fabric.IText(message,{
        top:200,
        left:200,
        fill:"blue",
        fontSize:38,
        fontStyle:'normal',
        cornerStyle:'circle',
        selectable:true,
        borderScaleFactor:1,
        overline:false,
        lineHeight:5
      });
      
      canvas.add(text).setActiveObject(text);
      canvas.renderAll(text);
      canvas.requestRenderAll();
      canvas.centerObject(text);
    }else{
      let text= new fabric.IText("TEXTE",{
        top:200,
        left:200,
        fill:"blue",
        fontSize:38,
        fontStyle:'normal',
        cornerStyle:'circle',
        selectable:true,
        borderScaleFactor:1,
        overline:false,
        lineHeight:1.5
      });
      
      canvas.add(text).setActiveObject(text);
      canvas.renderAll(text);
      canvas.requestRenderAll();
      canvas.centerObject(text);
    }
  
    
    

  }

  overline=(canvas:any)=>{
    let item = canvas.getActiveObject()
    if(item.type=="activeSelection"){
      for(let i of item){
        if(!i.overline){
          i.set({overline:true})
          canvas.renderAll(i);
          canvas.requestRenderAll()
        }else{
          i.set({overline:false});
          canvas.renderAll(i);
          canvas.requestRenderAll()
        }
      }
    }
    
    if(item.type!="activeSelection"){
        if(!item.overline){
          item.set({overline:true});
          canvas.renderAll(item);
          canvas.requestRenderAll()
        }else{
          item.set({overline:false});
          canvas.renderAll(item);
          canvas.requestRenderAll()
        }
      
    }
  }


  underline=(canvas:any)=>{
    let item=canvas.getActiveObject()
    if(item && item.type!="activeSelection"){
      if(!item.underline){
        item.set({underline:true});
        canvas.renderAll(item);
        
  
      }else{
        item.set({underline:false});
        canvas.renderAll(item);
        canvas.requestRenderAll();

    
      }
    }
  
    if(item && item.type=="activeSelection"){
  
      for(let el of item._objects){
        if(!item.underline){
  
          el.set({underline:true});
          canvas.renderAll(el);
  
        }else{
          el.set({underline:false});
          canvas.renderAll(el);

        }
      }
      canvas.requestRenderAll();

    }
  }


  italic=(canvas:any)=>{
    let item= canvas.getActiveObject();
    if(item!=undefined && item.type=="activeSelection"){
      for(let el of item._objects){
        if(el.fontStyle!="normal"){
          el.set({fontStyle:'normal'});
          canvas.renderAll(el)
         
        }else{
          el.set({fontStyle:'italic'});
          canvas.renderAll(el);
        }
      }
      canvas.requestRenderAll();

      
    }
    if(item && item!="activeSelection"){
      if(item.fontStyle!="normal"){
        item.set({fontStyle:"normal"});
        canvas.renderAll(item);
      }else{
        item.set({
          fontStyle:"italic"
        });
      canvas.renderAll(item);
      canvas.requestRenderAll();

      }
    }
  }


  bold=(canvas:any)=>{
    let item= canvas.getActiveObject();
  if(item!=undefined && item.type=="activeSelection"){    
      for(let el of item._objects){
        if(el.fontWeight=="normal"){
        el.set({fontWeight:'bold'});
        canvas.renderAll(el);
      }else{
          el.set({fontWeight:'normal'});
          canvas.renderAll(el);  

      }
      canvas.requestRenderAll();  

    }
  }
    
  if(item!=undefined && item.type!="activeSelection"){
    if(item.fontWeight=="normal"){   
      item.set({fontWeight:'bold'});
        canvas.renderAll(item);
        canvas.requestRenderAll();
     
    }else{
      item.set({fontWeight:'normal'});
        canvas.renderAll(item);
        canvas.requestRenderAll();   
    }

  }
  }

  remove=(canvas:any)=>{
    let item = canvas.getActiveObject();
    if(item && item.type!="activeSelection"){
      canvas.remove(item);
    } 
    if(item && item.type=="activeSelection"){
      for(let e of item._objects){
      canvas.remove(e);
      }
    }
  }




  textbefore=(canvas:any)=>{
    let text = canvas.getActiveObject()
    if(text && text.type!="activeSelection"){ 
      if(text.underline && text._textBeforeEdit){
        text.set({text:text._textBeforeEdit,underline:false});
        canvas.renderAll(text);
  
      }   
      if(text._textBeforeEdit){
        text.set({text:text._textBeforeEdit});
        canvas.renderAll(text);

  
      }
      canvas.requestRenderAll();


    } 
  
    if(text && text.type=="activeSelection"){
      for(let el of text._objects){
        if(el.underline && el._textBeforeEdit){
          el.set({text:el._textBeforeEdit,underline:false});
          canvas.renderAll(el);
  
        }   
        if(el._textBeforeEdit){
          el.set({text:el._textBeforeEdit});
          canvas.renderAll(el);

        }
  
      }
      canvas.requestRenderAll();

    }
  }


  textfont=(event:any,canvas:any)=>{
 const myfont :any= new FontFace(event.name,`url(${event.url})`);
  myfont.load().then((loaded:any)=>{
    (document as any).fonts.add(loaded)
    let item= canvas.getActiveObject();

    if( item &&  item.type=="activeSelection"){
      for(let el of  item._objects){
        el.set({
          fontFamily:event.name
        });
        canvas.renderAll(el);
         canvas.requestRenderAll();
  
      }
  
    }
  
    if( item &&  item.type!="activeSelection"){
      item.set({
        fontFamily:event.name
      });
      canvas.renderAll(item);
      canvas.requestRenderAll();
    }

  }).catch((err:any)=>{
    console.log(err);
    
  })
  }


  textwidth=(event:any,canvas:any)=>{
    let text_w =document.getElementById("R")
    let item = canvas.getActiveObject()
         item.set({width:+event,fontSize:+event});
          canvas.renderAll(item);
          canvas.requestRenderAll();
  
    text_w?.addEventListener("input",()=>{
      if(item){
        if(item.type!="activeSelection"){
          item.set({width:+event,fontSize:+event});
          canvas.renderAll(item);
          canvas.requestRenderAll();
        }
      }
  });
  }

  minusOrplus=(event:any,canvas:any)=>{
    let item = canvas.getActiveObject()
    item.set({width:+event,fontSize:+event});
     canvas.renderAll(item);
     canvas.requestRenderAll();
  }

  rightclick=(canvas:any,element:any)=>{
    let item = canvas.getActiveObject();
    if(item){
     this.triggerMouse(element)   
    }  
  }


  triggerMouse(event:any){
    var evt = new MouseEvent("click", {
      view: window,
      bubbles: true,
      cancelable: true,
  });
  event.dispatchEvent(evt);
  
  }

  set=(canvas:any,url:any,data:any)=>{
    console.log(url)
      var img = new Image();
      img.src=url
      img.crossOrigin ='anonymous';   
      img.onload = ()=> {
      var Img = new fabric.Image(img);
      canvas.setBackgroundImage(Img,canvas.renderAll.bind(canvas), {
              backgroundImageOpacity: 0.1,
              backgroundImageStretch: true,
              left:0,
              top:5,
              right:5,
              bottom:0,
              scaleX:canvas.width/img.width,     
              scaleY:canvas.height/img.height
            
          });
          canvas.centerObject(Img);
          canvas.renderAll(Img);
          canvas.requestRenderAll();
          this.faceupdated.next({face1:data.url,face2:data.back}) 
         // this.changeproduct.next(true)

      }

  
    }
  

    handleChanges(file:any):Boolean {
      var fileTypes = [".png"];
      var filePath = file.name;
      if (filePath) {
                   var filePic = file; 
                   var fileType = filePath.slice(filePath.indexOf(".")); 
                   var fileSize = file.size; //Select the file size
                   if (fileTypes.indexOf(fileType) == -1) { 
                           myalert.fire({
                            title: "Erreur",
                            text: `le format ${fileType} n'est pas autorisé utilisez le format .png`,
                            icon: "error",
                             button: "Ok"
                            });
                        return true;
                   }
    
          if (+fileSize > 200*200) {
            myalert.fire({
              title: "Erreur",
              text: `importer un fichier de taille ${200}x${200} MG!`,
              icon: "error",
               button: "Ok"
              });
              return true;
          }
    
          var reader = new FileReader();
          reader.readAsDataURL(filePic);
          reader.onload =  (e:any) =>{
              var data = e.target.result;
                           
              var image = new Image();
              image.onload = ():any =>{
                  var width = image.width;
                  var height = image.height;
                  if(width == 200 && height == 200) {                        
                    return false ;
                     
                  } else {
                       myalert.fire({
                        title: "Erreur",
                        text: `la taille doit etre ${200}x${200}`,
                        icon: "error",
                         button: "Ok"
                        });
                      return true ;
                  }
    
    
              };
            };
      } else {
    
          return true;
          
      }
    
      return false;
    }

    shadow=($event:any,canvas:any)=>{
      let shadow = new fabric.Shadow({
        color: $event,
        blur: 20,
        offsetX: -4,
        offsetY: 3
      });
    
      let item =  canvas.getActiveObject();
      if(item && item.type!="activeSelection"){
        item.set({shadow:shadow});
        canvas.renderAll(item);
        canvas.requestRenderAll();

      }
      if(item && item.type=="activeSelection"){
        for(let el of item._objects){
          el.set({shadow:shadow});
          canvas.renderAll(el);
        }
        canvas.requestRenderAll();

      }
    }


    stroke=($event:any,canvas:any)=>{
      let color=$event.color;
      console.log(color);
      console.log($event.width);
      let item =  canvas.getActiveObject();
      if(item && item.type!="activeSelection"){
        item.set({strokeWidth:1,stroke:color});
        canvas.renderAll(item);
        canvas.requestRenderAll();

      }
      if(item && item.type=="activeSelection"){
        for(let el of item._objects){
          el.set({strokeWidth:1,stroke:color});
          canvas.renderAll(el);

        }
        canvas.requestRenderAll();

      }
    }


    textcolor=(color:any,canvas:any)=>{
      let item= canvas.getActiveObject();
      if(item && item.type!="activeSelection"){
        item.set({fill:color});
        canvas.renderAll(item);
        canvas.requestRenderAll();

      }
   
      if(item && item.type=="activeSelection"){
        for(let el of item._objects){
          el.set({fill:color});
          canvas.renderAll(el);
        }
        canvas.requestRenderAll();

       }
    }


    itemcolor=(color:any,canvas:any)=>{
      let imgInstance=canvas.getActiveObject()
      if(canvas.getActiveObject()._originalElement.localName==="img"){
        canvas.getActiveObject()._originalElement.setAttribute('width',200)
        canvas.getActiveObject()._originalElement.setAttribute('height',200)
        let   filter = new fabric.Image.filters.BlendColor({
          color: color,
         });
       filter.setOptions({
      mode:"tint",
      alpha:0.5,
      scaleX:0.5,     
      scaleY:0.5,
     

    
       })
        imgInstance.filters.push(filter);
        imgInstance.applyFilters();
        canvas.add(imgInstance).setActiveObject(imgInstance);
        canvas.centerObject(imgInstance);
        canvas.renderAll(imgInstance);
          
      }
    }


    setitem=(event:any,canvas:any)=>{
      var img = event.target;
      img.setAttribute("crossOrigin",'anonymous')   
      fabric.Image.fromURL(img.src,(img:any) => {
        this.changeItem.next(true)
       canvas.add(img).setActiveObject(img);
      canvas.centerObject(img);
      canvas.renderAll(img);
      },
      {
         scaleX:0.5,     
         scaleY:0.5,
         crossOrigin: "Anonymous",
      
         
      }
      );
      
    }


    copy=(canvas:any)=>{
      canvas.getActiveObject().clone(function(cloned:any) {
        canvas._clipboard= cloned;
      });
    }

    paste=(canvas:any)=>{
      canvas._clipboard.clone(function(clonedObj:any) {
        canvas.discardActiveObject();
        clonedObj.set({
          left: clonedObj.left + 15,
          top: clonedObj.top + 15,
          evented: true,
        });
        if (clonedObj.type === 'activeSelection') {
          clonedObj.canvas = canvas;
          clonedObj.forEachObject(function(obj:any) {
            canvas.add(obj);
          });
          clonedObj.setCoords();
        } else {
          canvas.add(clonedObj);
        }
        canvas._clipboard.top += 15;
        canvas._clipboard.left += 15;
        canvas.setActiveObject(clonedObj);
        canvas.requestRenderAll();
      });
    }


    setwidth=(canvas:any,width:number)=>{
      canvas.set({width:width})

    }

    setheight=(canvas:any,height:number)=>{
      canvas.set({height:height})

    }

    shapes=(canvas:any=null):any=>{
      let urls:any[]= []
    var rec = new fabric.Rect({
      top: 10,
      left: 10,
      width: 75,
      height: 100,
      fill: 'red',
      stroke: 'blue',
      strokeWidth: 2,
      selectable:false

  });
  
  var cir = new fabric.Circle({
      top: 10,
      left: 100,
      radius: 50,
      fill: 'red',
      stroke: 'blue',
      strokeWidth: 2,
      selectable:false

  });
  
  var tri = new fabric.Triangle({
      top: 10,
      left: 200,
      width: 200, 
      height: 100,
      fill: 'red',
      stroke: 'blue',
      strokeWidth: 2,
      selectable:false

  });
  
  var eli = new fabric.Ellipse({
      top: 150,
      left: 10,
     /* Try same values rx, ry => circle */
      rx: 75,
      ry: 50,
      fill: 'red',
      stroke: 'blue',
      strokeWidth: 4,
      selectable:false

  });
  
  var trapezoid = [ {x:-100,y:-50},{x:100,y:-50},{x:150,y:50},{x:-150,y:50} ];
  var emerald = [ 	{x:850,y:75},
                    {x:958,y:137.5},
                    {x:958,y:262.5},
                    {x:850,y:325},
                    {x:742,y:262.5},
                    {x:742,y:137.5},
                    ];
  var star4 = [
    {x:0,y:0},
    {x:100,y:50},
    {x:200,y:0},
    {x:150,y:100},
    {x:200,y:200},
    {x:100,y:150},
    {x:0,y:200},
    {x:50,y:100},
    {x:0,y:0}
  ];
  var star5 = [ 	{x:350,y:75},
                {x:380,y:160},
                {x:470,y:160},
                {x:400,y:215},
                {x:423,y:301},
                {x:350,y:250},
                {x:277,y:301},
                {x:303,y:215},
                {x:231,y:161},
                {x:321,y:161},];
  var shape = new Array(trapezoid,emerald,star4,star5);
  
  var polyg = new fabric.Polygon(shape[1], {
      top: 180,
      left: 200,
      fill: 'red',
      stroke: 'blue',
      strokeWidth: 2,
      selectable:false
  });
  
  

  urls.push(rec.toDataURL({}),cir.toDataURL({}),tri.toDataURL({}),eli.toDataURL({}),polyg.toDataURL({}))  
    
  
      return urls

    }



    drawtable=()=>{
      var cas:any = document.querySelector("#drawing-here");
      cas.style.border = "1px solid red";
      // Getting Context Information
      var ctx = cas.getContext("2d");
      var rectH = 10;
      var rectW = 10;
      ctx.lineWidth = 0.3;
      // Draw horizontal lines
      for(var i= 0;i < cas.height/rectH; i++){
          ctx.moveTo(0,i*rectH);
          ctx.lineTo(cas.width, i*rectH);
      }
      // Draw vertical lines
      for(var i = 0; i < cas.width/rectW; i++){
          ctx.moveTo(i*rectH,0);
          ctx.lineTo(i*rectH,cas.width);
      }
      ctx.stroke();
    }



    Ondragging(event:any){
      console.log(event)
      event
    .dataTransfer
    .setData('text/html', event.target.id);

  event
    .currentTarget
    .style
    .backgroundColor = 'yellow';
    }


    onDragOver(event:any) {
      event.preventDefault();
    }



    getBoundingBox(ctx:any, left:any, top:any, width:any, height:any) {
      var ret :any= {};
      
      // Get the pixel data from the canvas
      var data = ctx.getImageData(left, top, width, height).data;
      console.log(data);
      var first:any = false; 
      var last:any = false;
      var right :any= false;
      var left:any = false;
      var r:any = height;
      var w = 0;
      var c = 0;
      var d = 0;
  
      // 1. get bottom
      while(!last && r) {
          r--;
          for(c = 0; c < width; c++) {
              if(data[r * width * 4 + c * 4 + 3]) {
                  console.log('last', r);
                  last = r+1;
                  ret.bottom = r+1;
                  break;
              }
          }
      }
  
      // 2. get top
      r = 0;
      var checks = [];
      while(!first && r < last) {
          
          for(c = 0; c < width; c++) {
              if(data[r * width * 4 + c * 4 + 3]) {
                  console.log('first', r);
                  first = r-1;
                  ret.top = r-1;
                  ret.height = last - first - 1;
                  break;
              }
          }
          r++;
      }
  
      // 3. get right
      c = width;
      while(!right && c) {
          c--;
          for(r = 0; r < height; r++) {
              if(data[r * width * 4 + c * 4 + 3]) {
                  console.log('last', r);
                  right = c+1;
                  ret.right = c+1;
                  break;
              }
          }
      }
  
      // 4. get left
      c = 0;
      while(!left && c < right) {
  
          for(r = 0; r < height; r++) {
              if(data[r * width * 4 + c * 4 + 3]) {
                  console.log('left', c-1);
                  left = c;
                  ret.left = c;
                  ret.width = right - left - 1;
                  break;
              }
          }
          c++;
          
          // If we've got it then return the height
          if(left) {
            return ret;    
          }
      }
  
      // We screwed something up...  What do you expect from free code?
      return false;
  }



  drawBoundingBox(ctx:any, bbox:any){
    ctx.strokeStyle="#FF0000";
    ctx.lineWidth=1;
    ctx.beginPath();

    // top
    ctx.moveTo(bbox.left+0.5, bbox.top + 0.5);
    ctx.lineTo(bbox.right+0.5 ,bbox.top + 0.5);
    ctx.stroke();

    // right
    ctx.lineTo(bbox.right+0.5 ,bbox.bottom + 0.5);
    ctx.stroke();

    // bottom
    ctx.lineTo(bbox.left+0.5, bbox.bottom + 0.5);
    ctx.stroke();

    // left
    ctx.lineTo(bbox.left+0.5, bbox.top + 0.5);
    ctx.stroke();
}



  
}
