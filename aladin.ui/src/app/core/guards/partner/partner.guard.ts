import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { LoginService } from '../..';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PartnerGuard implements CanActivate {
  constructor(private isloggedin:LoginService){

  }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if(this.isloggedin.isauthaspartner()){
        return true;
      }
      window.location.href="/"
    return false;
  }
  
}
