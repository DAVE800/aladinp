import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';
import {Observable} from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class ListService {
url="cloths/items";

  constructor(private http:HttpClient) { }

getcloths():Observable<any>{
    return this.http.get<any>(environment.test+this.url+"?page=1")
}

getclt(page:any):Observable<any>{
  return this.http.get<any>(environment.test+this.url+"?page="+page)
}

getcloth(id:any){
  return this.http.get<any>(environment.test+"cloths/"+id)
}

SaveCloth(data:any,file:any):Observable<any>{  
   const formdata = new FormData();
   formdata.append('img',file,file.name);
   formdata.append('name_cloths',data.name_cloths); 
   formdata.append('price',data.price); 
   formdata.append('mode',data.mode); 
   formdata.append('color',data.color); 
   formdata.append('comment',data.comment); 
   formdata.append('user_id',data.user_id);
   formdata.append('type_imp',data.type_imp);
   formdata.append('material',data.material); 
   formdata.append('size',data.size); 

  return this.http.post<any>("https://api.aladin.ci/"+this.url,formdata);

}

SaveGadget(data:any,file:any):Observable<any>{
  let url="gadgets"
  const formdata = new FormData();
   formdata.append('img',file,file.name);
   formdata.append('name_gadget',data.name_gadget); 
   formdata.append('price',data.price); 
   formdata.append('dim',data.dim); 
   formdata.append('diam',data.diam); 
   formdata.append('user_id',data.user_id);
   formdata.append('material',data.material); 
   formdata.append('size',data.size); 
   formdata.append('comment',data.comment); 
  return this.http.post<any>(environment.test+url,formdata);

}

getGadgets():Observable<any>{
  let url="gadgets/?page=1"
  return this.http.get<any>(environment.test+url);

}

getgadgets(page:any):Observable<any>{
  let url="gadgets/?page="
  return this.http.get<any>(environment.test+url+page);

}

getprints(page:any):Observable<any>{
  let url="prints/?page="
  return this.http.get<any>(environment.test+url+page);

}

getPrints():Observable<any>{
  let url="prints/?page=1"
  return this.http.get<any>(environment.test+url);

}

getDisps():Observable<any>{
  let url="disps/?page=1"
  return this.http.get<any>(environment.test+url);

}

getdisps(page:any):Observable<any>{
  let url="disps/?page="
  return this.http.get(<any>environment.test+url+page);

}

getPacks():Observable<any>{
  let url="packs/?page=1"
  return this.http.get<any>(environment.test+url);

}

getpacks(page:any):Observable<any>{
  let url="packs/?page="
  return this.http.get<any>(environment.test+url+page);

}

getDisp(id:any):Observable<any>{
  let url="disps"
  return this.http.get<any>(environment.test+url+ "/"+id);

}

getPack(id:any):Observable<any>{
  let url="packs"
  return this.http.get<any>(environment.test+url+ "/"+id);

}

getGadget(id:any):Observable<any>{
  let url="gadgets";
  return this.http.get<any>(environment.test+url+ "/"+id);

}


getprint(id:any):Observable<any>{
  let url="prints";
  return this.http.get<any>(environment.test+url+ "/"+id);

}

SaveDisp(data:any,file:any):Observable<any>{
  let url="disps"
  const formdata = new FormData();
   formdata.append('img',file,file.name);
   formdata.append('name_display',data.name_display); 
   formdata.append('price',data.price); 
   formdata.append('dim',data.dim); 
   formdata.append('gram',data.gram); 
   formdata.append('user_id',data.user_id);
   formdata.append('comment',data.comment); 
  return this.http.post<any>(environment.test+url,formdata);

}



SavePack(data:any,file:any):Observable<any>{
  let url="packs"
  console.log(data)
  const formdata = new FormData();
   formdata.append('img',file,file.name);
   formdata.append('name_packaging',data.name_packaging); 
   formdata.append('price',data.price); 
   formdata.append('dim',data.dim); 
   formdata.append('gram',data.gram); 
   formdata.append('material',data.material); 
   formdata.append('user_id',data.user_id);
   formdata.append('comment',data.comment); 
  return this.http.post<any>(environment.test+url,formdata);

}


SavePrint(data:any,file:any):Observable<any>{
  let url="prints"
  const formdata = new FormData();
   formdata.append('img',file,file.name);
   formdata.append('name_printed',data.name_printed); 
   formdata.append('price',data.price); 
   formdata.append('dim',data.dim); 
   formdata.append('gram',data.gram); 
   formdata.append('pellicule',data.pellicule); 
   formdata.append('border',data.border); 
   formdata.append('volet',data.volet); 
   formdata.append('verni',data.verni); 
   formdata.append('user_id',data.user_id);
   formdata.append('comment',data.comment); 
  return this.http.post<any>(environment.test+url,formdata);

}

SaveTop(data:any,file:any):Observable<any>{
  let url="tops"
  const formdata = new FormData();
   formdata.append('img',file,file.name);
   formdata.append('price',data.price); 
   formdata.append('user_id',data.user_id);
   formdata.append('comment',data.comment); 
  return this.http.post<any>(environment.test+url,formdata);

}

gettop(id:any):Observable<any>{
  let url="tops";
  return this.http.get<any>(environment.test+url+ "/"+id);

}


gettops():Observable<any>{
  let url="tops";
  return this.http.get<any>(environment.test+url);

}

getfonts():Observable<any>{
  let url="https://api.aladin.ci/fonts"
  
  return this.http.get<any>(url)

}


addStylesheetURL(url:any) {
  var link = document.createElement('link');
  link.rel = 'stylesheet';
  link.href = url;
  document.getElementsByTagName('head')[0].appendChild(link);

}


triggerMouse(event:any){
  var evt = new MouseEvent("click", {
    view: window,
    bubbles: true,
    cancelable: true,
});
event.dispatchEvent(evt);

}



getformes(page:any){
  let url="shapes"
  return this.http.get(environment.test+url+'/?page='+page)

}

getclipart(page:any){
  let url="cliparts/"
  return this.http.get(environment.test+url+'?page='+page)
}
getclothespage():Observable<any>{
  let url ="https://api.aladin.ci/cloths/home/?page=1"
  return this.http.get<any>(url)
}

saveOrder(data:any):Observable<any>{
  return this.http.post<any>(environment.apiBaseUrl+"orders",data)
 
}
saveOrderProducts(data:any):Observable<any>{
  return this.http.post<any>(environment.apiBaseUrl+"orders/products",data)

}
getUserOrder(id:any):Observable<any>{
return this.http.get<any>(environment.apiBaseUrl+"orders/customers/"+id)
}

UpdateUser(id:any,data:any):Observable<any>{
  return this.http.put(<any>environment.apiBaseUrl+"users/"+id,data);
}


ChangePassword(data:any,id:any):Observable<any>{
  let url ="users/change/password/"+id;
  return this.http.put<any>(environment.apiBaseUrl+url,data);
}
}


 