import { NgModule, SkipSelf ,Optional} from '@angular/core';
import { RegisterService,LoginService,FormvalidationService } from './auth';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [],
  imports: [
    HttpClientModule
  ],
  exports:[HttpClientModule,
    
  ],

  providers:[RegisterService,LoginService,FormvalidationService],
})
export class CoreModule { 
 
  constructor(@Optional()@SkipSelf() core:CoreModule ){
    if (core) {
        throw new Error("You should import core module only in the root module")
    }
  }
}
 