
import { NgModule ,CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { SharedModule } from '../shared';
import { CartRoutingModule } from './cart-routing.module';
import { PanierComponent } from './panier/panier.component';



@NgModule({
  declarations: [
    PanierComponent,
 
  ],
  imports: [
    CartRoutingModule,
    SharedModule,
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class CartModule { }
