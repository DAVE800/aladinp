import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { DevmodeComponent } from './devmode/devmode.component';
const routes: Routes = [
  {
  path:'home',
  component: HomeComponent,

},
{
  path:'dev',
  component:DevmodeComponent
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AcceuilRoutingModule { }
