import { Component, OnInit ,OnChanges, OnDestroy} from '@angular/core';
import { AladinService,LocalService } from 'src/app/core';
declare var require: any;

var $ = require("jquery");

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit ,OnChanges,OnDestroy{
  name:any;
  id:any;
  show: boolean = true;
  private subscription:any
  data={}
  cart=false
  editor=false;
  fromhome=true
  constructor(private aladin:AladinService,private l:LocalService) { }

  ngOnInit(): void {
   
    
    this.aladin.ShowEditor.subscribe(mess=>{
      if(!mess.show){
        this.show= mess.show;
        this.data=mess;
        this.editor= !mess.show;
        this.cart= mess.show;
        $(document).ready(function () {
          $('html, body').animate({
              scrollTop: $('#editor').offset().top
          }, 'slow');
          
      });
      }
     
      
  })
   this.l.activatecart.subscribe(res=>{
     if(res){
      this.cart=res
      this.show=!res;
      this.editor=!res;
      $(document).ready(function () {
        $('html, body').animate({
            scrollTop: $('#cart').offset().top
        }, 'slow');
    });

     }else{
      this.cart=res
      this.show=!res;
      this.editor=res;
      
     }

   })


  }

ngOnChanges(){
 

}


ngOnDestroy(){
  this.subscription.unsubscribe()
}
  toggleSpinner(){
    this.show = !this.show;   
  }

}
