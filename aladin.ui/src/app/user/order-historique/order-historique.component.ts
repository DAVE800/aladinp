import { ListService } from 'src/app/core';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-order-historique',
  templateUrl: './order-historique.component.html',
  styleUrls: ['./order-historique.component.scss']
})
export class OrderHistoriqueComponent implements OnInit {
@Input() id:any;
  order:any=[];
  constructor(private m :ListService) {

   }
  ngOnInit(): void {
    this.m.getUserOrder(this.id).subscribe(
      res=>{
        this.order=res;
        this.order=this.order.data
        for(let i=0 ;i<this.order.length;i++){
          this.order[i].description=JSON.parse(this.order[i].description);
        
        }
        console.log(this.order);  
      },
      err=>{
        console.log(err)
      }
    )
  }

}
