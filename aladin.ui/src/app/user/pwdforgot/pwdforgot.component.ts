import { Component, OnInit } from '@angular/core';
import { FormvalidationService } from 'src/app/core';
import { ForgotpwdService } from 'src/app/core';

@Component({
  selector: 'app-pwdforgot',
  templateUrl: './pwdforgot.component.html',
  styleUrls: ['./pwdforgot.component.scss']
})
export class PwdforgotComponent implements OnInit {
email="";
newpwd="";
newpwdc="";
ismail=false;
ispwdm=false;
msg={
  email:"",
  pwd:""
}
  constructor(private validate:FormvalidationService,private forgotpwd:ForgotpwdService) { }

  ngOnInit(): void {
  }

  Updatepwd(){
    if(this.email!=null&&this.newpwd!=null){
      if(this.validate.validateEmail(this.email)){
        let verify =this.forgotpwd.veryfyemail(this.email)
        if(verify.user){
          if(this.validate.validatePassword(this.newpwd,this.newpwdc)){

            this.forgotpwd.passwordforgot({email:this.email,password:this.newpwd},verify.user.id).subscribe(res=>{
              console.log(res);
              },
              err=>{
                console.log(err);
              }
              );
          }else{
            this.ispwdm=!this.ispwdm;
            this.msg.email="passwords do not match";
          }
            
        }else{
          this.ismail=!this.ismail;
          this.msg.email=verify.console.error.message;
          
        }
        
      }
      else{
        this.ismail=!this.ismail;
        this.msg.email="invalide email";
      }

    }


  }

}
