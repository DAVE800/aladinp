import { Component, OnInit,Input,Output,EventEmitter,OnChanges,AfterContentInit } from '@angular/core';
import {ListService} from 'src/app/core';
import {  ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit,OnChanges,AfterContentInit {
 @Input() data:any;
 @Input() isauth:any;
 @Output() neworder=new EventEmitter<any>();

 total:any;
 city:any;
 address:any;
 date:any;
 today:any;
 hidebtn=false;
 t:any
 tab=["Abidjan","Bassam","Bingerville","Dabou","Jacqueville","Autre"]
 

 price_zone={
   abjd:1000,
   other:2000
 }
 isother=false
 delivery_cost:number=0;
 cpt:number=0;
 hasdeliverycost:boolean=false;
 order_data:any;
 payment=false
 options = {weekday: "long", year: "numeric", month: "long", day: "numeric",hour:"2-digit",minute:"2-digit",second:"2-digit"};
  constructor(private item:ListService,private activeroute:ActivatedRoute) { }

  ngOnInit(): void {  
    this.today= new Date(new Date().getTime()+(5*24*60*60*1000))
   this.today= this.today.toLocaleString("fr",{weekday: "long", year: "numeric", month: "long", day: "2-digit"});

  }



  ngAfterContentInit(){
    setTimeout(()=>{
      let m:any=document.querySelector('#money');
      this.item.triggerMouse(m);
    },1000);


  }

  ngOnChanges(){

    
    setInterval(()=>{
      this.date=new Date();
     this.date= this.date.toLocaleString("fr",this.options);
     

  
    },1000);
  
  }


  getTotal(value:any){
    this.total=value
    this.t=this.total;

  }

  ChangeHandler(event:any){
    this.hasdeliverycost=true;
    this.total=parseInt(this.total)
    console.log(event.target.value)
    if(event.target.value!="choix de commune"){
      if(event.target.value=="Autre"){
        this.isother=true;
        this.city=""

      }else{
        this.isother=false;
      }
      if(event.target.value=="Abidjan"){
        this.t=parseInt(this.total)+1000;
        this.delivery_cost=1000;
        this.city=event.target.value
        console.log(this.t)
      }else{
        this.t=parseInt(this.total)+2000;
        this.delivery_cost=2000;
        console.log(this.t);
        this.city=event.target.value

      }
    }else{
      this.t=this.total;
    }
   
  }

  
  followp(){
    if(this.address && this.city){
      this.order_data={
        user:this.data,
        delivery:{
          city:this.city,
          address:this.address
        },
        pmode:"MTN MOMO",
        total:this.t
      }
      document.getElementById('checkout')?.setAttribute('hidden',"true");
      
      this.payment=true
    }

  }

  backcheck(){
    if(this.payment){
      let check=document.getElementById('checkout')
      if(check)
       check.hidden=!check?.hidden
       this.payment=!this.payment;
    }
  }




}
