import { Component, OnInit,Input ,OnChanges,EventEmitter,Output} from '@angular/core';
import { timeout } from 'rxjs/operators';
import { LoginService,ListService,LocalService } from 'src/app/core';
declare var require:any
var $ = require("jquery");
var myalert=require('sweetalert2')

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit ,OnChanges{

constructor(private auth:LoginService,private l:ListService,private cart:LocalService) { }
auth_data:any;
code=225;
phone:any;
paystatus=false;
status=false;
show_success=false;
order_id:any;
cart_items:any
@Input() data:any;
@Output() newbackcheck=new EventEmitter<any>()
timer:any;
  ngOnInit(): void { 
    this.cart_items=this.cart.cart_items
    console.log(this.cart_items)
    this.auth.getToken().subscribe(
      res=>{
        if(res.code==200){
          console.log(res);
          var now = new Date();
          let expiryDate = new Date(now.getTime() + res.token.expires_in*1000);
         console.log(expiryDate);
         this.auth_data={
           token:res.token.access_token,
           expiryDate:expiryDate
          }
        }
       
      },
      error=>{
        console.log(error)
      }
    )
   }

ngOnChanges(){
  console.log(this.data)
}

ClearTimer(){
  if(this.status){
    clearInterval(this.timer)
  } 
}

pay(){
  if(this.phone && this.phone.length==10){
    let data={
      amount:JSON.stringify(this.data.total),
      phone:this.code+this.phone,   
    }
    let now = new Date();
    if(now<=this.auth_data.expiryDate){
      if(this.paystatus){
        this.paystatus=!this.paystatus
      }
      this.toggle()
      Object.assign(data,{token:this.auth_data.token});
      this.auth.pay(data).subscribe(
        res=>{
          if(res.code==202){
            this.timer= setInterval(()=>{
              this.getpaymentstatus(res.ref); 
               
            },10000)   
          }else{
            this.toggle()
            myalert.fire({
              title: "echec de transaction",
              text: "La transaction a echoué verifiez votre solde.",
              icon: "error",
               button: "Ok"
              })
            console.log(res.code)
          }
        },
        err=>{
          this.toggle()
          myalert.fire({
            title: "echec de transaction",
            text: "La transaction a echoué verifiez votre solde.",
            icon: "error",
             button: "Ok"
            })
          console.log(err)
        }
      )
    }else{
      myalert.fire({
        title: "echec de transaction",
        text: "La transaction a echoué verifiez votre solde.",
        icon: "error",
         button: "Ok"
        })
      console.log("err")
    }

  }
}

toggle(){
  this.paystatus=!this.paystatus
}
getpaymentstatus(ref:any){
  let now =new Date()
  if(now>this.auth_data.expiryDate){
    this.auth.getToken().subscribe(res=>{
      if(res.code==200){
        console.log(res);
        var now = new Date();
        let expiryDate = new Date(now.getTime() + res.token.expires_in*1000);
       console.log(expiryDate);
       this.auth_data={
         token:res.token.access_token,
         expiryDate:expiryDate
        }
        this.auth.getpaymentStatus(ref,this.auth_data.token).subscribe(res=>{
          if(res.code==200){
            if(res.data.status=="SUCCESSFUL"){
              this.status=!this.status
              var ok = document.getElementById("ok")
               this.l.triggerMouse(ok);
              this.toggle();
              this.saveOrder();
            }
            if(res.data.status=="FAILED"){
              this.status=!this.status;
              this.toggle()
              var ok = document.getElementById("ok");
              this.l.triggerMouse(ok)
              this.toggle()
              myalert.fire({
                title: "echec de transaction",
                text: "La transaction a echoué verifiez votre solde.",
                icon: "error",
                 button: "Ok"
                })

            }
            console.log(res.data.status)
          }
        },err=>{
          myalert.fire({
            title: "echec de transaction",
            text: "La transaction a echoué verifiez votre solde.",
            icon: "error",
             button: "Ok"
            })
          console.log(err);
          this.toggle()

        })
      }

    },err=>{
      myalert.fire({
        title: "echec de transaction",
        text: "La transaction a echoué verifiez votre solde.",
        icon: "error",
         button: "Ok"
        })
        console.log(err);
        this.toggle()  
    })

  }else{
    this.auth.getpaymentStatus(ref,this.auth_data.token).subscribe(res=>{
      if(res.code==200){
        if(res.data.status=="SUCCESSFUL"){
          this.status=true
          var ok = document.getElementById("ok")
          this.l.triggerMouse(ok)
          this.saveOrder()
         
        }
        if(res.data.status=="FAILED"){
          this.status=true
          var ok = document.getElementById("ok")
          this.l.triggerMouse(ok)
          this.toggle()
          myalert.fire({
            title: "echec de transaction",
            text: "La transaction a echoué verifiez votre solde.",
            icon: "error",
             button: "Ok"
            })
        }
        console.log(res.data.status)
      }
    },err=>{
      myalert.fire({
        title: "echec de transaction",
        text: "La transaction a echoué verifiez votre solde.",
        icon: "error",
         button: "Ok"
        })
      console.log(err);
      this.toggle()

    })

  }
}


saveOrderproduct(order:any){
let data ={
items:JSON.stringify(this.cart_items),
order:order,
email:this.data.user.email,
name:this.data.user.name
}
this.l.saveOrderProducts(data).subscribe(
  res=>{
    console.log(res)
    if(res.status){
      this.toggle();
      localStorage.removeItem("cart");
      localStorage.removeItem("Total");
      this.show_success=!this.show_success;
      myalert.fire({
        title: "Transaction",
        text: "Transaction reussi, votre commande a été effectuée avec succès",
        icon: "success",
         button: "Ok"
        })
      setTimeout(()=>{
       location.href="/home"
      },20000)
      }else{
      this.toggle();
      myalert.fire({
        title: "echec",
        text: "operation echouée",
        icon: "error",
         button: "Ok"
        })
 
    }
  },
  err=>{
    myalert.fire({
      title: "echec de transaction",
      text: "La transaction a echoué",
      icon: "error",
       button: "Ok"
      })
    this.toggle();
    console.log(err);
  }
)

}


saveOrder(){
 let data={
  status:"new",
  customer:this.data.user.user_id,
  dmode:this.data.pmode,
  d_place:this.data.delivery.address,
  city:this.data.delivery.city,
  total:this.data.total
 }
 this.l.saveOrder(data).subscribe(
   res=>{
    console.log(res)
     if(res.status){
      this.saveOrderproduct(res.resp.insertId);
      this.order_id=res.resp.insertId;
     }else
     {
       this.toggle();
       myalert.fire({
        title: "echec de transaction",
        text: "La transaction a echoué",
        icon: "error",
         button: "Ok"
        })
     }
   },
   err=>{
    myalert.fire({
      title: "echec de transaction",
      text: "La transaction a echoué",
      icon: "error",
       button: "Ok"
      })
    this.toggle();
     console.log(err)
   }
 )
}

backcheck(){
this.newbackcheck.emit();
}


}
