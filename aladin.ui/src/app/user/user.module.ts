import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { UserRoutingModule } from './user-routing.module';
import { HomeuserComponent } from './homeuser/homeuser.component';
import { RegisterComponent } from './register/register.component';
import { PwdforgotComponent } from './pwdforgot/pwdforgot.component';
import { InfoComponent } from './info/info.component';
import { SharedModule } from '../shared';
import { LoginComponent } from './login/login.component';
import {OrderHistoriqueComponent }from './order-historique/order-historique.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { PaymentComponent } from './payment/payment.component';
import {Ng2TelInputModule} from 'ng2-tel-input';
import { RegistertopayComponent } from './registertopay/registertopay.component';
import { AuthComponent } from './auth/auth.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { ChangepwdComponent } from './changepwd/changepwd.component';
import { MenuComponent } from './menu/menu.component';
import { SuccessComponent } from './success/success.component';

@NgModule({
  declarations: [
    HomeuserComponent,
    RegisterComponent,
    PwdforgotComponent,
    LoginComponent,
    InfoComponent,
    OrderHistoriqueComponent,
    CheckoutComponent,
    PaymentComponent,
    RegistertopayComponent,
    AuthComponent,
    SidebarComponent,
    ChangepwdComponent,
    MenuComponent,
    SuccessComponent 
  ],
  imports: [
    UserRoutingModule,
    SharedModule,
    Ng2TelInputModule
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class UserModule { }
