import { Component, OnInit,OnChanges } from '@angular/core';
import { LoginService } from '../../core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit,OnChanges {
password:any;
email:any;
id:any;
message={
  email:"",
  password:""
}
spinning=true;
epwd=false
maile=false
show=false
isauth=false;
userdata:any;
payment=false;
prog=false;
loginform=false;
auth_data:any

  constructor(private loginservice:LoginService) { }
ngOnChanges(){
  
}
  ngOnInit(): void {
    if(localStorage.getItem('token')){
      let token:any= localStorage.getItem("token");
      token= JSON.parse(token);
      this.id=token.id;
      this.loginservice.getUser(token.id).subscribe(
        res=>{
          let r:any=res
          console.log(r)
          if(r.status){
           
            this.userdata=r.data[0];
            console.log(this.userdata)
            this.isauth=true;
            this.loginform=false;
            this.spinning=!this.spinning
  
          }
        }
      )
     
    }else{
      this.loginform=true;
      this.spinning=!this.spinning

    }
  }

toggle(){
  this.show=!this.show;
}
  login(){
    if(this.password && this.email){
      if(this.show){
        this.toggle();
      }

      this.toggle()
      this.loginservice.login({email:this.email,password:this.password}).subscribe(
        res=>{
          if(res.status){
            let id=res.user.id;
            this.isauth=!this.isauth;
            this.loginform=!this.loginform
            this.userdata=res.user.data
            Object.assign(this.userdata,{id:id});
            let now =new Date()
          let expiryDate = new Date(now.getTime() + res.token.exp*1000);
          this.auth_data={
           token:res.token.access_token,
           expiryDate:expiryDate,
           user:this.userdata.name,
           id:this.userdata.user_id
          }
          try{
           localStorage.setItem('access_token',res.user.is_partner);
           localStorage.setItem('user',res.user.id);
            localStorage.setItem('token',JSON.stringify(this.auth_data));
            location.reload()
          }catch(e:any){
            console.log(e)
          }

          }
          if(res.message!=undefined){
            this.toggle()
            this.maile=!this.maile;
            this.message.email=res.message; 
          }
          
        },
        err=>{
          this.toggle();
          console.log(err);
          if(err.error.text!=undefined){
           this.maile=!this.maile;
           this.message.email=err.error.text;   
          }
          if(err.error.password!=undefined){
            this.epwd=!this.epwd
            this.message.password=err.error.password;   
          }  
        }
      )
    }
  }


  

 


}


