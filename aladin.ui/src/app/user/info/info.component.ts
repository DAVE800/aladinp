import {LoginService ,ListService} from 'src/app/core';
import { Component, OnInit,Input, } from '@angular/core';
declare var require:any;
var myalert=require('sweetalert2')
@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss']
})

export class InfoComponent implements OnInit {
  @Input() id:any;
  user:any=[];
  name:any;
  phone:any;
  email:any;
  loading=false;
  constructor(private z:LoginService,private L:ListService) { }
  ngOnInit(): void {
    console.log(this.id)
    this.z.getUser(this.id).subscribe(
      res=>{
        this.user=res;
        this.user=this.user.data[0];
        this.email=this.user.email;
        this.phone= this.user.phone;
        this.name=this.user.name
      },
      err=>{
        console.log(err)
      }
    )
  }
onloading(){
  this.loading=!this.loading
}

  updateUser(){
    let data ={
      name:JSON.stringify(this.name),
      phone:JSON.stringify(this.phone),
      email:JSON.stringify(this.email)

    }
    if(this.loading){
      this.onloading()
    }
    this.onloading()
    this.L.UpdateUser(+this.id,data).subscribe(
      res=>{
        this.onloading()
        let r:any= res;
        if(r.status){
          location.reload();
          myalert.fire(
            {
              icon:"success",
              html:"vous avez modifié vos informations",
              title:"modification de données "
            }
          )
        }else{
          console.log(r.status);
        }
      },
      err=>{
        this.onloading();
        myalert.fire(
          {
            icon:"error",
            html:"opération a échoué",
            title:"modification de données "

          }
        )
        console.log(err)
      }
    )
    
  }

}
