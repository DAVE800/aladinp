import { ListService } from 'src/app/core';
import { Component, OnInit, Input } from '@angular/core';
declare var require:any;
var myalert=require('sweetalert2')


@Component({
  selector: 'app-changepwd',
  templateUrl: './changepwd.component.html',
  styleUrls: ['./changepwd.component.scss']
})
export class ChangepwdComponent implements OnInit {
  @Input() id:any;
change:any;
showHide=true;
showHide1=true;
showHide2=true;
password:any;
newpassword:any;
cnewpassword:any;
loading=false
  constructor(private f:ListService) { }

  ngOnInit(): void {
    // this.f.UpdateUser(this.id).subscribe(
    //   res=>{
    //     this.change=res
    //   },
    //   err=>{
    //     console.log(err)
    //   }
    // )
  }

  onloading(){
    this.loading=!this.loading;
  }


toggle1(){
  this.showHide1=!this.showHide1
}

toggle2(){
  this.showHide2=!this.showHide2
}

toggle(){
  this.showHide=!this.showHide
}
hide_eye2(){
  let x = document.getElementById('n-pwd')
  if(this.showHide1){
    if(x){
      x.setAttribute("type","text")
      this.toggle1();
    }
  }else{
    if(x){
      x.setAttribute("type","password")
      this.toggle1();
    }
  }
}

hide_eye(){
  let x = document.getElementById('hold-pwd')
  if(this.showHide){
    if(x){
      x.setAttribute("type","text")
      this.toggle();
    }
  }else{
    if(x){
      x.setAttribute("type","password")
      this.toggle();
    }
  }
}

cacheoeil3(){
 let x  = document.getElementById("n-pwd2");

 if(this.showHide2){
  if(x){
   x.setAttribute("type","text")
   this.toggle2();

  }
 }else{
    if(x)
    {
     x.setAttribute("type","password")
     this.toggle2();

    }
 }

 }

 changepwd(){
   if(this.cnewpassword && this.password && this.newpassword){
   
     if(this.newpassword===this.cnewpassword){
      if(this.loading){
        this.onloading()
      }
      this.onloading()
       this.f.ChangePassword({newpassword:this.newpassword,password:this.password},this.id).subscribe(
         res=>{
           this.onloading();
           let a:any=res
           if(a.status){
            console.log(a)
            myalert.fire(
              {
               icon:"success",
               title:"Opération",
               html:a.message
              }
            )
           }else{
            myalert.fire(
              {
               icon:"error",
               title:"Opération",
               html:a.message
 
              })
             console.log(a)
           }
         },err=>{
          this.onloading() 

          myalert.fire(
            {
             icon:"error",
             title:"Opération",
             html:err.error.message

            })
         }
       )
     }else{
       myalert.fire({
         icon:"error",
         title:"Opération",
         html:"password do not match"
       })
     }

   }

 }

}
