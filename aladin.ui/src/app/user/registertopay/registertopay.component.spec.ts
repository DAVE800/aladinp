import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistertopayComponent } from './registertopay.component';

describe('RegistertopayComponent', () => {
  let component: RegistertopayComponent;
  let fixture: ComponentFixture<RegistertopayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistertopayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistertopayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
