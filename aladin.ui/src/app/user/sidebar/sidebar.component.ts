import { Component, OnInit ,Input} from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  @Input() id:any;

  constructor() { }

  ngOnInit(): void {
  }


  logout(){
    try{
        localStorage.removeItem("token");
        localStorage.removeItem('user')
        localStorage.removeItem('access_token')
        location.href="/home"

    }catch(e:any){
      console.log(e)

    }
  }

}
