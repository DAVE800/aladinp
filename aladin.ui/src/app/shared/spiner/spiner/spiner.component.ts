import { Component, OnInit,Input ,SimpleChange } from '@angular/core';

@Component({
  selector: 'app-spiner',
  templateUrl: './spiner.component.html',
  styleUrls: ['./spiner.component.scss']
})
export class SpinerComponent implements OnInit {
  @Input() size : number = 25;
  @Input() show: boolean =false;
  @Input() httploading:boolean=false;

  url="/assets/images/spin.png";
  
  constructor() { }

  ngOnInit():void{

  }

  ngOnChanges(changes:SimpleChange){
    console.log(changes); 
    
  }

}
