import { ListService,AladinService } from 'src/app/core';
import { Component, OnInit,Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-aladin-shapes',
  templateUrl: './aladin-shapes.component.html',
  styleUrls: ['./aladin-shapes.component.scss']
})
export class AladinShapesComponent implements OnInit {
  forms:any=[];
  pages=[1,2,3];

  urls:any[]=[];
  show=false;
  current_page:any=1;
  @Output() newItemEvent =new EventEmitter<string>();
  constructor(private http:ListService,private aladin:AladinService) { }

  ngOnInit(): void {

     this.urls =this.aladin.shapes()
    this.http.getformes(this.current_page).subscribe(res=>{
      this.forms=res;
      console.log(this.forms.shapes);
    this.forms=this.forms.shapes
    for(let url of this.forms){
    //  this.urls.push({url:this.forms.})
    }
  },er=>{console.log(er)})

   
  }


  toggle(){
    this.show=!this.show
  }
  addNewItem(event:any){
    this.newItemEvent.emit(event);
  }
nextPage(){
  this.current_page= this.current_page + 1
  this.toggle()
  this.http.getformes(this.current_page).subscribe(res=>{
   let data :any=res;
      if(data.shapes.length>0){
        this.toggle();
         console.log(data.shapes)
           this.forms=data.shapes;
      }else{
        this.toggle();
      }
      
  },er=>{
    this.toggle()
    console.log(er)})

}

currentPage(event:any){
  var page=event.target.id
    if(+page){
      this.current_page=page
      this.toggle()
      this.http.getformes(page).subscribe(res=>{
        let data :any=res;
        if(data.status==200){
         if(data.shapes.length>0){
        this.toggle();
         console.log(data.shapes)
           this.forms=data.shapes;
      }else{
        this.toggle();
      }
        }else{
          this.toggle();
        }
     },er=>{
      this.toggle();
      console.log(er);
      })
    }

}

previousPage(){
  if (+this.current_page==1){

  }else{
    this.current_page = (+this.current_page)- 1
    this.toggle()
    this.http.getformes(this.current_page).subscribe(res=>{
      let data :any=res;
      if(data.shapes.length>0){
        this.toggle();
         console.log(data.shapes)
           this.forms=data.shapes;
      }else{
        this.toggle();
      }
   }, er=>{
    this.toggle()
     console.log(er)})
  }

}


}
