import { Component, OnInit,EventEmitter,Output,Input } from '@angular/core';

@Component({
  selector: 'app-aladin-products',
  templateUrl: './aladin-products.component.html',
  styleUrls: ['./aladin-products.component.scss']
})
export class AladinProductsComponent implements OnInit {
@Input() data:any;
@Output() newItem=new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void {
  }

  triggeraction(value: any){
    this.newItem.emit(value);
  }
  
  
}
