import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AladinFacesComponent } from './aladin-faces.component';

describe('AladinFacesComponent', () => {
  let component: AladinFacesComponent;
  let fixture: ComponentFixture<AladinFacesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AladinFacesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AladinFacesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
