  import { style } from '@angular/animations';
  import { Component, Input, OnInit,OnChanges } from '@angular/core';
  import { fabric } from 'fabric';
  var domtoimage=require('dom-to-image');
  var contextmenu=require("@mturco/context-menu")
  const interact = require('interactjs')

  var myalert=require('sweetalert2')
  const radius = 50;
  const fill = "#0F0";
  var pos = 60;
  declare var FontFace:any;
  declare var require: any;
  var $ = require("jquery");
  import { ColorEvent } from 'ngx-color';
  import { LocalService,AladinService,CartService } from 'src/app/core';

  @Component({
    selector: 'app-aladin',
    templateUrl: './aladin.component.html',
    styleUrls: ['./aladin.component.scss']
  })
  export class AladinComponent implements OnInit ,OnChanges{
  @Input() data:any;
  canvas:any;
  spinner=false
  objectToSendBack:any;
  face1:any
  face2:any
  current_col:any;
  cactive=true;
  active=true;
  origin_1=null;
  origin_2=null;
  front_data:any=[];
  back_data:any=[];
  production_f1:any;
  colors:any=["indigo"];
  production_f2:any;
  newdatapack:any;
  newcloth:any;
  category:any
  iscloth=false;
  ispack=false;
  height:number=600;
  width:number=600
  url:any;
  group:any;
  currentface:any
  innercanvas:any;
  startX:any
  startY:any
  startWidth:any
  startHeight :any;
  p:any;
  cpt=0;
  newcanvas:any
  mydesigns=0;
  colorarray=['#D9E3F0', '#F47373', '#697689', '#37D67A', '#2CCCE4', '#555555',"#D2691E",'#dce775', '#ff8a65', '#ba68c8',"blue","yellow","orange","blacK","red","indigo","green","brown","#800080","#808000","#000080"];
  constructor(private local:LocalService,private aladin:AladinService,private service:CartService) { }

    ngOnInit(): void {
  this.mydesigns=this.local.items.length
      this.service.dignUpdated.subscribe(d=>{
        this.mydesigns=d;
      })
      this.aladin.addedText.subscribe(
      message=>{
        if(message){
          if(this.canvas.getActiveObject()){
            if(this.canvas.getActiveObject().get('type')==="text"){
              this.canvas.getActiveObject().set({text:message})
              this.canvas.renderAll(this.canvas.getActiveObject())
            }else{
              console.log(this.canvas.getActiveObject().type);
              this.aladin.addText(this.canvas,message);


            }
          
          }else{
            this.aladin.addText(this.canvas,message);

          }
        }else{
          console.log(message)
        }
      }
      )
      setTimeout(()=>{
        console.log(this.data);
        if(this.data.category=="clothes"){
          this.iscloth=!this.iscloth
          this.face1=this.data.url;
          this.face2=this.data.back
        }
        if(this.data.category=="packs"){
          this.ispack=!this.ispack
          this.face1=this.data.url;
          this.face2=this.data.url;
        }
        this.url=this.data.url;

      },2000);

      this.canvas = new fabric.Canvas('aladin',{ 
        hoverCursor: 'pointer',
        selection: true,
        selectionBorderColor:'blue',
        fireRightClick: true,
        preserveObjectStacking: true,
        stateful:true,
        stopContextMenu:false
      }); 
      this.canvas.filterBackend=new fabric.WebglFilterBackend();
    

      this.height=200;
      this.width=200;


  //this.canvas.loadFromJSON('{"objects":[{"type":"group","version":"4.5.0","left":133.89,"top":240,"width":253.22,"height":210.92,"angle":321.15,"objects":[{"type":"group","version":"4.5.0","left":7.99,"top":36.05,"width":161,"height":161,"scaleX":0.43,"scaleY":0.43,"objects":[{"type":"rect","version":"4.5.0","left":-80.5,"top":19.5,"width":80,"height":50,"fill":"red"},{"type":"rect","version":"4.5.0","left":19.5,"top":-80.5,"width":50,"height":70,"fill":"blue"},{"type":"rect","version":"4.5.0","left":19.5,"top":19.5,"width":60,"height":60,"fill":"green"}]},{"type":"group","version":"4.5.0","left":68.08,"top":-105.46,"width":85.76,"height":131.3,"angle":46.96,"objects":[{"type":"rect","version":"4.5.0","left":1.88,"top":-43,"width":80,"height":50,"fill":"red","scaleX":0.17,"scaleY":2.2,"angle":22.89},{"type":"rect","version":"4.5.0","left":-11.04,"top":-65.65,"width":50,"height":70,"fill":"blue","scaleX":0.38,"angle":26.64},{"type":"rect","version":"4.5.0","left":3.95,"top":25.28,"width":60,"height":60,"fill":"green","scaleY":0.31,"angle":290.63}]},{"type":"group","version":"4.5.0","left":-126.61,"top":-45.25,"width":113.05,"height":86.33,"scaleY":1.88,"angle":332.32,"objects":[{"type":"rect","version":"4.5.0","left":-37.06,"top":0.64,"width":80,"height":50,"fill":"red","scaleY":0.36},{"type":"rect","version":"4.5.0","left":25.11,"top":-43.17,"width":50,"height":70,"fill":"blue","scaleX":0.62},{"type":"rect","version":"4.5.0","left":-56.53,"top":-17.83,"width":60,"height":60,"fill":"green","scaleX":0.6}]}]},{"type":"group","version":"4.5.0","left":194.14,"top":42.81,"width":79.65,"height":136.11,"objects":[{"type":"rect","version":"4.5.0","left":-39.82,"top":-48.7,"width":80,"height":50,"fill":"red","scaleX":0.59},{"type":"rect","version":"4.5.0","left":9.97,"top":-68.06,"width":50,"height":70,"fill":"blue","scaleX":0.59},{"type":"rect","version":"4.5.0","left":-8.68,"top":7.06,"width":60,"height":60,"fill":"green","scaleX":0.59}]}]')

    this.toggleSpinner()
    this.aladin.makeImage(this.data.url,this.canvas);
    this.aladin.subject.subscribe(spinner=>{
      if(spinner){
        this.spinner=false;
        console.log(spinner)
      }

    })
  this.aladin.drawtable();

  const menu = new contextmenu('table tr', [
    {
      name: 'Add row',
      fn: () => {
        /* ... */
      },
    },
    {
      name: 'Remove row',
      fn: () => {
        /* ... */
      },
    },
  ]);

  menu.on('itemselected', () => {
    /* ... */
  });
  let scope = this;
  $(".upper-canvas").on('contextmenu', function (options: any) {
      let target: any = scope.canvas.findTarget(options, false);
      if (target) {
          let type: string = target.type;
          if (type === "group") {
              console.log('right click on group');
          } else {
              scope.canvas.setActiveObject(target);
              console.log('right click on target, type: ' + type);
          }
      } else {
          scope.canvas.discardActiveObject();
          scope.canvas.discardActiveGroup();
          scope.canvas.renderAll();
          console.log('right click on canvas');
      }
      options.preventDefault();
  });



    this.canvas.on('object:moving', (event:any)=>{
      // this.onMouseMove(event)
      });
      this.canvas.on('mouse:down', (event:any)=>{
        this.onMouseDown();  
        console.log(event.e.clientX, event.e.clientY);

      });

      this.canvas.on('mouse:out', (event:any)=>{

    
    
      });
      this.canvas.on("selection:created", (event:any)=>{
        this.objectToSendBack = event.target;
        this.sendSelectedObjectBack()
      }); 
      this.canvas.on("selection:updated",(event:any)=>{
        this.objectToSendBack = event.target;
      });

    }

    ngOnChanges(){
    
  
    }

    initDrag(e:any) {
      this.startX = e.clientX;
      this.startY = e.clientY;
      this.startWidth = parseInt((document as any).defaultView.getComputedStyle(this.p).width, 10);
      this.startHeight = parseInt((document as any).defaultView.getComputedStyle(this.p).height, 10);
      document.documentElement.addEventListener('mousemove', this.doDrag, false);
      document.documentElement.addEventListener('mouseup', this.stopDrag, false);
  }

  doDrag(e:any) {
    this.p.style.width = (this.startWidth + e.clientX - this.startX) + 'px';
    this.p.style.height = (this.startHeight + e.clientY - this.startY) + 'px';
  }
    

    sendSelectedObjectBack =()=>{
      this.canvas.sendToBack(this.objectToSendBack);
    }
    toggleSpinner(){
    this.spinner=!this.spinner;
    }
    
    stopDrag(e:any) {
      document.documentElement.removeEventListener('mousemove', this.doDrag, false); 
        document.documentElement.removeEventListener('mouseup', this.stopDrag, false);
  }



  createText(event:any){
  this.aladin.addText(this.canvas)
  }


  overLineText(event:any){
  this.aladin.overline(this.canvas);
  }

  underLineText(){
  this.aladin.underline(this.canvas);
  }


  ItalicText(event:any){
  this.aladin.italic(this.canvas);
  }


  boldText(event:any){
    this.aladin.bold(this.canvas);
  }

  removeItem(){
  this.aladin.remove(this.canvas)

  }

  setTextBeforeEdit(){

  this.aladin.textbefore(this.canvas);
  }

  Textfont(event: any) {
    this.aladin.textfont(event,this.canvas);
  
  
  }


  textwidth(event:any){
  this.aladin.textwidth(event,this.canvas);
  }


  textwidthminus(event:any){
  this.aladin.minusOrplus(event,this.canvas);
  }
  textwidthplusOne(event:any){
    this.aladin.minusOrplus(event,this.canvas);
  }


  OnRightClick(event:any):boolean{   
    var elt=document.getElementById('xdv')
  this.aladin.rightclick(this.canvas,elt)
  return false

  }



  changeComplete($event:ColorEvent){
    let image;
    if(this.colors.length=1){

      myalert.fire({
        title: "pas de couleur disponible",
        text: `il n'y a pas d'autre couleurs pour ce produit`,
        icon: "error",
        button: "Ok"
        });
        setTimeout(()=>{
          let idcol= document.getElementById("clr");
          this.aladin.triggerMouse(idcol)
        },1000);
    }
    for(let elt of this.data){
        if($event.color.hex==elt.lib){
          image=elt.lib_img;
          this.face1=image;
          this.face2=elt.lib_back;
          this.current_col=$event.color.hex;
          break
          }
        }
    if(image!=undefined){
      this.canvas.backgroundImage=null;
      this.aladin.makeImage(image,this.canvas);
      this.cactive=true;
      this.origin_1=null;
      this.origin_2=null;
      let idcol= document.getElementById("clr");
      this.aladin.triggerMouse(idcol);

    }

  }


  setshdow($event:any){
    this.aladin.shadow($event,this.canvas);
  }

  setStroke($event:any){
  this.aladin.stroke($event,this.canvas)
  }


  texteclor($event:ColorEvent){
    let color = $event.color.hex; 
    this.aladin.textcolor(color,this.canvas);
    // this.changeColor(color)
    
  }


  changeColor(color:any){
  this.aladin.itemcolor(color,this.canvas);
  
  }

  setItem(event:any){
    if(!this.spinner){
      this.toggleSpinner()

    }
  this.aladin.setitem(event,this.canvas);
  this.aladin.changeItem.subscribe(res=>{
    if(res){
      this.toggleSpinner();
      
    }
  })

  }


  
  Copy() {
    this.aladin.copy(this.canvas);

  }


  Paste() {
  this.aladin.paste(this.canvas)	
  }



  onFileUpload(event:any){
    let file = event.target.files[0];
    if(!this.aladin.handleChanges(file)){
      
      const reader = new FileReader();
    
    reader.onload = () => {
      let url:any = reader.result;
      fabric.Image.fromURL(url,(oImg) =>{
      oImg.set({  
          scaleX:0.5,     
          scaleY:0.5,
          crossOrigin: "Anonymous",
    });
      this.canvas.add(oImg).setActiveObject(oImg);
      this.canvas.centerObject(oImg);
      this.canvas.renderAll(oImg)
    
    })
      };
      reader.readAsDataURL(file);
      
    }
    
  }


  setImage(value:any){
   
    if(value.category=="clothes"){
    
      this.data=value;
      this.face2=this.data.back;
      this.face1=this.data.url;
      console.log(this.data,"cloths");
      var objt=this.canvas.getObjects()
      for(let i=0;i<objt.length;i++){
        this.canvas.remove(objt[i]);
      }
      this.aladin.set(this.canvas,this.data.url,{url:this.data.url,back:this.data.back})  
    
    }
    
    if(value.category=="packs"){

      this.data=value;
      console.log(this.data,"packs");
      var objt=this.canvas.getObjects()
      for(let i=0;i<objt.length;i++){
        this.canvas.remove(objt[i]);
      }


      this.aladin.set(this.canvas,this.data.url,{url:this.data.url,back:this.data.url}) 
    }
    
    

  }
      


  process_front(e:any){
    
    if(e=="imgfront" && this.active){
      let objts=this.canvas.getObjects()
      if(objts.length>0){
        for(let i=0;i<objts.length;i++){
          this.canvas.remove(objts[i]);
        }
      this.canvas.backgroundImage=null;

      }
      if(this.front_data.length>0){ 
        for(let elt of this.front_data){
          this.canvas.add(elt);
          this.canvas.renderAll(elt);
        }
          
          }

          if(this.data.category=="packs"){
            this.aladin.set(this.canvas,this.data.url,{url:this.face1,back:this.face2});

          }
          if(this.data.category=="clothes"){
            this.aladin.set(this.canvas,this.data.url,{url:this.face1,back:this.face2});


          }

      }
  }


  turnImage(event:any){
    let e=event.target.id;
   
  if(e=="imgback"){
    this.process_back(e);
  }
  if(e=="imgfront"){
    this.process_front(e);
  }
  
  }


  process_back(id:string){
    if(id=="imgback" && !this.active){
      let objts=this.canvas.getObjects();
      if(objts.length>0){
        for(let i=0;i<objts.length;i++){
          this.canvas.remove(objts[i]);
        }

      } 

      if(this.back_data.length>0){  
        for(let elt of this.back_data){
          this.canvas.add(elt);
          this.canvas.renderAll(elt);
        }     
      }
      if(this.data.category=="packs")
      this.aladin.set(this.canvas,this.data.url,{url:this.face1,back:this.face2});
  
    }
    if(this.data.category=="clothes"){
      console.log(this.data.url,"ok");
      this.toggleSpinner()
      this.aladin.set(this.canvas,this.data.back,{url:this.face1,back:this.face2});
      

    }


    }

    savedesigns(){
      
      if(this.active){
        let items = this.canvas.getObjects();
        console.log(items)
        for(let el of items){
          this.front_data.push(el);
        }

          this.origin_1=this.canvas.toDataURL();
          this.face1=this.origin_1;
          this.active=!this.active;
          this.canvas.backgroundImage=null;
          this.production_f1=this.canvas.toDataURL()  
          var elt=document.getElementById('imgback');
          this.aladin.triggerMouse(elt);  
        
      }else{

        this.origin_2=this.canvas.toDataURL();
        this.face2=this.origin_2;
        let items = this.canvas.getObjects();
        for(let el of items){
          this.back_data.push(el);
        }     
        this.canvas.backgroundImage=null;
        this.production_f2=this.canvas.toDataURL()
        this.active=!this.active;
        var elt=document.getElementById('imgfront');
        this.aladin.triggerMouse(elt);

      
    } 

    this.saveIt();

    }




  saveIt(){
  if(this.production_f1 && this.production_f2){

    try{
      if(this.local.items.length>=8){
      myalert.fire({
        title:"quota de stockage",
        icon: 'info',
        html:"<h2 style='color:red;font-Size:15px'>Vous ne pouvez plus enregistrer de designs dans l'espace local, veuillez supprimer certains de vos designs ou passer au panier pour passer votre commande, afin de pouvoir poursuivre vos operations sur aladin.ci </h2>"
      }
      )
      }else{
          let data:any={}

        Object.assign(data,{face1:this.face1,face2:this.face2,text1:this.front_data,text2:this.back_data,production_f1:this.production_f1,production_f2:this.production_f2,data:this.data});
        this.local.add(data);
        this.production_f1=null;
        this.production_f2=null;
        
  }
    }catch(err:any){
      if(err.code==22){
      myalert.fire({
        title:"quota de stockage",
        icon: 'info',
        html:"<h2 style='color:red;font-Size:15px'>Vous ne pouvez plus enregistrer de designs dans l'espace local, veuillez supprimer certains de vos designs ou passer au panier pour passer votre commande, afin de pouvoir poursuivre vos operations sur aladin.ci </h2>"
      }
      )
        console.log(err);
      }else if(err=="QUOTA_EXCEEDED_ERR"){
      myalert.fire({
        title:"quota de stockage",
        icon:'info',
        html:"<h2 style='color:red;font-Size:15px'>Vous ne pouvez plus enregistrer de designs dans l'espace local, veuillez supprimer certains de vos designs ou passer au panier pour passer votre commande, afin de pouvoir poursuivre vos operations sur aladin.ci </h2>"
      }
      )
      }
  }


  }


  }
  onMouseDown=()=>{
    let canvas= this.canvas;
    let key=0
  $(document).keydown((e:any)=>{
    if( e.which === 89 && e.ctrlKey){
        console.log(e.which,e.ctrlKey);
    }
    else if(e.which === 90 && e.ctrlKey){
        console.log(e.which,e.ctrlKey)
    } else if(e.which === 67 && e.ctrlKey){
      console.log(e.which,e.ctrlKey)
      canvas.getActiveObject().clone(function(cloned:any) {
        canvas._clipboard= cloned;
      });
  
    }
  
    else if(e.which === 86 && e.ctrlKey){
      console.log(e.which,e.ctrlKey);
      if(e.ctrlKey)
      {
        canvas._clipboard.clone(function(clonedObj:any) {
          canvas.discardActiveObject();
          clonedObj.set({
            left: clonedObj.left + 15,
            top: clonedObj.top + 15,
            evented: true,
          });
          if (clonedObj.type === 'activeSelection') {
            // active selection needs a reference to the canvas.
            clonedObj.canvas = canvas;
            clonedObj.forEachObject(function(obj:any) {
              canvas.add(obj);
            });
            // this should solve the unselectability
            clonedObj.setCoords();
          } else {
            canvas.add(clonedObj);
          }
          canvas._clipboard.top += 15;
          canvas._clipboard.left += 15;
          canvas.setActiveObject(clonedObj);
          canvas.requestRenderAll();
        });
        e.ctrlKey=false
      }
  
    }
    else if(e.which === 65 && e.ctrlKey){
      e.preventDefault();
    
    }else
    if(e.which === 13 ){
      console.log(e.which);
      this.savedesigns()
      e.preventDefault();
      e.which=false;
  
    }
    else
    if(e.which === 83 && e.ctrlKey){
      // console.log(e.which);
      key=e.which;
    this.savedesigns()
      e.ctrlKey=false;
      e.preventDefault();
  
    }
  
  });
  
  }
  
  onMouseMove(event:any){
    if(this.canvas.backgroundImage){
      if(this.canvas.getActiveObject()){
        // this.Show()
      }
      var pointer = this.canvas.getPointer(event.e);
      var posX = pointer.x;
      var posY = pointer.y;
    if(posX>300||posX<300){

      if(this.canvas.getActiveObject()){
        this.canvas.getActiveObject().lockMovementX=true;
      }
      
      
    }
    
    }
  }





  upload= async ()=>{
    var node :any= document.getElementById('aladin-wraper');
  domtoimage.toPng(node).then( (dataUrl:any) =>{
      // Print the data URL of the picture in the Console
      console.log(dataUrl);

      // You can for example to test, add the image at the end of the document
      var img = new Image();
      this.currentface= dataUrl;
    // img.src=dataU
    // document.body.appendChild(img);
  }).catch(function (error:any) {
      console.error('oops, something went wrong!', error);
  });
  }


  drag(event:any){
    this.aladin.Ondragging(event);
  }


  createcanvas(){
    let doc:any=document.getElementById("aladin-wraper")
    const canvas = document.createElement("canvas")
    const div= document.createElement("div")
    div.draggable=true
    canvas.width = 250;
    canvas.height =250;
    let start:any;
    let end :any;
    canvas.id="cani"
    canvas.className="draggable"
    this.newcanvas=canvas
    canvas.draggable=true
    
    canvas.ondragstart=(e:any)=>{
      console.log(e)
      e.dataTransfer.setData("text", e.target.id);

    }

  canvas.ondragover=(e:any)=>{
    e.preventDefault()
    console.log(e)

    var data = e.dataTransfer.getData("text");

    e.target.appendChild(document.getElementById(data));

  }

    canvas.setAttribute("style","border:dashed 1px black;position:relative;top:5%,margin-left:80%;bottom:25%")
    div.setAttribute("style","border:dashed 1px red;position:relative;top:5%,margin-left:100%;bottom:25%;left:50% width:250;height:250")
    div.appendChild(canvas)
    doc.appendChild(div);


    this.aladin.addText(new fabric.Canvas(this.newcanvas.id,
      {
      // curser:pointer
      hoverCursor: 'move',
      selection: true,
      selectionBorderColor:'blue',
      fireRightClick: true,
      preserveObjectStacking: true,
      stateful:true
      }),"Bonjour")





  }

  }