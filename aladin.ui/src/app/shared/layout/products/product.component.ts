import { Component, OnInit,OnDestroy } from '@angular/core';
import { ListService } from 'src/app/core';
import { AladinService } from 'src/app/core';
declare  var require:any;
var myalert=require('sweetalert2');
var $ = require("jquery");
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
 clothes:any
 url="/editor/cloth/"
 data={}
show=true;

  constructor(private cltd:ListService,private aladin:AladinService) { }

  ngOnInit(): void {

    this.cltd.getclothespage().subscribe(
      res=>{
        this.clothes = res.data
        console.log(this.clothes);
      
      },
      err=>{
        console.log(err)
      }
    )
  }


  go(event:any){
     if(event){
       this.show=!this.show;
      Object.assign(this.data,{name:event.name_cloths,price:event.price,url:event.lib_img,show:this.show,category:"clothes",back:event.lib_back,size:JSON.parse(event.size),type:JSON.parse(event.type_imp)})
      this.aladin.ShowEditor.next(this.data)
      }

  }
 
}
