import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderUPComponent } from './header-up.component';

describe('HeaderUPComponent', () => {
  let component: HeaderUPComponent;
  let fixture: ComponentFixture<HeaderUPComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeaderUPComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderUPComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
