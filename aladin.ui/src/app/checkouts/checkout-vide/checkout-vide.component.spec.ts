import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckoutVideComponent } from './checkout-vide.component';

describe('CheckoutVideComponent', () => {
  let component: CheckoutVideComponent;
  let fixture: ComponentFixture<CheckoutVideComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CheckoutVideComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckoutVideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
