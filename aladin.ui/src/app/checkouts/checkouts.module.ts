import { SharedModule } from '../shared';
import { NgModule ,CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { CheckoutsRoutingModule } from './checkouts-routing.module';
import { CheckoutComponent } from './checkout/checkout.component';
import { CheckoutVideComponent } from './checkout-vide/checkout-vide.component';
import { PaiementComponent } from './paiement/paiement.component';


@NgModule({
  declarations: [
    CheckoutComponent,
    CheckoutVideComponent,
    PaiementComponent
  ],
  imports: [
    CheckoutsRoutingModule,
    SharedModule,
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class CheckoutsModule { }
