import { NgModule } from '@angular/core';
import { RouterModule, Routes,PreloadAllModules } from '@angular/router';
import { ConditiongComponent } from './cg/conditiong/conditiong.component';
import { ConditionuComponent } from './cu/conditionu/conditionu.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
const routes: Routes = [
  {
    path:'',
    redirectTo: '/home',
    pathMatch: 'full',
},
  {path:'',loadChildren:()=>import('./acceuil/acceuil.module').then(m=>m.AcceuilModule)},
  {path:'editor',loadChildren:()=>import('./editor/editor.module').then(m=>m.EditorModule)},
  {path:'users',loadChildren:()=>import('./user/user.module').then(m=>m.UserModule)},
  {path:'displays',loadChildren:()=>import('./affichage/affichage.module').then(m=>m.AffichageModule)},
  {path:'printed',loadChildren:()=>import('./imprimer/imprimer.module').then(m=>m.ImprimerModule)},
  {path:'cloths',loadChildren:()=>import('./clothes/clothes.module').then(m=>m.ClothesModule)},
  {path:'gadgets',loadChildren:()=>import('./gadgets/gadgets.module').then(g=>g.GadgetsModule)},
  {path:'packages',loadChildren:()=>import('./printed/printed.module').then(g=>g.PrintedModule)},
  {path:'cart',loadChildren:()=>import('./cart/cart.module').then(l=>l.CartModule)},
  {path:'cg', component:ConditiongComponent},
  {path:'cu', component:ConditionuComponent},
  {path:'**',
   component: PagenotfoundComponent
   }

];
@NgModule({
  imports: [RouterModule.forRoot(routes,
    {
      preloadingStrategy:PreloadAllModules,
      enableTracing:false
    })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
