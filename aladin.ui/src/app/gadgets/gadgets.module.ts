import { SharedModule } from '../shared';
import { CommonModule } from '@angular/common';
import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { GadgetsRoutingModule } from './gadgets-routing.module';
import { ListgadgetComponent } from './listgadget/listgadget.component';


@NgModule({
  declarations: [
    ListgadgetComponent
  ],
  imports: [
    CommonModule,
    GadgetsRoutingModule,
    SharedModule
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class GadgetsModule { }
