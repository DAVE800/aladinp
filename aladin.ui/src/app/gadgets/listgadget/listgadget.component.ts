import { Component, OnInit } from '@angular/core';
import { ListService } from 'src/app/core';
@Component({
  selector: 'app-listgadget',
  templateUrl: './listgadget.component.html',
  styleUrls: ['./listgadget.component.scss']
})
export class ListgadgetComponent implements OnInit {
  gadgets:any;
  url="/editor/gadgets/"
  shw=false
  constructor(private l :ListService) { }

  ngOnInit(): void {
    this.l.getGadgets().subscribe(
      res=>{
        this.gadgets=res;
      }
      ,
      err=>{
        console.log(err);
      }
    )
  }
  View(){
    let view = document.getElementById('view');
    view?.scrollIntoView({behavior:"smooth"})
   }
}
