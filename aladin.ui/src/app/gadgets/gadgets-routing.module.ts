import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListgadgetComponent } from './listgadget/listgadget.component';

const routes: Routes = [
  {path:'', component:ListgadgetComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GadgetsRoutingModule { }
