import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportCreaComponent } from './import-crea.component';

describe('ImportCreaComponent', () => {
  let component: ImportCreaComponent;
  let fixture: ComponentFixture<ImportCreaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImportCreaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportCreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
