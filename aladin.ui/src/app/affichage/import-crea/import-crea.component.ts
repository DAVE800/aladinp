import { Component, OnInit,Input,EventEmitter,Output } from '@angular/core';
import { isEmpty } from 'rxjs/operators';
import { LocalService } from 'src/app/core';
declare  var require:any;
var myalert=require('sweetalert2');

@Component({
  selector: 'app-import-crea',
  templateUrl: './import-crea.component.html',
  styleUrls: ['./import-crea.component.scss']
})
export class ImportCreaComponent implements OnInit {
@Input() url:any;
@Input() estbache:boolean=true;
@Input() estkake:boolean=true;
@Input() estvinyle:boolean=true;
@Input() estmicro:boolean=true;
changeit=true;
@Output() ChangeIt=new EventEmitter<boolean>()
quantite:any
t:any
Price:any;
pricestandbache=3000
pricesupbache=7000;
pricestandmicro=5000
pricesupmicro=10000
pricestandPkake=35000
pricesupPkake=45000
pricestandLkake=70000
pricesupLkake=80000
pricestandSD=3500
pricestandD=4500
pricesupSD=7000
pricesupD=8000
perimetre:any=1
longueur:any=1.00;
largeur:any=1.00
null=null
vide=0
impressionbache=false
impressionmicro=false
kakemono=false
vynile=false
petibas=false
largebas=false
dimension=false
hidestandvine=false
hidesupvine=false
file3:any
viewimage:any
//bache
standbache=false
supbache=false
//micro
standmicro=false
supmicro=false
//kakemono
standPkake=false
supPkake=false
standBkake=false
supBkake=false
//vynile
standSD=false
standD=false
supSD=false
supD=false
error=""
err=""
erreur=""
cmpt=0
cpt=0
  constructor(private localservice:LocalService) { }

  ngOnInit(): void {
    
  }
  Uplade(event:any){
    this.file3 =event.target.files[0]
    const reader = new FileReader();
  reader.onload = () => {
  
   this.viewimage= reader.result;
   
   };
   
   reader.readAsDataURL(this.file3);

     
   }
   
  Change2(event:any){
//bache
   if(this.standbache){
     
     if(this.longueur>=1){
      //this.Price= this.Price 
      this.perimetre=Math.ceil((parseFloat(this.longueur)) * (parseFloat(this.largeur)) )
      this.Price=this.pricestandbache * this.perimetre

      
    }
    
    if(this.longueur==null || this.largeur==null){
      this.perimetre=1
      this.Price=this.pricestandbache
    }
   }
   if(this.supbache){
     
    if(this.longueur>=1){
     //this.Price= this.Price 
     this.perimetre=Math.ceil((parseFloat(this.longueur)) * (parseFloat(this.largeur)) )
     this.Price=this.pricesupbache * this.perimetre
   }
   if(this.longueur==null || this.largeur==null){
     this.perimetre=1
     this.Price=this.pricesupbache
   }
  }
  
//micro
    if(this.standmicro){
        
      if(this.longueur>=1){
      //this.Price= this.Price 
      this.perimetre=Math.ceil((parseFloat(this.longueur)) * (parseFloat(this.largeur)) )
      this.Price=this.pricestandmicro * this.perimetre
      }

    if(this.largeur==null || this.longueur==null){

      console.log(this.Price)
      this.perimetre=1
      this.Price=this.pricestandmicro 
      }
    }
    if(this.supmicro){
    
    if(this.longueur>=1){
    //this.Price= this.Price 
    this.perimetre=Math.ceil((parseFloat(this.longueur)) * (parseFloat(this.largeur)) )
    this.Price=this.pricesupmicro * this.perimetre
      }
    if(this.longueur==null || this.largeur==null){
    this.perimetre=1
    this.Price=this.pricesupmicro
      }
    }
//fin micro
  //vinyle
  if(this.standSD){
    if(this.longueur>=1){
      //this.Price= this.Price 
      this.perimetre=Math.ceil((parseFloat(this.longueur)) * (parseFloat(this.largeur)) )
      this.Price=this.pricestandSD * this.perimetre
    }
    if(this.longueur==null || this.largeur==null){
      this.perimetre=1
      this.Price=this.pricestandSD
    }
  }
  if(this.standD){
    if(this.longueur>=1){
      //this.Price= this.Price 
      this.perimetre=Math.ceil((parseFloat(this.longueur)) * (parseFloat(this.largeur)) )
      this.Price=this.pricestandD * this.perimetre
    }
    if(this.longueur==null || this.largeur==null){
      this.perimetre=1
      this.Price=this.pricestandD
    }
  }
  if(this.supSD){
    if(this.longueur>=1){
      //this.Price= this.Price 
      this.perimetre=Math.ceil((parseFloat(this.longueur)) * (parseFloat(this.largeur)) )
      this.Price=this.pricesupSD * this.perimetre
    }
    if(this.longueur==null || this.largeur==null){
      this.perimetre=1
      this.Price=this.pricesupSD
    }
  }
  if(this.supD){
    if(this.longueur>=1){
      //this.Price= this.Price 
      this.perimetre=Math.ceil((parseFloat(this.longueur)) * (parseFloat(this.largeur)) )
      this.Price=this.pricesupD * this.perimetre
    }
    if(this.longueur==null || this.largeur==null){
      this.perimetre=1
      this.Price=this.pricesupD
    }
  }
  //fin vinyle
  }
  
Change(event:any){
  //baches
  if(this.standbache){
     
    if(this.largeur>=1){
     //this.Price= this.Price 
     this.perimetre=Math.ceil((parseFloat(this.longueur)) * (parseFloat(this.largeur)) )
     this.Price=this.pricestandbache * this.perimetre
   }
   
   if(this.largeur==null || this.longueur==null){

     console.log(this.Price)
     this.perimetre=1
    this.Price=this.pricestandbache 
   }
  }
  if(this.supbache){
     
    if(this.largeur>=1){
     //this.Price= this.Price 
     this.perimetre=Math.ceil((parseFloat(this.longueur)) * (parseFloat(this.largeur)) )
     this.Price=this.pricesupbache * this.perimetre
   }
   if(this.longueur==null || this.largeur==null){
     this.perimetre=1
     this.Price=this.pricesupbache
   }
  }
//fin baches

//micro
  if(this.standmicro){
     
      if(this.largeur>=1){
      //this.Price= this.Price 
      this.perimetre=Math.ceil((parseFloat(this.longueur)) * (parseFloat(this.largeur)) )
      this.Price=this.pricestandmicro * this.perimetre
      }
    
    if(this.largeur==null || this.longueur==null){

      console.log(this.Price)
      this.perimetre=1
      this.Price=this.pricestandmicro 
      }
  }
  if(this.supmicro){
     
    if(this.largeur>=1){
     //this.Price= this.Price 
     this.perimetre=Math.ceil((parseFloat(this.longueur)) * (parseFloat(this.largeur)) )
     this.Price=this.pricesupmicro * this.perimetre
   }
   if(this.longueur==null || this.largeur==null){
     this.perimetre=1
     this.Price=this.pricesupmicro
   }
  }
  //fin micro
  //kakemono
  if(this.standSD){
    if(this.largeur>=1){
      //this.Price= this.Price 
      this.perimetre=Math.ceil((parseFloat(this.longueur)) * (parseFloat(this.largeur)) )
      this.Price=this.pricestandSD * this.perimetre
    }
    if(this.longueur==null || this.largeur==null){
      this.perimetre=1
      this.Price=this.pricestandSD
    }
  }
  if(this.standD){
    if(this.largeur>=1){
      //this.Price= this.Price 
      this.perimetre=Math.ceil((parseFloat(this.longueur)) * (parseFloat(this.largeur)) )
      this.Price=this.pricestandD * this.perimetre
    }
    if(this.longueur==null || this.largeur==null){
      this.perimetre=1
      this.Price=this.pricestandD
    }
  }
  if(this.supSD){
    if(this.largeur>=1){
      //this.Price= this.Price 
      this.perimetre=Math.ceil((parseFloat(this.longueur)) * (parseFloat(this.largeur)) )
      this.Price=this.pricesupSD * this.perimetre
    }
    if(this.longueur==null || this.largeur==null){
      this.perimetre=1
      this.Price=this.pricesupSD
    }
  }
  if(this.supD){
    if(this.largeur>=1){
      //this.Price= this.Price 
      this.perimetre=Math.ceil((parseFloat(this.longueur)) * (parseFloat(this.largeur)) )
      this.Price=this.pricesupD * this.perimetre
    }
    if(this.longueur==null || this.largeur==null){
      this.perimetre=1
      this.Price=this.pricesupD
    }
  }
  //fin kakemono
}

showpricestandbache(){
 if(this.cmpt==0){ 
   this.cmpt=this.cmpt+1
  this.Price= this.pricestandbache
  this.standbache=true
 this.supbache=false
 this.supmicro=false
 this.standmicro=false
 this.standPkake=false
  this.supPkake=false
  this.standBkake=false
  this.supBkake=false
  this.standSD=false
  this.standD=false
  this.supSD=false
  this.supD=false
   }
}
showpricesupbache(){
     if(this.cpt==0){
    this.Price= this.pricesupbache 
    this.standbache=false
  this.supbache=true
  this.supmicro=false
  this.standmicro=false
  this.standPkake=false
  this.supPkake=false
  this.standBkake=false
  this.supBkake=false
  this.standSD=false
  this.standD=false
  this.supSD=false
  this.supD=false
     }
  
}

showpricestandmicro(){
      this.Price= this.pricestandmicro
      this.standbache=false
      this.supbache=false
      this.standmicro=true
      this.supmicro=false
      this.standPkake=false
  this.supPkake=false
  this.standBkake=false
  this.supBkake=false
  this.standSD=false
  this.standD=false
  this.supSD=false
  this.supD=false
      }
showpricesupmicro(){
        this.Price= this.pricesupmicro
        this.standmicro=false
        this.supmicro=true
        this.standbache=false
        this.supbache=false
        this.standPkake=false
        this.supPkake=false
        this.standBkake=false
        this.supBkake=false
        this.standSD=false
       this.standD=false
        this.supSD=false
        this.supD=false
        }
//espace kakemono
showpricestandPkake(){
this.Price=this.pricestandPkake
this.standPkake=true
  this.supPkake=false
  this.standBkake=false
  this.supBkake=false
  this.standSD=false
  this.standD=false
  this.supSD=false
  this.supD=false
  this.standmicro=false
  this.supmicro=false
  this.standbache=false
  this.supbache=false
}
showpricesupPkake(){
  this.Price=this.pricesupPkake
  this.standPkake=false
  this.supPkake=true
  this.standBkake=false
  this.supBkake=false
  this.standSD=false
  this.standD=false
  this.supSD=false
  this.supD=false
  this.standmicro=false
  this.supmicro=false
  this.standbache=false
  this.supbache=false
  }  
  showpricestandLkake(){
    this.Price=this.pricestandLkake
    this.standPkake=false
  this.supPkake=false
  this.standBkake=true
  this.supBkake=false
  this.standSD=false
  this.standD=false
  this.supSD=false
  this.supD=false
  this.standmicro=false
  this.supmicro=false
  this.standbache=false
  this.supbache=false
    }
    showpricesupLkake(){
      this.Price=this.pricesupLkake
  this.standPkake=false
  this.supPkake=false
  this.standBkake=false
  this.supBkake=true
  this.standSD=false
  this.standD=false
  this.supSD=false
  this.supD=
  this.standmicro=false
  this.supmicro=false
  this.standbache=false
  this.supbache=false
      } 
showkakemono(){
    this.dimension=false
    this.vynile=false
    this.kakemono=true
    this.impressionbache=false
    this.impressionmicro=false
  
  }

  //fin
showbache(){
  this.dimension=true
  this.vynile=false
  this.impressionbache=true
  this.impressionmicro=false
   this.kakemono=false
}
showmicro(){
  this.dimension=true
  this.vynile=false
  this.impressionbache=false
  this.impressionmicro=true
  this.kakemono=false
}
showpetitbas(){
this.petibas=true
this.largebas=false
}

showlargebas(){
  this.petibas=false
  this.largebas=true
}
//espace vynyle
showvynile(){
this.dimension=true
this.vynile=true
this.kakemono=false
this.impressionbache=false
this.impressionmicro=false

}
showhidestandvine(){
  this.hidestandvine=true
  this.hidesupvine=false
}
showhidesupvine(){
  this.hidestandvine=false
  this.hidesupvine=true
}
showpricestandSD(){
  this.Price=this.pricestandSD
  this.standSD=true
  this.standD=false
  this.supSD=false
  this.supD=false
  this.standmicro=false
  this.supmicro=false
  this.standbache=false
  this.supbache=false
  this.standPkake=false
  this.supPkake=false
  this.standBkake=false
  this.supBkake=false
}
showpricestandD(){
  this.Price=this.pricestandD
  this.standSD=false
  this.standD=true
  this.supSD=false
  this.supD=false
  this.standmicro=false
  this.supmicro=false
  this.standbache=false
  this.supbache=false
  this.standPkake=false
  this.supPkake=false
  this.standBkake=false
  this.supBkake=false
}
showpricesupSD(){
  this.Price=this.pricesupSD
  this.standSD=false
  this.standD=false
  this.supSD=true
  this.supD=false
  this.standmicro=false
  this.supmicro=false
  this.standbache=false
  this.supbache=false
  this.standPkake=false
  this.supPkake=false
  this.standBkake=false
  this.supBkake=false
}
showpricesupD(){
  this.Price=this.pricesupD
  this.standSD=false
  this.standD=false
  this.supSD=false
  this.supD=true
  this.standmicro=false
  this.supmicro=false
  this.standbache=false
  this.supbache=false
  this.standPkake=false
  this.supPkake=false
  this.standBkake=false
  this.supBkake=false
}

//fin
ChangeComponent(value:boolean){
  this.ChangeIt.emit(value)

}

AddTocart=()=>{
  //var data:any={face1:this.url,};
let cart:any

cart={

  face1:this.url,
  price:this.Price,
  qty: this.quantite,
  category:"affichage",
  type_product:"crea",
  t: this.Price * (parseInt(this.quantite))
  
}
if(this.estbache){
  Object.assign(cart, {
   
     name: "bache",
     longueur:this.longueur,
     largeur:this.largeur
   
  })
}
if(this.estkake){
  Object.assign(cart, {
    
     name: "Kakemono"
  
  })
}
if(this.estvinyle){
  Object.assign(cart, {
    
     name: "vinyle",
     longueur:this.longueur,
     largeur:this.largeur
  
  })
}
if(this.estmicro){
  Object.assign(cart, {
     name: "micro-perforé",
     longueur:this.longueur,
     largeur:this.largeur
  
  })
}
if(this.standbache){
  Object.assign(cart,{
    
      nome:"Standard",
      prix: this.pricestandbache

  })
}
if(this.supbache){
  Object.assign(cart,{
    
      nome:"Supérieur",
      prix: this.pricesupbache
   
  })
}
if(this.standmicro){
  Object.assign(cart,{
  
      nome:"Standard",
      prix: this.pricestandmicro
  
  })
}
if(this.supmicro){
  Object.assign(cart,{
   
      nome:"Supérieur",
      prix: this.pricesupmicro
  
  })
}
if(this.standPkake){
  Object.assign(cart,{
  
      nome:"Pétit bas standard ",
      prix: this.pricestandPkake
    
  })
}
if(this.supPkake){
  Object.assign(cart,{
   
      nome:"Pétit bas superieur ",
      prix: this.pricesupPkake
    
  })
}
if(this.standBkake){
  Object.assign(cart,{
    
      nome:"Large bas standard ",
      prix: this.pricestandLkake
    
  })
}
if(this.supBkake){
  Object.assign(cart,{
   
      nome:"Pétit bas superieur ",
      prix: this.pricesupLkake
    
  })
}
if(this.standSD){
  Object.assign(cart,{
    
      nome:"Standard sans découpe",
      prix: this.pricestandSD
    
  })
}
if(this.standD){
  Object.assign(cart,{
    
      nome:"Standard avec découpe",
      prix: this.pricestandD
  
  })
}
if(this.supSD){
  Object.assign(cart,{
   
      nome:"Supérieur sans découpe",
      prix: this.pricesupSD
  
  })
}
if(this.standSD){
  Object.assign(cart,{
   
      nome:"Superieur avec découpe",
      prix: this.pricesupD
    
  })
}
try {
  if(this.file3!=undefined){
    if((this.standbache || this.supbache || this.standmicro || this.supmicro || this.standPkake || this.supPkake
      || this.standBkake || this.supBkake || this.standSD || this.standD || this.supSD || this.supD)&&(this.file3!=undefined)){
      this.localservice.adtocart(cart);
      console.log(this.longueur, this.largeur)
      myalert.fire({
        title:'<strong>produit ajouté</strong>',
        icon:'success',
        html:
          '<h6 style="color:blue">Felicitation</h6> ' +
          '<p style="color:green">Votre design a été ajouté dans le panier</p> ' +
          '<a href="/cart">Je consulte mon panier</a>' 
          ,
        showCloseButton: true,
        focusConfirm: false,
       
      })
       console.log(cart)
    }else{
      if(this.estkake && this.standPkake==false && this.standBkake==false && this.supPkake==false && this.supBkake==false){
        this.err="Choisissez un type de support" 
        console.log(this.longueur, this.largeur)
      }else{
        this.err="Choisissez un type d'impression"
        console.log(this.longueur, this.largeur)
      }
      
    }
  }else{
    this.error="Veillez importer votre maquette"
    console.log(this.longueur, this.largeur)
  }
  
} catch (e:any) {
  console.log(e)
}

}

}
