import { Component, OnInit } from '@angular/core';
import { ListService } from 'src/app/core';
@Component({
  selector: 'app-affiche',
  templateUrl: './affiche.component.html',
  styleUrls: ['./affiche.component.scss']
})
export class AfficheComponent implements OnInit {
  disps:any;
  url="/editor/disps/";
  imagepreview:any;
  showchoices=false;
  text="Je veux imprimer ma créa!!!";
  estbache=false;
  estvinyle=false;
  estkake=false;
  estmicro=false;
  types=["Bache","Kakémono","micro-perforé","Vynile"];
  myfile="myfile"
  //isavailable=false
  data="Grand format : l'impression grand format est une impression réalisée sur de grands supports tels que: le kakemono, la bâche, le vinyle et le micro-perforé. La qualité de l'impression est meilleure avec des couleurs riches et vibrantes et une superbe reproduction des images."
  constructor( private l :ListService) { }
 affichage=true
 importcrea=false
  ngOnInit(): void {
    this.l.getDisps().subscribe(
      res=>{this.disps=res;},
      err=>{
        console.log(err);
      }
    );
   
  }
  showimportcrea(){
  this.affichage=false;
  this.importcrea=true;
  }

  changeComponent(value:boolean){
    this.affichage=value;
    this.importcrea=!value;
    this.estvinyle=false;
    this.estbache=false;
    this.estkake=false;
    this.estmicro=false;
    this.text="Je veux imprimer ma créa...!!!"
  }


  Upload(event:any){
   
    let file =event.target.files[0]
    const reader = new FileReader();
  reader.onload = () => {
  
   this.imagepreview = reader.result;
   
   };
   
   reader.readAsDataURL(file);
   this.showimportcrea()
 
    console.log(event)
 
  }


  showmechoices(){
  this.showchoices=true;
  }

  OnclickVinyle(){
    if(this.estvinyle==false){
      this.text="J'importe mon visuel de vinyle";
      this.showchoices=true;
      this.estmicro=false;
      this.estbache=false;
      this.estkake=false;
      this.estvinyle=true
    }else{
      this.showchoices=true;
      this.text="Je veux imprimer ma créa!!";
  
    }
  
  }

  OnclickMicro(){
    if(this.estmicro==false){
      this.text="J'importe mon visuel de micro-perforé";
      this.showchoices=true;
      this.estvinyle=false;
      this.estbache=false;
      this.estkake=false;
      this.estmicro=true
    
    }else{
      this.showchoices=true;
      this.text="Je veux imprimer ma créa!!";
  
    }
  
  }

  OnclickKake(){
    if(this.estkake==false){
      this.text="J'importe mon visuel de kakémono";
      this.showchoices=true;
      this.estvinyle=false;
      this.estbache=false;
      this.estmicro=false;
      this.estkake=true
    }else{
      this.showchoices=true;
      this.text="Je veux imprimer ma créa!!";
  
    }
  
  }

 
OnclickBache(){
  if(this.estbache==false){
    this.text="J'importe mon visuel de bache";
    this.showchoices=true;
    this.estvinyle=false;
    this.estmicro=false;
    this.estkake=false;
    this.estbache=true
  }else{
    this.showchoices=true;
    this.text="Je veux imprimer ma créa!!";

  }

}


 


}
