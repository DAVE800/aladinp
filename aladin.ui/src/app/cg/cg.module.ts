import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConditiongComponent } from './conditiong/conditiong.component';
import { SharedModule } from '../shared';



@NgModule({
  declarations: [
    ConditiongComponent
  ],
  imports: [
    CommonModule,
    SharedModule

  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class CGModule { }
