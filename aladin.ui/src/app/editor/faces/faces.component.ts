import { Component, OnInit ,Input,OnChanges,EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-faces',
  templateUrl: './faces.component.html',
  styleUrls: ['./faces.component.scss']
})
export class FacesComponent implements OnInit {
  Front="imgfront";
  Back="imgback";

@Input() URL:any;
@Input() face1:any;
@Input() back:any;
@Input() front:any;

@Output() newItemEvent =new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }

addNewItem(value: any) {
  this.newItemEvent.emit(value);
}
}
