import { ListService } from 'src/app/core';
import { Component, OnInit,Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-forme',
  templateUrl: './forme.component.html',
  styleUrls: ['./forme.component.scss']
})
export class FormeComponent implements OnInit {
forms:any=[];
pages=[1,2,3];
show=false;
current_page:any=1;
@Output() newItemEvent =new EventEmitter<string>();

  constructor(private http:ListService) { }

  ngOnInit(): void {
    this.http.getformes(this.current_page).subscribe(res=>{
      this.forms=res;
      console.log(this.forms.shapes);
    this.forms=this.forms.shapes
  },er=>{console.log(er)})
  }

  toggle(){
    this.show=!this.show
  }
  addNewItem(event:any){
    this.newItemEvent.emit(event);
  }
nextPage(){
  this.current_page= this.current_page + 1
  this.toggle()
  this.http.getformes(this.current_page).subscribe(res=>{
   let data :any=res;
      if(data.shapes.length>0){
        this.toggle();
         console.log(data.shapes)
           this.forms=data.shapes;
      }else{
        this.toggle();
      }
      
  },er=>{
    this.toggle()
    console.log(er)})

}

currentPage(event:any){
  var page=event.target.id
    if(+page){
      this.current_page=page
      this.toggle()
      this.http.getformes(page).subscribe(res=>{
        let data :any=res;
        if(data.status==200){
         if(data.shapes.length>0){
        this.toggle();
         console.log(data.shapes)
           this.forms=data.shapes;
      }else{
        this.toggle();
      }
        }else{
          this.toggle();
        }
     },er=>{
      this.toggle();
      console.log(er);
      })
    }

}

previousPage(){
  if (+this.current_page==1){

  }else{
    this.current_page = (+this.current_page)- 1
    this.toggle()
    this.http.getformes(this.current_page).subscribe(res=>{
      let data :any=res;
      if(data.shapes.length>0){
        this.toggle();
         console.log(data.shapes)
           this.forms=data.shapes;
      }else{
        this.toggle();
      }
   }, er=>{
    this.toggle()
     console.log(er)})
  }

}


}
