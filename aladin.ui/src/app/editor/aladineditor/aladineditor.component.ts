import { Component, OnInit ,OnChanges,ViewChild} from '@angular/core';
import { fabric } from 'fabric';
import { install } from 'chart-js-fabric';
install(fabric);
import { ActivatedRoute } from '@angular/router';
import {ListService,LocalService ,LoginService} from '../../core';
declare var require: any
var $ = require("jquery");

var FontFaceObserver = require('fontfaceobserver');
import { ColorEvent } from 'ngx-color';
var myalert=require('sweetalert2')

import { ContextMenuComponent } from 'ngx-contextmenu';

  @Component({
    selector: 'app-aladineditor',
    templateUrl: './aladineditor.component.html',
    styleUrls: ['./aladineditor.component.scss']
  })
  export class AladineditorComponent implements OnInit{
    canvas:any;
    s:any;
    face1:any;
    dt:any=[];
    selectedDay="blue";
    isbold:boolean =false;
    Text:any;
    url:any;
    stroke_size=1;
    strokecolor="#0B44A5"
    Textcolor="#0B44A5";
    stroke_color="blue"
    colorarray=['#D9E3F0', '#F47373', '#697689', '#37D67A', '#2CCCE4', '#555555',"#D2691E",'#dce775', '#ff8a65', '#ba68c8',"blue","yellow","orange","blacK","red","indigo","green","brown","#800080","#808000","#000080"];
    name:any;
    text_height:any;
    text_width:any;
    imgsrc:any;
    changecolor=false    
    public fontFace: any;
    imagePreview:any;
    fonts = ["Choix de police","Arial","roboto",'Tangerine','caveat',"poppins","Dancing Script","cinzel","Festive","satisfy","pacifico","Pinyon Script"];
    designed:any=[];
    para:any;
    prodid:any;
    iscat:any;
    Image:any;
    show:any=false;
    active:boolean=true;
    pback:any;
    pfront:any;
    spinner=false;
    colors:any=["indigo"];
    objectToSendBack:any;
    face2:any;
    origin:any;
    cactive:boolean=false;
    front_data:any=[];
    back_data:any=[];
    cart_length:any;
    textmenu=false;
    current_col:any;
    production_f1:any;
    production_f2:any;
    mobilescreen=false;
  @ViewChild(ContextMenuComponent) public basicMenu:any= ContextMenuComponent;

    constructor(private route: ActivatedRoute,private p:ListService,private L:LocalService,private auth:LoginService) { 
    }

  

   Show(){
  this.textmenu=true;
  }

  toggelSpinner(){
    this.spinner=!this.spinner
  }

  hide(){
    this.textmenu=false;

  }

texteclor($event:ColorEvent){
     let color = $event.color.hex;   

      let item= this.canvas.getActiveObject();

      if(item && item.type!="activeSelection"){
        item.set({fill:color});
        this.canvas.renderAll(item);
      }

      if(item && item.type=="activeSelection"){
        for(let el of item._objects){
          el.set({fill:color});
          this.canvas.renderAll(el);
        }
       }
    this.canvas.requestRenderAll();

    this.changeColor(color)

}


setStroke($event:any){
  let color=$event.color;
  console.log(color);
  console.log($event.width);
  let item =  this.canvas.getActiveObject();
  if(item && item.type!="activeSelection"){
    item.set({strokeWidth:1,stroke:color});
    this.canvas.renderAll(item);
  }
  if(item && item.type=="activeSelection"){
    for(let el of item._objects){
      el.set({strokeWidth:1,stroke:color});
      this.canvas.renderAll(el);
    }
  }
    this.canvas.requestRenderAll();
}

setshdow($event:any){
  console.log($event)
  let shadow = new fabric.Shadow({
    color: $event,
    blur: 20,
    offsetX: -4,
    offsetY: 3
  });

  let item =  this.canvas.getActiveObject();
  if(item && item.type!="activeSelection"){
    item.set({shadow:shadow});
    this.canvas.renderAll(item);
  }
  if(item && item.type=="activeSelection"){
    for(let el of item._objects){
      el.set({shadow:shadow});
      this.canvas.renderAll(el);
    }
  }
    this.canvas.requestRenderAll();
}

changeComplete($event:ColorEvent){
      let image;
      for(let elt of this.dt){
          if($event.color.hex==elt.lib){
            image=elt.lib_img;
            this.s=image;
            this.face1=elt.lib_back;
            this.current_col=$event.color.hex;
            break
            }
          }
       if(image!=undefined){
        this.canvas.backgroundImage=null;
        this.changecolor=true;
        this.makeImage(image);
        this.cactive=true;
        this.origin=null;
        this.face2=null;
        let idcol= document.getElementById("clr");
        this.p.triggerMouse(idcol)

      }

    }


    toggle(){
      this.show=!this.show;
    }


    OnRightClick(event:any){   
      let item = this.canvas.getActiveObject();
      if(item){
        var elt=document.getElementById('xdv')
        this.p.triggerMouse(elt)   
      }  
      return false
    }

    ngOnInit(): void { 
      $("body").children().first().before($(".modal"));

      this.cart_length=this.L.cart_items;
   // localStorage.removeItem("designs");
    this.cart_length=this.L.cart_items

    this.canvas = new fabric.Canvas('aladin-editor',{ 
      hoverCursor: 'pointer',
      selection: true,
      selectionBorderColor:'blue',
      fireRightClick: true,
      preserveObjectStacking: true,
      stateful:true
    }); 
  
    this.canvas.filterBackend=new fabric.WebglFilterBackend();

    this.canvas.on('mouse:move', (event:any)=>{
      this.onMouseMove(event);
    });
    this.canvas.on('mouse:down', (event:any)=>{
      this.onMouseDown();   

    });

    this.canvas.on('mouse:out', (event:any)=>{

      this.hide();   

    });
    this.canvas.on("selection:created", (event:any)=>{
      this.objectToSendBack = event.target;
      this.sendSelectedObjectBack()
    }); 
    this.canvas.on("selection:updated",(event:any)=>{
      this.objectToSendBack = event.target;
    });


  this.name=this.route.snapshot.paramMap.get('id');
  this.para =this.route.snapshot.paramMap.get('name');

  

   if(this.para=="cloth"){
    this.iscat=this.para;
    this.prodid=this.name;
    this.toggelSpinner();
    this.p.getcloth(this.name).subscribe(res=>{
      this.dt=res;  
      console.log(this.dt)
      for(let elt of this.dt){
       this.s=elt.front_side;
       this.face1=elt.back_side;
       if(this.colors.indexOf(elt.lib)==-1){
        this.colors.push(elt.lib);

       }
       if(+elt.is_back==0){
        this.imgsrc=elt.front_side;
        this.Text=JSON.parse(elt.data);
               
       }
       
      }
     // this.makeText(this.Text.fill,this.Text.fontFamily,this.Text.text,this.Text.top,this.Text.left,this.Text.height,this.Text.width,this.Text.stroke,this.Text.strokeWidth,this.Text.fontSize)
    this.makeImage(this.imgsrc);
 
    },
    err=>{
      this.toggelSpinner()
      console.log(err)}
    );
   }

   if(this.para=="gadgets"){
    this.iscat=this.para;
    this.prodid=this.name;
    this.toggelSpinner()

    this.p.getGadget(this.name).subscribe(
      res=>{
      this.dt=res;
      for(let elt of this.dt){
       this.s=elt.img;
       this.face1=elt.back_side;
       if(this.colors.indexOf(elt.lib)==-1){
        this.colors.push(elt.lib);

       }
        if(+elt.is_back==0){
        this.imgsrc=elt.front_side;
        this.Text=JSON.parse(elt.data);
       // this.makeText(this.Text.fill,this.Text.fontFamily,this.Text.text,this.Text.top,this.Text.left,this.Text.height,this.Text.width,this.Text.stroke,this.Text.strokeWidth,this.Text.fontSize)
     
       }
      
      }
      this.makeImage(this.imgsrc);

    },
    err=>{
      this.toggelSpinner()
      console.log(err);
    }
    );
   }

   let ofont:any=[]
   this.p.getfonts().subscribe(
     res=>{
       ofont=res
       if(ofont.status==200){
         for(let item of ofont.fonts){
           this.fonts.push(item.name);
           this.p.addStylesheetURL(item.url);
         }
         this.fonts.unshift('Times New Roman');
         var select = document.getElementById("family");
         this.fonts.forEach(function(font) {
         var opt = document.createElement('option');
          opt.innerHTML=font;
          opt.value=font;
          select?.appendChild(opt);
            });
            var select = document.getElementById("font-family");
            this.fonts.forEach(function(font) {
            var opt = document.createElement('option');
             opt.innerHTML=font;
             opt.value=font;
             select?.appendChild(opt);
            });
       }
     },
     err=>{
       console.log(err);
     }
   )
   
   if(this.para=="disps"){
    this.iscat=this.para;
    this.prodid=this.name;
    this.p.getDisp(this.name).subscribe(res=>{
      this.dt=res;
      for(let elt of this.dt){
       this.s=elt.img;
       if(this.colors.indexOf(elt.lib)==-1){
        this.colors.push(elt.lib);

       }
       this.face1=elt.back_side;
       if(+elt.is_back==0){
        this.imgsrc=elt.front_side;
        this.Text=JSON.parse(elt.data);
       // this.makeText(this.Text.fill,this.Text.fontFamily,this.Text.text,this.Text.top,this.Text.left,this.Text.height,this.Text.width,this.Text.stroke,this.Text.strokeWidth,this.Text.fontSize)
      
      }
      
      }
      this.makeImage(this.imgsrc);

    },
    err=>{console.log(err)}
    );
   }

   if(this.para=="prints"){
    this.iscat=this.para;
    this.prodid=this.name;
    this.p.getprint(this.name).subscribe(res=>{
      this.dt=res;
      for(let elt of this.dt){
       this.s=elt.img;
       if(this.colors.indexOf(elt.lib)==-1){
        this.colors.push(elt.lib);
       }
       this.face1=elt.back_side
       if(+elt.is_back==0){
        this.imgsrc=elt.front_side;
        this.Text=JSON.parse(elt.data);
        this.makeText(this.Text.fill,this.Text.fontFamily,this.Text.text,this.Text.top,this.Text.left,this.Text.height,this.Text.width,this.Text.stroke,this.Text.strokeWidth,this.Text.fontSize)
      }
       
      }
      this.makeImage(this.imgsrc);

    },
    err=>{console.log(err)}
    );
   }

   if(this.para=="packs"){
    this.iscat=this.para;
    this.prodid=this.name;
    this.p.getPack(this.name).subscribe(res=>{
      this.dt=res
      this.imgsrc=this.dt[0].front_side;
      this.s=this.imgsrc;
      this.face1=this.imgsrc;
      console.log(res)
      this.makeImage(this.imgsrc);
    },
    err=>{
      console.log(err)
    }
    );
   }





}

underline(){
  let item=this.canvas.getActiveObject()
  if(item && item.type!="activeSelection"){
    if(!item.underline){
      item.set({underline:true});
      this.canvas.renderAll(item);

    }else{
      item.set({underline:false});
      this.canvas.renderAll(item);
  
    }
  }

  if(item && item.type=="activeSelection"){

    for(let el of item._objects){
      if(!item.underline){

        el.set({underline:true});
        this.canvas.renderAll(el);

      }else{
        el.set({underline:false});
        this.canvas.renderAll(el);
      }
    }
  }
  this.canvas.requestRenderAll();

}

turnImage(event:any){
  let e=event.target.id;
 if(e=="imgback"){
  this.process_back(e);

 }

 if(e=="imgfront"){
  this.process_front(e);

 }
 
}


process_back(id:string){
  let src; 
  if(id=="imgback" && !this.active){
    let objts=this.canvas.getObjects();
    if(objts.length>0){
      for(let i=0;i<objts.length;i++){
        this.canvas.remove(objts[i]);
      }
     // this.canvas.renderAll();
    }
    if(this.back_data.length>0){  
      for(let elt of this.back_data){
        this.canvas.add(elt);
        this.canvas.renderAll(elt);
      }
        for(let elt of this.dt){
          if(this.cactive){
            if(+elt.is_back==1 && this.current_col==elt.lib){
              src=elt.lib_back;
              break;
            }

          }else{
            if(+elt.is_back==1){
              src=elt.back_side;
              console.log(src);

            break;
          
            }
            

          }
          
        
    }
    
    this.MakeImage(src);
    }else{
      for(let elt of this.dt){
        if(this.cactive){
          if(+elt.is_back==1 && this.current_col==elt.lib){
            src=elt.lib_back;
            this.Text=JSON.parse(elt.data);
            break;
          }
      
        }else{
          if(+elt.is_back==1){
            src=elt.back_side;
            this.Text=JSON.parse(elt.data);
           
            break;
          }
       
        }
        
      }

   // this.makeText(this.Text.fill,this.Text.fontFamily,this.Text.text,this.Text.top,this.Text.left,this.Text.height,this.Text.width,this.Text.stroke,this.Text.strokeWidth,this.Text.fontSize);     
      this.MakeImage(src);
    }
  }

}
  

process_front(e:string){
  console.log(e);
  let src;
  if(e=="imgfront" && this.active){
    let objts=this.canvas.getObjects()
    if(objts.length>0){
      for(let i=0;i<objts.length;i++){
        this.canvas.remove(objts[i]);
      }
     this.canvas.backgroundImage=null;

    }
    if(this.front_data.length>0){
  
      for(let elt of this.front_data){
        this.canvas.add(elt);
        this.canvas.renderAll(elt);
      }
        for(let elt of this.dt){
          if(this.cactive){
            if(+elt.is_back==0 && this.current_col==elt.lib){
              src=elt.lib_img;
              break;
            }

          }else{
            if(+elt.is_back==0){
              src=elt.img;
              this.Text=JSON.parse(elt.data);
              console.log(this.active);
              break;
            }

          }
  }
    this.MakeImage(src);

    }
    else{
      for(let elt of this.dt){
        if(this.cactive){
          if(+elt.is_back==0 && this.current_col==elt.lib){
            src=elt.lib_img;
            this.Text=JSON.parse(elt.data);
            break
          }
      
        }else{
          if(+elt.is_back==0){
            src=elt.img;
            this.Text=JSON.parse(elt.data);
             break;
          }
         

        }
        
      }
    //  this.makeText(this.Text.fill,this.Text.fontFamily,this.Text.text,this.Text.top,this.Text.left,this.Text.height,this.Text.width,this.Text.stroke,this.Text.strokeWidth,this.Text.fontSize);     
      this.MakeImage(src);
    }
  }

}


textwidthminus(event:any){
  let item = this.canvas.getActiveObject()
  item.set({width:+event,fontSize:+event});
   this.canvas.renderAll(item);
   this.canvas.requestRenderAll();
}
textwidthplusOne(event:any){
  let item = this.canvas.getActiveObject()
  item.set({width:+event,fontSize:+event});
   this.canvas.renderAll(item);
   this.canvas.requestRenderAll();
}

textwidth(event:any){
  let text_w =document.getElementById("R")
  let item = this.canvas.getActiveObject()
       item.set({width:+event,fontSize:+event});
        this.canvas.renderAll(item);
        this.canvas.requestRenderAll();

  text_w?.addEventListener("input",()=>{
    if(item){
      if(item.type!="activeSelection"){
        item.set({width:+event,fontSize:+event});
        this.canvas.renderAll(item);
        this.canvas.requestRenderAll();
      }
    }
    console.log(event);
});
}

selectChangeHandler(event: any) {
      let item= this.canvas.getActiveObject();
      this.Text=item;
      this.loadfont(event.target.value);
      if(this.Text && this.Text.type=="activeSelection"){
        for(let el of this.Text._objects){
          el.set({
            fontFamily:event.target.value
          });
          this.canvas.renderAll(el);
          this.canvas.requestRenderAll();

        }

      }

      if(this.Text && this.Text.type!="activeSelection"){
        this.Text.set({
          fontFamily:event.target.value
        });
        this.canvas.renderAll(this.Text);
        this.canvas.requestRenderAll();
      }
     

    }

    setTextBeforeEdit(){
      let text = this.canvas.getActiveObject()
      if(text && text.type!="activeSelection"){ 
        if(text.underline && text._textBeforeEdit){
          text.set({text:text._textBeforeEdit,underline:false});
          this.canvas.renderAll(text);

        }   
        if(text._textBeforeEdit){
          text.set({text:text._textBeforeEdit});
          this.canvas.renderAll(text);

        }
      } 
  
      if(text && text.type=="activeSelection"){
        for(let el of text._objects){
          if(el.underline && el._textBeforeEdit){
            el.set({text:el._textBeforeEdit,underline:false});
            this.canvas.renderAll(el);

          }   
          if(el._textBeforeEdit){
            el.set({text:el._textBeforeEdit});
            this.canvas.renderAll(el);
          }

        }
      }
      this.canvas.requestRenderAll();

    }

    removeItem(){
      let item = this.canvas.getActiveObject();
      if(item && item.type!="activeSelection"){
        this.canvas.remove(item);
      }

      if(item && item.type=="activeSelection"){
        for(let e of item._objects){
          this.canvas.remove(e);
        }
      }

    }
    

    MakeItalic(event:any){
      let item= this.canvas.getActiveObject();
      this.Text=item;
      if(this.Text!=undefined && this.Text.type=="activeSelection"){
        for(let el of this.Text._objects){
          if(el.fontStyle!="normal"){
            el.set({fontStyle:'normal'});
            this.canvas.renderAll(el)
           
          }else{
            el.set({fontStyle:'italic'});
            this.canvas.renderAll(el);
          }
        }
        
      }
      if(this.Text && this.Text!="activeSelection"){
        if(this.Text.fontStyle!="normal"){
          this.Text.set({fontStyle:"normal"});
          this.canvas.renderAll(this.Text);
        }else{
          this.Text.set({
            fontStyle:"italic"
          });
          this.canvas.renderAll(this.Text);
        }
      }
      this.canvas.requestRenderAll();

    }

    MakeBold(event:any){
      let item= this.canvas.getActiveObject();
      this.Text=item;  
      if(this.Text!=undefined && this.Text.type=="activeSelection"){    
          for(let el of this.Text._objects){
            if(el.fontWeight=="normal"){
            el.set({fontWeight:'bold'});
            this.canvas.renderAll(el);
          }else{
              el.set({fontWeight:'normal'});
              this.canvas.renderAll(el);        
          }
        }
        this.canvas.requestRenderAll();  
      }
        
      if(this.Text!=undefined && this.Text.type!="activeSelection"){
        if(this.Text.fontWeight=="normal"){   
          this.Text.set({fontWeight:'bold'});
            this.canvas.renderAll(this.Text);
            this.canvas.requestRenderAll();
         
        }else{
            this.Text.set({fontWeight:'normal'});
            this.canvas.renderAll(this.Text);
            this.canvas.requestRenderAll();   
        }

      }
    }

   sendSelectedObjectBack =()=>{
      this.canvas.sendToBack(this.objectToSendBack);
    }



    setImage(event:any){
      this.colors=["indigo"];
      this.back_data=[];
      this.front_data=[];
      let id=event.target.id;
      let m=event.target.name;
      var objt=this.canvas.getObjects()
      for(let i=0;i<objt.length;i++){
        this.canvas.remove(objt[i]);
      }
      this.canvas.backgroundImage=null;
      this.origin=null;
      this.face2=null;
      this.cactive=false;
      this.active=true;
      if(id!=undefined && m!=undefined){
        //vetements 
     
        if(m=="cloth"){
          this.iscat=m;
           this.prodid=id;           
           this.p.getcloth(id).subscribe(res=>{
          this.dt=res;   
          for(let elt of this.dt){
           this.s=elt.front_side;
           this.face1=elt.back_side;
            if(this.colors.indexOf(elt.lib)==-1){
            this.colors.push(elt.lib);         
          }
         if(+elt.is_back==0){
          this.imgsrc=elt.front_side;
          this.Text=JSON.parse(elt.data);
      }
       
      }
      this.MakeImage(this.imgsrc);
     // this.makeText(this.Text.fill,this.Text.fontFamily,this.Text.text,this.Text.top,this.Text.left,this.Text.height,this.Text.width,this.Text.stroke,this.Text.strokeWidth,this.Text.fontSize);
      this.p.triggerMouse(document.getElementById('closept')); 

    },
   err=>{
    if(this.spinner){
      this.toggelSpinner()
    }
    console.log(err);}
    );

    }
     //gadgets
      if(m=="gadgets"){
        this.iscat=m;
        this.prodid=id;
        this.p.getGadget(id).subscribe(res=>{
          this.dt=res;
          for(let elt of this.dt){
           this.s=elt.front_side;
           this.face1=elt.back_side;
           if(this.colors.indexOf(elt.lib)==-1){
            this.colors.push(elt.lib);
        }
           if(+elt.is_back==0){
            this.imgsrc=elt.front_side;
            this.Text=JSON.parse(elt.data);
          //  this.makeText(this.Text.fill,this.Text.fontFamily,this.Text.text,this.Text.top,this.Text.left,this.Text.height,this.Text.width,this.Text.stroke,this.Text.strokeWidth,this.Text.fontSize)    
            break

          }
          
          }
          this.makeImage(this.imgsrc);

        var elt=document.getElementById('closept');
        this.p.triggerMouse(elt); 
        },
        err=>{console.log(err)}
        );
       }
    
       //disps
       if(m=="disps"){
        this.iscat=m;
        this.prodid=id;
        this.p.getDisp(id).subscribe(res=>{
          this.dt=res;
          for(let elt of this.dt){
           this.s=elt.front_side;
           this.face1=elt.back_side;
           if(this.colors.indexOf(elt.lib)==-1){
            this.colors.push(elt.lib);
    
           }    
           if(+elt.is_back==0){
            this.imgsrc=elt.front_side;
            this.Text=JSON.parse(elt.data);
            this.makeText(this.Text.fill,this.Text.fontFamily,this.Text.text,this.Text.top,this.Text.left,this.Text.height,this.Text.width,this.Text.stroke,this.Text.strokeWidth,this.Text.fontSize)               }
           
          }
          this.makeImage(this.imgsrc)
          var elt=document.getElementById('closept');
        this.p.triggerMouse(elt); 
        },
        err=>{console.log(err);}
        );
       }
       //packs
       if(m=="packs"){
        this.iscat=m;
        this.prodid=id;
        this.p.getPack(id).subscribe(res=>{
          this.dt=res;
          for(let elt of this.dt){
           this.s=elt.front_side;
           this.face1=elt.back_side;
           if(this.colors.indexOf(elt.lib)==-1){
            this.colors.push(elt.lib);
          }
           if(+elt.is_back==0){
            this.imgsrc=elt.front_side;
            this.Text=JSON.parse(elt.data);
            this.makeText(this.Text.fill,this.Text.fontFamily,this.Text.text,this.Text.top,this.Text.left,this.Text.height,this.Text.width,this.Text.stroke,this.Text.strokeWidth,this.Text.fontSize)    
          }
           
          }
          this.makeImage(this.imgsrc);
          var elt=document.getElementById('closept');
        this.p.triggerMouse(elt); 
        },
        err=>{console.log(err)}
        );
       }
       //printed
       if(m=="prints"){
        this.iscat=m;
        this.prodid=id;
        this.p.getprint(id).subscribe(res=>{
          this.dt=res;
          for(let elt of this.dt){
           this.s=elt.front_side;
           this.face1=elt.back_side;
           if(this.colors.indexOf(elt.lib)==-1){
            this.colors.push(elt.lib);
    
           }
    
           if(+elt.is_back==0){
            this.imgsrc=elt.front_side;
            this.Text=JSON.parse(elt.data);
            this.makeText(this.Text.fill,this.Text.fontFamily,this.Text.text,this.Text.top,this.Text.left,this.Text.height,this.Text.width,this.Text.stroke,this.Text.strokeWidth,this.Text.fontSize);    
           }
          }
          this.makeImage(this.imgsrc)
          var elt=document.getElementById('closept');
        this.p.triggerMouse(elt); 
        },
        err=>{console.log(err);}
        );
       }
      }
    }



    MakeImage(src:any):any{
      var img = new Image();
      let canvas= this.canvas;
      img.src=src
      img.crossOrigin ='anonymous';
      this.Image=img;  
    

      img.onload = ()=> {
      var f_img = new fabric.Image(img);

      canvas.setBackgroundImage(f_img,canvas.renderAll.bind(canvas), {
              backgroundImageOpacity: 0.1,
              backgroundImageStretch: true,
              left:5,
              top:5,
              right:5,
              bottom:5,
              height:600,
              width:600,

          });
          canvas.centerObject(f_img);
          canvas.renderAll(f_img);
          canvas.requestRenderAll(); 
        }

  }

     makeImage(src:any):any{
      var img = new Image();
      let canvas= this.canvas;
      img.src=src
      img.crossOrigin ='anonymous';
      this.Image=img;  
      if(!this.spinner){
        this.toggelSpinner()
      }

      img.onload = ()=> {
      var f_img = new fabric.Image(img);

      canvas.setBackgroundImage(f_img,canvas.renderAll.bind(canvas), {
              backgroundImageOpacity: 0.1,
              backgroundImageStretch: true,
              left:5,
              top:5,
              right:5,
              bottom:5,
              height:600,
              width:600,

          });
          this.toggelSpinner();
          canvas.centerObject(f_img);
          canvas.renderAll(f_img);
          canvas.requestRenderAll(); 
        }

  }


  makeImageformobile(src:any):any{
    var img = new Image();
    let canvas= this.canvas;
    img.src=src
    img.crossOrigin ='anonymous';
    this.Image=img;        
    img.onload = function() {
    var f_img = new fabric.Image(img);
 
    canvas.setBackgroundImage(f_img,canvas.renderAll.bind(canvas), {
            backgroundImageOpacity: 0.1,
            backgroundImageStretch: true,
            left:0,
            top:0,
            right:0,
            bottom:0,
            height:600,
            width:600,

        });
        canvas.centerObject(f_img);
        canvas.renderAll(f_img);
        canvas.requestRenderAll();     
}

}

createText(event:any){
  this.Text= new fabric.IText("TEXTE",{
    top:200,
    left:200,
    fill:"blue",
    fontSize:38,
    fontStyle:'normal',
    cornerStyle:'circle',
    selectable:true,
    borderScaleFactor:1,
    overline:false
  });
  
  this.canvas.add(this.Text).setActiveObject(this.Text);
  this.canvas.renderAll(this.Text);
  this.canvas.requestRenderAll()
  this.canvas.centerObject(this.Text)
 
 
}

textoverline(event:any){
    let item = this.canvas.getActiveObject()
    if(item.type=="activeSelection"){
      for(let i of item){
        if(!i.overline){
          i.set({overline:true})
          this.canvas.renderAll(i);
          this.canvas.requestRenderAll()
        }else{
          i.set({overline:false});
          this.canvas.renderAll(i);
          this.canvas.requestRenderAll()
        }
      }
    }
    
    if(item.type!="activeSelection"){
        if(!item.overline){
          item.set({overline:true});
          this.canvas.renderAll(item);
          this.canvas.requestRenderAll()
        }else{
          item.set({overline:false});
          this.canvas.renderAll(item);
          this.canvas.requestRenderAll()
        }
      
    }
}

makenewText(fill:any,font:any,text:string,top:number,left:number,height:number,width:number,strok:string,strokwidth:number){
  this.Text= new fabric.IText(text,{
    top:top,
    left:left,
    fill:fill,
    fontSize:25,
    fontStyle:'normal',
    cornerStyle:'circle',
    fontFamily:font,

  });
  this.Text.set({width:width,height:height,stroke:strok,strokWidth:strokwidth})
  this.canvas.add(this.Text).setActiveObject(this.Text);
  this.canvas.centerObject(this.Text)
  this.canvas.renderAll(this.Text);
  this.canvas.requestRenderAll();


}

  makeText(fill:any,font:any,text:string,top:number,left:number,height:number,width:number,strok:string,strokwidth:number,fontsize:number){
      this.Text= new fabric.IText(text,{
        top:top,
        left:left,
        fill:fill,
        fontSize:fontsize,
        fontStyle:'normal',
        cornerStyle:'circle',
        fontFamily:font,
      });
      console.log(height,width,strok);
      this.Text.set({width:width,height:height,stroke:strok,strokWidth:strokwidth})
      this.canvas.add(this.Text).setActiveObject(this.Text);
      this.canvas.renderAll(this.Text);
      this.canvas.requestRenderAll();
  
  }


changeColor(color:any){
  let imgInstance=this.canvas.getActiveObject()
if(this.canvas.getActiveObject()._originalElement.localName==="img"){
  this.canvas.getActiveObject()._originalElement.setAttribute('width',350)
  this.canvas.getActiveObject()._originalElement.setAttribute('height',350)
  let   filter = new fabric.Image.filters.BlendColor({
    color: color,
   });
 filter.setOptions({
mode:"tint",
alpha:0.5,
scaleX:0.5,
scaleY:0.5
 })
  imgInstance.filters.push(filter);
  imgInstance.applyFilters();
 


}
 
}

setLogo(event:any){
 event.target.setAttribute("crossOrigin",'anonymous');
event.target.setAttribute("width",200);
event.target.setAttribute("height",200);
var imgInstance:any = new fabric.Image(event.target);
this.canvas.add(imgInstance).setActiveObject(imgInstance);
this.canvas.centerObject(imgInstance);
this.canvas.renderAll(imgInstance)
}

onFileUpload(event:any){
let file = event.target.files[0];
if(!this.handleChanges(file)){
  
  const reader = new FileReader();

 reader.onload = () => {
  let url:any = reader.result;
  fabric.Image.fromURL(url,(oImg) =>{
  oImg.set({  
   left: 100,
  top: 100,
  
  width:200,
  height:200
 });
  this.canvas.add(oImg).setActiveObject(oImg);
  this.canvas.centerObject(oImg);
  this.canvas.renderAll(oImg)

})
  };
  reader.readAsDataURL(file);
  
}
 
  }


loadfont(f:any){
    let fonts=new FontFaceObserver(f);
    fonts.load().then(function () {

    }).catch((err:any)=>{
      console.log(err);
    });
  }

Copy() {
  let canvas=this.canvas;
	canvas.getActiveObject().clone(function(cloned:any) {
		canvas._clipboard= cloned;
	});
}


Paste() {
  let canvas=this.canvas
	// clone again, so you can do multiple copies.
	canvas._clipboard.clone(function(clonedObj:any) {
		canvas.discardActiveObject();
		clonedObj.set({
			left: clonedObj.left + 15,
			top: clonedObj.top + 15,
			evented: true,
		});
		if (clonedObj.type === 'activeSelection') {
			clonedObj.canvas = canvas;
			clonedObj.forEachObject(function(obj:any) {
				canvas.add(obj);
			});
			clonedObj.setCoords();
		} else {
			canvas.add(clonedObj);
		}
		canvas._clipboard.top += 15;
		canvas._clipboard.left += 15;
		canvas.setActiveObject(clonedObj);
		canvas.requestRenderAll();
	});
}


savedesign(event:any){

  if(this.active){
    let items = this.canvas.getObjects();
    for(let el of items){
      this.front_data.push(el);
    }
      this.origin=this.canvas.toDataURL("png");
      this.s=this.origin;
      this.active=!this.active;
      this.canvas.backgroundImage=null;
      this.production_f1=this.canvas.toDataURL()
     
      var elt=document.getElementById('imgback');
      this.p.triggerMouse(elt);  
    

  }
  
  else{
    this.face2=this.canvas.toDataURL("png");
    this.face1=this.face2;
    let items = this.canvas.getObjects();
    for(let el of items){
      this.back_data.push(el);
    }
    this.canvas.backgroundImage=null;
    this.production_f2=this.canvas.toDataURL()
    this.active=!this.active;
    var elt=document.getElementById('imgfront');
    this.p.triggerMouse(elt);
  
 }
 
if(this.origin && this.face2){
 let dataset={}; 
 let prod_info={};
   for(let item of this.dt){
     Object.assign(prod_info,{
      name:item.name_cloths,
      price:item.price,
      size:JSON.parse(item.size),
      type:JSON.parse(item.type_imp),
      material:item.material,
      lib_img:item.lib_img,
      lib_back:item.lib_back,
      front:item.front_side,
      back:item.back_side,
      user_id:item.user_id,
      staff:item.staff,
      id:item.clt_id
      
     })
   break;
 }
 if(this.L.items.length==0){
  Object.assign(dataset,{face1:this.origin,face2:this.face2,text1:this.front_data,text2:this.back_data,prod:prod_info,production_f1:this.production_f1,production_f2:this.production_f2,category:"clothes",index:0});

 }else{
  Object.assign(dataset,{face1:this.origin,face2:this.face2,text1:this.front_data,text2:this.back_data,prod:prod_info,production_f1:this.production_f1,production_f2:this.production_f2,category:"clothes",index:this.L.items.indexOf(this.L.items[this.L.items.length-1])+1});

 }
 try{
   if(this.L.items.length>8){
    myalert.fire({
      title:"quota de stockage ",
      icon: 'info',
      html:"<h2 style='color:red;font-Size:12px'>Vous ne pouvez plus enregistrer de designs dans l'espace local, veuillez supprimer certains de vos designs ou passer au panier pour passer votre commande, afin de pouvoir poursuivre vos operations sur aladin.ci </h2>"
    }
    )
   }else{
    this.L.add(dataset);
   
   }
 }catch(err:any){
   if(err.code==22){
    myalert.fire({
      title:"quota de stockage ",
      icon: 'info',
      html:"<h2 style='color:red;font-Size:12px'>Vous ne pouvez plus enregistrer de designs dans l'espace local, veuillez supprimer certains de vos designs ou passer au panier pour passer votre commande, afin de pouvoir poursuivre vos operations sur aladin.ci </h2>"
    }
    )
      console.log(err);
   }else if(err=="QUOTA_EXCEEDED_ERR"){
    myalert.fire({
      title:"quota de stockage ",
      icon: 'info',
      html:"<h2 style='color:red;font-Size:12px'>Vous ne pouvez plus enregistrer de designs dans l'espace local, veuillez supprimer certains de vos designs ou passer au panier pour passer votre commande, afin de pouvoir poursuivre vos operations sur aladin.ci </h2>"
    }
    )
   }
 
 }


}


}

onMouseDown=()=>{
 let canvas= this.canvas;
 let key=0
$(document).keydown((e:any)=>{
  if( e.which === 89 && e.ctrlKey){
     console.log(e.which,e.ctrlKey);
  }
  else if(e.which === 90 && e.ctrlKey){
     console.log(e.which,e.ctrlKey)
  } else if(e.which === 67 && e.ctrlKey){
    console.log(e.which,e.ctrlKey)
    canvas.getActiveObject().clone(function(cloned:any) {
      canvas._clipboard= cloned;
    });

  }

  else if(e.which === 86 && e.ctrlKey){
    console.log(e.which,e.ctrlKey);
    if(e.ctrlKey)
    {
      canvas._clipboard.clone(function(clonedObj:any) {
        canvas.discardActiveObject();
        clonedObj.set({
          left: clonedObj.left + 15,
          top: clonedObj.top + 15,
          evented: true,
        });
        if (clonedObj.type === 'activeSelection') {
          // active selection needs a reference to the canvas.
          clonedObj.canvas = canvas;
          clonedObj.forEachObject(function(obj:any) {
            canvas.add(obj);
          });
          // this should solve the unselectability
          clonedObj.setCoords();
        } else {
          canvas.add(clonedObj);
        }
        canvas._clipboard.top += 15;
        canvas._clipboard.left += 15;
        canvas.setActiveObject(clonedObj);
        canvas.requestRenderAll();
      });
      e.ctrlKey=false
    }

  }
  else if(e.which === 65 && e.ctrlKey){
    e.preventDefault();
  
  }else
  if(e.which === 13 ){
    console.log(e.which);
    this.savedesign(e)
    e.preventDefault();
    e.which=false;

  }
  else
  if(e.which === 83 && e.ctrlKey){
   // console.log(e.which);
    key=e.which;
    this.savedesign(e)
    e.ctrlKey=false;
    e.preventDefault();

  }

});

}

onMouseMove(event:any){
  if(this.canvas.backgroundImage){
    if(this.canvas.getActiveObject()){
      this.Show()
    }
    var pointer = this.canvas.getPointer(event.e);
    var posX = pointer.x;
    var posY = pointer.y;
  if(posX>300||posX<300){
   /**
     if(this.canvas.getActiveObject()){
      this.canvas.getActiveObject().lockMovementX=true;
    }
    */ 
   
  }
 
  }
}


handleChanges(file:any):Boolean {
  var fileTypes = [".png"];
  var filePath = file.name;
  if (filePath) {
               var filePic = file; 
               var fileType = filePath.slice(filePath.indexOf(".")); 
               var fileSize = file.size; //Select the file size
               if (fileTypes.indexOf(fileType) == -1) { 
                       myalert.fire({
                        title: "Erreur",
                        text: `le format ${fileType} n'est pas autorisé utilisez le format .png`,
                        icon: "error",
                         button: "Ok"
                        });
                    return true;
               }

      if (+fileSize > 200*200) {
        myalert.fire({
          title: "Erreur",
          text: `importer un fichier de taille ${200}x${200} MG!`,
          icon: "error",
           button: "Ok"
          });
          return true;
      }

      var reader = new FileReader();
      reader.readAsDataURL(filePic);
      reader.onload =  (e:any) =>{
          var data = e.target.result;
                       
          var image = new Image();
          image.onload = ():any =>{
              var width = image.width;
              var height = image.height;
              if(width == 200 && height == 200) {                        
                return false ;
                 
              } else {
                   myalert.fire({
                    title: "Erreur",
                    text: `la taille doit etre ${200}x${200}`,
                    icon: "error",
                     button: "Ok"
                    });
                  return true ;
              }


          };
        };
  } else {

      return true;
      
  }

  return false;
}

  }