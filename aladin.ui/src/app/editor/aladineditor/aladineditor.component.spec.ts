import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AladineditorComponent } from './aladineditor.component';

describe('AladineditorComponent', () => {
  let component: AladineditorComponent;
  let fixture: ComponentFixture<AladineditorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AladineditorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AladineditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
