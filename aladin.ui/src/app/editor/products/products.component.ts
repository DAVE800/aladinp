import { Component, OnInit, Output,EventEmitter ,Input} from '@angular/core';
import { ListService } from 'src/app/core';




@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
cltobj:any;
prints:any;
gadgets:any;
packs:any;
disps:any;
@Input() id:any;
@Input() name:any
cloth="cloth";
print="prints"
pack="packs";
gadget="gadgets";
disp="disps";
current=1;
current_disp=1
current_gad=1;
current_print=1;
current_pack:any=1;
pages=[1,2,3,4,5];
loading=false;

@Output() newItemEvent =new EventEmitter<string>();

  constructor(private p: ListService) { }

  ngOnInit(): void {
    this.current=this.pages[0]
    this.p.getclt(this.current).subscribe(
      res=>{
    this.cltobj=res;
    this.cltobj= this.cltobj.data
    },
    err=>{
      console.log(err);
    }
    );
    //prints
    

    this.p.getPrints().subscribe(
      res=>{
      
     this.prints=res;
    },
    err=>{
      console.log(err);
    }
    );

    this.p.getGadgets().subscribe(
      res=>{
     this.gadgets=res;
    },
    err=>{
      console.log(err);
    }
    );

    this.p.getDisps().subscribe(
      res=>{
     this.disps=res;
    },
    err=>{
      console.log(err);
    }
    );


    this.p.getPacks().subscribe(
      res=>{
     this.packs=res;
    },
    err=>{
      console.log(err);
    }
    );

  }

  addNewItem(value: any) {
    this.newItemEvent.emit(value);
  }
  //pagination vetement
  currentpage(event:any){
    event.className="page-item "
    console.log(event)
    let id=event.target.id
    this.current=id
    console.log(event.target);
    if(this.loading){
      this.onloading()

    }
    this.onloading()
    this.p.getclt(id).subscribe(
      res=>{
        let data=res;
        if(data.data.length>0){
          this.onloading()
          this.cltobj=res;
          this.cltobj= this.cltobj.data
          console.log(res)
        }
        
        else{
          this.onloading()
    
        }
        
    },
    err=>{
     this.onloading()
    })
  }

 nextPage(){
 this.current=(+this.current)+1;
 if(this.loading){
  this.onloading()

}
this.onloading()
 this.p.getclt(this.current).subscribe(
  res=>{
    let data=res;
    if(data.data.length>0){
      this.onloading()
      this.cltobj=res;
      this.cltobj= this.cltobj.data;
      console.log(res);
    }else{
      this.onloading()

    }
    
},
err=>{
 this.onloading()
}
)

}

previouspage(){
  if(+this.current>1&&this.current!=undefined){
  this.current=(+this.current)-1;
  if(this.loading){
    this.onloading()

  }
  this.onloading()
 this.p.getclt(this.current).subscribe(
  res=>{
    let data=res;
    if(data.data.length>0){
      this.onloading()
      this.cltobj=res;
      this.cltobj= this.cltobj.data
      console.log(res)
    }
    
    else{
      this.onloading()

    }
    
},
err=>{
 this.onloading()
}

)
  }
}

//prints
currentpages1(event:any){
  event.className="page-item active "
  let id=event.target.id
  this.current_print=id
  this.p.getprints(this.current_print).subscribe(
    res=>{
      this.prints=res;
      console.log(this.prints)
  })
}

currentpages2(event:any){
  event.className="page-item active"
  let id=event.target.id
  this.current_disp=id
  this.p.getdisps(this.current_disp).subscribe(
    res=>{
      this.disps=res;
      console.log(res)
  })
}

nextpage(){
  if(this.current_disp!=undefined){
    this.current_disp=(+this.current_disp)+1
  }else{
    this.current_disp=1
  }
 
  this.p.getdisps(this.current_disp).subscribe(
   res=>{
     this.disps=res;
     console.log(res)
 })
 
}

previousdisp(){
if(this.current_disp !=undefined && this.current_disp>1){
  this.current_disp=(+this.current_disp)-1
  this.p.getdisps(this.current_disp).subscribe(
   res=>{
     this.disps=res;
     console.log(res)
 })

}
}


previousprint(){
  if(this.current_print !=undefined && this.current_print>1){
    this.current_print=(+this.current_print)-1
    this.p.getprints(this.current_print).subscribe(
     res=>{
       this.prints=res;
       console.log(res)
   })
  
  }
  }

nextprint(){
  if(this.current_print!=undefined){
    this.current_print=(+this.current_print)+1
  }else{
    this.current_print=1
  }
 
  this.p.getprints(this.current_print).subscribe(
   res=>{
     this.prints=res;
     console.log(res)
 })
}

currentpack(event:any){
  event.className="page-item active"
  let id=event.target.id
  this.current_pack=id
  this.p.getpacks(this.current_pack).subscribe(
    res=>{
      this.packs=res;
      console.log(res)
  })
}

nextpack(){
  if(this.current_pack!=undefined){
    this.current_pack=(+this.current_pack)+1
  }else{
    this.current_pack=1
  }
 
  this.p.getpacks(this.current_pack).subscribe(
   res=>{
     this.packs=res;
     console.log(res)
 })
}



previouspack(){
  if(this.current_pack !=undefined && this.current_pack>1){
    this.current_pack=(+this.current_pack)-1
    this.p.getpacks(this.current_pack).subscribe(
     res=>{
       this.packs=res;
       console.log(res);
   })
  
  }
  }

  currentgadget(event:any){
    event.className="page-item active"
    let id=event.target.id
    this.current_gad=id
    this.p.getgadgets(this.current_gad).subscribe(
      res=>{
        this.gadgets=res;
        console.log(res);
    })
  } 

  previousgadget(){
    if(this.current_gad !=undefined && this.current_gad>1){
      this.current_gad=(+this.current_gad)-1
      this.p.getgadgets(this.current_gad).subscribe(
       res=>{
         this.gadgets=res;
         console.log(res);
     })
    
    }
    }

    nextgadget(){
      if(this.current_gad!=undefined){
        this.current_gad=(+this.current_gad)+1
      }else{
        this.current_gad=1
      }
     
      this.p.getgadgets(this.current_gad).subscribe(
       res=>{
         this.gadgets=res;
         console.log(res)
     })
      }


onloading(){
  this.loading=!this.loading;
}

}
