import { Component, OnInit } from '@angular/core';
import { ListService } from 'src/app/core';
@Component({
  selector: 'app-listprint',
  templateUrl: './listprint.component.html',
  styleUrls: ['./listprint.component.scss']
})
export class ListprintComponent implements OnInit {
packs:any;
url="/editor/packs/";
shw=true
uploadedurl:any;
details:any;
crea=false;
showchoices=false;
estsac=false;
estsachet=false;

text="Je veux imprimer ma créa !!"

  constructor(private l:ListService) { }

  ngOnInit(): void {
    this.l.getPacks().subscribe(res=>{
      this.packs=res
      console.log(res)
    },err=>{console.log(err)})
  }
  View(){
    let view = document.getElementById('view');
    view?.scrollIntoView({behavior:"smooth"})
   }


   OnclickSac(){
    if(this.estsac==false){
      this.text="J'importe mon visuel de sac";
      this.showchoices=true;
      this.estsachet=false;
      this.estsac=true
    }else{
      this.showchoices=true;
      this.text="Je veux imprimer ma créa!!";
  
    }
  

   }


  
displaychoices(){
  this.showchoices=true;
}

   OnclickSachet(){
    if(this.estsachet==false){
      this.text="J'importe mon visuel de sachet";
      this.showchoices=true;
      this.estsachet=true;
      this.estsac=false
    }else{
      this.showchoices=true;
      this.text="Je veux imprimer ma créa!!";
  
    }
   }


   ChangeComponent(value:boolean){
     this.shw=value;
     this.showchoices=false;
     this.text="Je veux imprimer ma créa !!";
     this.estsac=false;
     this.estsachet=false;


   }

   Upload(event:any){
    let file =event.target.files[0]
    const reader = new FileReader();
  reader.onload = () => {
  
   this.uploadedurl = reader.result;
   
   };
   reader.readAsDataURL(file);

   this.show()
    console.log(event)
 
  }

   Changecomponent(value:boolean){
    this.shw=value;
 


   }



show(){
  this.crea=true;
  this.shw=false;
}
  
letchange(value:boolean){
  this.shw=value
  this.crea=!value
}


showDetails(event:any){
  let id= event.target.id;
  if(id){
    this.shw=!this.show
    for(let el of this.packs){
      this.details={id:id}
      id= +id;
      if(el.pack_id==id){
Object.assign(
  this.details,{url:el.img,comment:el.comment,price:el.price,size:el.dim,name:el.name_packaging,show:true,qty:el.qty});
  break;

      }
    }
  }
}

Showaladin(data:any){
  Object.assign(this.details,data)
  console.log(data)
  
}

}
