import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashpartnerComponent } from './dashpartner.component';

describe('DashpartnerComponent', () => {
  let component: DashpartnerComponent;
  let fixture: ComponentFixture<DashpartnerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashpartnerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashpartnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
