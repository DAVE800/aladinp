import { Component, OnInit } from '@angular/core';
import { ListService ,AuthinfoService} from 'src/app/core';
@Component({
  selector: 'app-dashpartner',
  templateUrl: './dashpartner.component.html',
  styleUrls: ['./dashpartner.component.scss']
})
export class DashpartnerComponent implements OnInit {
  myvar=true;
  myvar2=true;
  selected:any;
  cloths={
    name:"T-SHIRT",
    size:"XL",
    color:"JAUNE",
    mode:"HOME",
    price:0,
    mat:"MAT",
    type_imp:"IU",
    comment:"JE VEUS"
  }
  show=false

  gadgets={
    name_gadget:"mon gadget",
    price:800,
    dim:4.8,
    size:45,
    diam:5.3,
    material:"gdg",
    user_id:2,
    comment:"JE VEUS"

  }

  disps={
    name_display:"Affichage",
    price:500,
    dim:8.5,
    gram:75,
    user_id:2,
    comment:"JE VEUS "
  };

  packs={
    name_packaging:"sac à dos",
    price:500,
    dim:8.5,
    material:"materiel",
    user_id:2,
    comment:"JE VEUS "
  };
  prints={
    name_printed:"carte de visite",
    price:500,
    dim:8.5,
    gram:75,
    pellicule:"pellicule",
    border:"bordure",
    volet:45,
    verni:"verni",
    user_id:2,
    comment:"JE VEUS "
  };
  user_id:any;
  message:any;
  succeed=false;
  constructor(private l:ListService,private INFO:AuthinfoService) { }

  ngOnInit(): void {
  }
  cache(){
    this.myvar=!this.myvar
  }
  cache2(){
    this.myvar2=!this.myvar2
  }

  logout(){
    localStorage.removeItem('access_token');
    location.href="/partners"
  }
  OnfileSelect(event:any){
    this.selected=event.target.files[0];  
  }


  Addcloths(){
    this.user_id = this.INFO.getItem('part');
    this.user_id=+JSON.parse(this.user_id)
    let data={
      name_cloths:this.cloths.name,
      price:this.cloths.price,
      size:this.cloths.size,
      mode:this.cloths.mode,
      comment:this.cloths.comment,
      material:this.cloths.mat,
      type_imp:this.cloths.type_imp,
      user_id:this.user_id,
      color:this.cloths.color
      };
      this.toggle()

    this.l.SaveCloth(data,this.selected).subscribe(
    resp=>{
      console.log(resp);
      this.toggle()
      this.succeed=!this.succeed;
      this.message="Le produit a été ajouté avec succès";

    },
    err=>{
      this.toggle()
      console.log(err);
    }
  );

  }
toggle(){
  this.show=!this.show
}

  AddGadget(){
    this.user_id = this.INFO.getItem('part');
    this.gadgets.user_id= +JSON.parse(this.user_id)
    this.toggle()
    this.l.SaveGadget(this.gadgets,this.selected).subscribe(
    resp=>{
      console.log(resp);
      this.toggle();
      this.succeed=!this.succeed;
      this.message="Le produit a été ajouté avec succès";

    },
    err=>{
      this.toggle()
      console.log(err);
    }
  );

  }

  Addprinted(){
    this.user_id = this.INFO.getItem('part');
    this.prints.user_id= +JSON.parse(this.user_id);
    this.toggle()
    this.l.SavePrint(this.prints,this.selected).subscribe(
    resp=>{
      this.toggle()

      console.log(resp);
      //location.reload();
      this.succeed=!this.succeed;
      this.message="Le produit a été ajouté avec succès";

    },
    err=>{
      this.toggle()

      console.log(err);
    }
  );

  }

  Addpack(){
    this.user_id = this.INFO.getItem('part');
    this.packs.user_id= +JSON.parse(this.user_id)
    this.toggle();

    this.l.SavePack(this.packs,this.selected).subscribe(
    resp=>{
      this.toggle()

      console.log(resp);
      //location.reload();
      this.succeed=!this.succeed;
      this.message="Le produit a été ajouté avec succès";

    },
    err=>{
      this.toggle()

      console.log(err);
    }
  );

  }

  Adddisp(){
    this.user_id = this.INFO.getItem('part');
    this.disps.user_id= +JSON.parse(this.user_id)
    this.toggle();
    this.l.SaveDisp(this.disps,this.selected).subscribe(
    resp=>{
      this.toggle()

      console.log(resp);
      //location.reload();
      this.succeed=!this.succeed;
      this.message="Le produit a été ajouté avec succès";

    },
    err=>{
      this.toggle()
      console.log(err);
    }
  );

  }




  
}
