import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PartnerRoutingModule } from './partner-routing.module';
import { HomePartnerComponent } from './home-partner/home-partner.component';
import{SharedModule} from  '../shared';
import { RegisterPartnerComponent } from './register-partner/register-partner.component';
import { LoginPartnerComponent } from './login-partner/login-partner.component';
import { ForgotpwdComponent } from './forgotpwd/forgotpwd.component';
import { DashpartnerComponent } from './dashpartner/dashpartner.component';

@NgModule({
  declarations: [
    HomePartnerComponent,
    RegisterPartnerComponent,
    LoginPartnerComponent,
    ForgotpwdComponent,
    DashpartnerComponent
  ],
  imports: [
    CommonModule,
    PartnerRoutingModule,
    SharedModule
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class PartnerModule { }
