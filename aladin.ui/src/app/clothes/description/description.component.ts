import { Component, OnInit,Input,OnChanges ,EventEmitter,Output} from '@angular/core';

@Component({
  selector: 'app-description',
  templateUrl: './description.component.html',
  styleUrls: ['./description.component.scss']
})

export class DescriptionComponent implements OnInit,OnChanges {

@Input() data:any;
@Output() changeComponent=new EventEmitter<boolean>();
Changevalue=true
url="/editor/cloth/"

ngOnChanges():void{
  console.log(this.data)

}

  constructor() { }

  ngOnInit(): void {
    this.data.show=false;

    

  }

  go(){
    Object.assign(this.data,{aladin:true,category:"clothes"});
    this.data.show=false;
}

reload(value:boolean){
 this.data.show=!this.Changevalue
  this.changeComponent.emit(value)

}

}
