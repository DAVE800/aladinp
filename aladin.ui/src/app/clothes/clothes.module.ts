import { NgModule ,CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { ClothesRoutingModule } from './clothes-routing.module';
import { SharedModule } from '../shared';
import { VetementComponent } from './vetement/vetement.component';
import { CreationComponent } from './creation/creation.component';
import { DescriptionComponent } from './description/description.component';


@NgModule({
  declarations: [VetementComponent, CreationComponent, DescriptionComponent ],
  imports: [
    ClothesRoutingModule,
    SharedModule
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class ClothesModule { }
