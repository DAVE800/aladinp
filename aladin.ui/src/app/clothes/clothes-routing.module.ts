import { NgModule } from '@angular/core';
import { VetementComponent } from './vetement/vetement.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {path:'', component:VetementComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClothesRoutingModule { }
