import { Component, OnInit,Input,Output,EventEmitter } from '@angular/core';
//import { emit } from 'process';
import { LocalService } from 'src/app/core';
import Swal from 'sweetalert2'


@Component({
  selector: 'app-creation',
  templateUrl: './creation.component.html',
  styleUrls: ['./creation.component.scss']
})
export class CreationComponent implements OnInit {
  constructor(private localservice:LocalService) { 

  }
  @Input() url:any;
  @Output() newplusMEvent=new EventEmitter<any>();
  @Output() newminusMEvent=new EventEmitter<any>();
  @Output() newplusSEvent=new EventEmitter<any>();
  @Output() newminusSEvent=new EventEmitter<any>();
  @Output() newplusLEvent=new EventEmitter<any>();
  @Output() newminusLEvent=new EventEmitter<any>();
  @Output() newplusXLEvent=new EventEmitter<any>();
  @Output() newminusXLEvent=new EventEmitter<any>();
  @Output() changecomponent=new EventEmitter<boolean>();
  hiderow=false;
 hide=false;
 visible=false
 hidechoice=false;
 hidechildopt=false
 hidepersonopt=false
 imagepreview:any;
 viewimage:any;
 ViewImg:any;
 QtM=1;
 QtS=0;
 QtL=0;
 QtXL=0;
 file2:any;
 file3:any;
 file4:any;
 cptcl=0;
 mybodr=1;
 isbord=false
 public serie=false;
 public flex=false;
 public borderie=false;
 public suble=false
 public transft=false
 nbcolorserie:any=1;
 Price:any;
 prix=2500;
 prixtcolor=3500
 pricepolo=3500
 pricetordinary=1500
 pricetpolycoton=1500;
 pricesuble=1000;
 pricetransfert=1000;
 priceAPchild=3600;
 priceSPchild=2600;
 priceAPgrand=5100;
 priceSPgrand=3600;
 cmpt=0
 cpt=0
 cptclikfl=0
 cmpte=0
 cptwithg=0;
 cptwthng=0;
 sgprice=500
 ispricetcolor=false
 ispricepolo=false;
 ispricepolycoton=false
 isprix=false
 ispriceordinary=false
 chanecpt=true
public shirtwhite=false
public shirtcolor=false
public shirtordinary=false
public shirtpolycoton=false
public polo=false
oui=false;
t:any
public sansphotochild=false
public avecphotochild=false
public enfant=false
public grandperson=false
public sansphotogrand=false
public avecphotogrand=false

  ngOnInit(): void {
    //let typed= new Typed('#description', {
   //   strings:["Veuillez!! importer la maquette et le visuel dans le meme fichier.S'il y a un recto verso mettez-les dans le meme fichier que la maquette."],
   //   typeSpeed:140,
   //   showCursor:false,
    //  
    //});


  }

  InputChange(){

 
 if(this.nbcolorserie){
  if(parseInt(this.nbcolorserie)==1 && this.sgprice==1000){
    this.Price = (parseInt(this.Price)-500) 
    this.sgprice=500
  }

  if(parseInt(this.nbcolorserie)==1 && this.sgprice==500){
    if(this.Price==this.prix){
      this.Price=+this.Price + 500
      this.sgprice=500
    }

    if(this.Price==this.prixtcolor){
      this.Price=+this.Price + 500
      this.sgprice=500
    }

    if(this.Price==this.pricepolo){
       this.Price=+this.Price + 500
       this.sgprice=500
  
    }
    if(this.Price==this.pricetpolycoton){
      this.Price=+this.Price + 500
      this.sgprice=500
 
   }
   if(this.Price==this.pricetordinary){
    this.Price=+this.Price + 500
    this.sgprice=500

 }

  }

  if(parseInt(this.nbcolorserie)>1 && this.sgprice==500){
    if(this.isprix){
      if(this.Price==this.prix){
        this.Price=parseInt(this.Price) + 1000
        this.sgprice=1000
      }else {
        this.Price=(parseInt(this.Price)-500) + 1000
        this.sgprice=1000
      }

    }
  
    if(this.ispriceordinary){
      if(this.Price==this.pricetordinary){
        this.Price=parseInt(this.Price) + 1000
        this.sgprice=1000
      }else {
        this.Price=(parseInt(this.Price)-500) + 1000
        this.sgprice=1000
      }
    }
   
    if(this.ispricepolycoton){
      if(this.Price==this.pricetpolycoton){
        this.Price=parseInt(this.Price) + 1000
        this.sgprice=1000
      }else {
        this.Price=(parseInt(this.Price)-500) + 1000
        this.sgprice=1000
      }
    }
   


    if(this.ispricepolo){
      if(this.Price==this.pricepolo){
        this.Price=parseInt(this.Price) + 1000
        this.sgprice=1000
      }else {
        this.Price=(parseInt(this.Price)-500) + 1000
        this.sgprice=1000
      }
    }
  
    if(this.ispricetcolor){
      if(this.Price==this.prixtcolor){
        this.Price=parseInt(this.Price) + 1000
        this.sgprice=1000
      }else {
        this.Price=(parseInt(this.Price)-500) + 1000
        this.sgprice=1000
      }
    }
   


  }

 

 }else{
   if(this.isprix){
    this.Price=this.prix;

   }
   if(this.ispricepolo){
    this.Price=this.pricepolo;

   }
   if(this.ispricetcolor){
    this.Price=this.prixtcolor;

   }

   if(this.ispricepolycoton){
    this.Price=this.pricetpolycoton;

   }

   if(this.ispriceordinary){
    this.Price=this.pricetordinary;

   }

 this.sgprice=500
   console.log(this.nbcolorserie)
 }
   
  
  }

cacher(){

  if(this.transft){
    if(parseInt(this.nbcolorserie)==1 && this.sgprice==500){
      // this.isprix=!this.isprix
      this.Price=(parseInt(this.Price) - this.pricetransfert) + 500;
      this.cmpt=0;
  
   }

  }
  

  if(this.suble)
  {
    if(parseInt(this.nbcolorserie)==1 && this.sgprice==500){
      // this.isprix=!this.isprix
      this.Price=(parseInt(this.Price) - this.pricesuble) + 500;
      this.cmpt=0;
  
   }


   if(parseInt(this.nbcolorserie)>1){
    this.Price=(parseInt(this.Price) - this.pricesuble) + 1000;
    this.cmpt=0;

 }
  }

  if(this.borderie){  

    if(parseInt(this.nbcolorserie)==1){
      this.Price= (+this.Price-((+this.mybodr)*500)) + 500;
      this.cmpt=0;
  
   }

   if(parseInt(this.nbcolorserie)>1 ){
    this.Price= (+this.Price-((+this.mybodr)*500)) +1000;
    this.cmpt=0;

 }

}

  
  if(!this.flex && !this.transft && !this.suble && !this.borderie && !this.serie){
    this.Price= parseInt(this.Price) + 500;

  }

  if(this.flex){
    if(this.isprix){
      if(parseInt(this.nbcolorserie)==1 && this.sgprice==500){
        this.Price=+this.prix+ 500;
        this.cmpt=0;
    
     }

     if(parseInt(this.nbcolorserie)>1){
      this.Price=+this.prix+ 1000;
      this.cmpt=0;
  
   }

      
   
    }


    if(this.ispricetcolor){

      if(parseInt(this.nbcolorserie)==1 && this.sgprice==500){
        this.Price=+this.prixtcolor+ 500;
        this.cmpt=0;
    
     }

     if(parseInt(this.nbcolorserie)>1){
      this.Price= +this.prixtcolor+ 1000;
      this.cmpt=0;
  
   }
  

    }

    if(this.ispricepolo){
      
      if(parseInt(this.nbcolorserie)==1 && this.sgprice==500){
        this.Price= +this.pricepolo+ 500;
        this.cmpt=0;
    
     }

     if(parseInt(this.nbcolorserie)>1){
      this.Price= +this.pricepolo+ 1000;
      this.cmpt=0;
  
   }
     

    }

    if(this.ispricepolycoton){

      if(parseInt(this.nbcolorserie)==1 && this.sgprice==500){
        this.Price= +this.pricetpolycoton + 500;
        this.cmpt=0;
    
     }

     if(parseInt(this.nbcolorserie)>1){
      this.Price= +this.pricetpolycoton+ 1000;
      this.cmpt=0;
  
   }

     

    }
    

    if(this.ispriceordinary){


      if(parseInt(this.nbcolorserie)==1 && this.sgprice==500){
        this.Price= +this.pricetordinary + 500;
        this.cmpt=0;
    
     }

     if(parseInt(this.nbcolorserie)>1){
      this.Price= +this.pricetordinary+ 1000;
      this.cmpt=0;
  
   }
    

    }





  }
 
  this.hidechildopt=false
  this.hidepersonopt=false
  this.hide=true;
  this.isbord=false;
  this.borderie=false
  this.hidechoice=false
  this.flex=false
  this.serie=true
  this.transft=false
    this.suble=false
   }


   viewchildopt(){
  this.cpt=0;
  this.cmpte=0;
  this.cptwithg=0;
  this.cptwthng=0;
  this.avecphotochild=false;
  this.avecphotogrand=false;
  this.sansphotogrand=false;
  this.sansphotochild=false;

    if(this.isprix){

      this.enfant=true
      this.grandperson=false
      this.hidechildopt=true
      this.hidepersonopt=false
      this.Price=this.prix
    }


    if(this.ispricetcolor){

      this.enfant=true
      this.grandperson=false
      this.hidechildopt=true
      this.hidepersonopt=false
      this.Price=this.prixtcolor
    }


    if(this.ispricepolo){

      this.enfant=true
      this.grandperson=false
      this.hidechildopt=true
      this.hidepersonopt=false
      this.Price=this.pricepolo
    }



    if(this.ispricepolycoton){

      this.enfant=true
      this.grandperson=false
      this.hidechildopt=true
      this.hidepersonopt=false
      this.Price=this.pricetpolycoton
    }



    if(this.ispriceordinary){

      this.enfant=true
      this.grandperson=false
      this.hidechildopt=true
      this.hidepersonopt=false
      this.Price=this.pricetordinary
    }

      

   }
   viewpersonopt(){

    this.cpt=0;
    this.cmpte=0;
    this.cptwithg=0;
    this.cptwthng=0;
    this.avecphotochild=false;
    this.avecphotogrand=false;
    this.sansphotogrand=false;
    this.sansphotochild=false;
    if(this.isprix){
      this.enfant=false
      this.grandperson=true
       this.hidepersonopt=true
       this.hidechildopt=false
      this.Price=this.prix
    }

    if(this.ispricetcolor){

      this.enfant=false
      this.grandperson=true
       this.hidepersonopt=true
       this.hidechildopt=false
      this.Price=this.prixtcolor
    }
     if(this.ispriceordinary){

      this.enfant=false
      this.grandperson=true
       this.hidepersonopt=true
       this.hidechildopt=false
      this.Price=this.pricetordinary
    }


    if(this.ispricepolycoton){

      
      this.enfant=false
      this.grandperson=true
       this.hidepersonopt=true
       this.hidechildopt=false
      this.Price=this.pricetpolycoton
    }


    if(this.ispricepolo){
    
      this.enfant=false
      this.grandperson=true
       this.hidepersonopt=true
       this.hidechildopt=false
      this.Price=this.pricepolo
    }


   }

InputChangeB(event:any){
  if(this.mybodr==null){
    this.mybodr=1
    

  }

  if(+this.mybodr>=1){
    if(this.isprix){
      if(this.Price>this.prix){
        this.Price=this.prix + ((+this.mybodr)*500)
      }
  
    }

    if(this.ispriceordinary){
      if(this.Price>this.pricetordinary){
        this.Price=this.pricetordinary + ((+this.mybodr)*500)
      }
  
    }


    if(this.ispricepolycoton){
      if(this.Price>this.pricetpolycoton){
        this.Price=this.pricetpolycoton + ((+this.mybodr)*500)
      }
  
    }

    if(this.ispricepolo){
      if(this.Price>this.pricepolo){
        this.Price=this.pricepolo + ((+this.mybodr)*500)
      }
  
    }

    if(this.ispricetcolor){
      if(this.Price>this.prixtcolor){
        this.Price=this.prixtcolor + ((+this.mybodr)*500)
      }
  
    }
  }
 
}

   cachebrd(){
     if(!this.suble && !this.transft && !this.serie && !this.flex && !this.borderie){
      this.Price=+this.Price + (+this.mybodr)*500
      this.transft=false
      this.suble=false
      this.hidechildopt=false
      this.hidepersonopt=false
       this.hide=false
       this.borderie=true
       this.flex=false
       this.serie=false
       this.hidechoice=false
       this.isbord=true
     }

    if(this.serie){

      if(this.nbcolorserie>1){
        this.Price=(+this.Price -1000)+ ((+this.mybodr)*500)

      }

      if(this.nbcolorserie==1){
        this.Price=(+this.Price -500)+ ((+this.mybodr)*500)

      }
      this.transft=false
      this.suble=false
      this.hidechildopt=false
      this.hidepersonopt=false
       this.hide=false
       this.borderie=true
       this.flex=false
       this.serie=false
       this.hidechoice=false
       this.isbord=true
    }


    if(this.flex){
   if(this.isprix){
     this.Price=+this.prix+ ((+this.mybodr)*500)
    this.transft=false
    this.suble=false
    this.hidechildopt=false
    this.hidepersonopt=false
     this.hide=false
     this.borderie=true
     this.flex=false
     this.serie=false
     this.hidechoice=false
     this.isbord=true
   }


   if(this.ispricetcolor){
    this.Price=+this.prixtcolor+ ((+this.mybodr)*500)
   this.transft=false
   this.suble=false
   this.hidechildopt=false
   this.hidepersonopt=false
    this.hide=false
    this.borderie=true
    this.flex=false
    this.serie=false
    this.hidechoice=false
    this.isbord=true
  }


  if(this.ispricepolycoton){
    this.Price=+this.pricetpolycoton+ ((+this.mybodr)*500)
   this.transft=false
   this.suble=false
   this.hidechildopt=false
   this.hidepersonopt=false
    this.hide=false
    this.borderie=true
    this.flex=false
    this.serie=false
    this.hidechoice=false
    this.isbord=true
  }


  if(this.ispricepolo){
    this.Price=+this.pricepolo+ ((+this.mybodr)*500)
   this.transft=false
   this.suble=false
   this.hidechildopt=false
   this.hidepersonopt=false
    this.hide=false
    this.borderie=true
    this.flex=false
    this.serie=false
    this.hidechoice=false
    this.isbord=true
  }

  if(this.ispriceordinary){
    this.Price=+this.pricetordinary + ((+this.mybodr)*500)
   this.transft=false
   this.suble=false
   this.hidechildopt=false
   this.hidepersonopt=false
    this.hide=false
    this.borderie=true
    this.flex=false
    this.serie=false
    this.hidechoice=false
    this.isbord=true
  }
    }


     if(this.transft){

      this.Price=(+this.Price-this.pricetransfert) + (+this.mybodr)*500
      this.transft=false
      this.suble=false
      this.hidechildopt=false
      this.hidepersonopt=false
       this.hide=false
       this.borderie=true
       this.flex=false
       this.serie=false
       this.hidechoice=false
       this.isbord=true
     }
     if(this.suble){

      this.Price=(+this.Price-this.pricesuble) + (+this.mybodr)*500
      this.transft=false
      this.suble=false
      this.hidechildopt=false
      this.hidepersonopt=false
       this.hide=false
       this.borderie=true
       this.flex=false
       this.serie=false
       this.hidechoice=false
       this.isbord=true

     }


  
     
   }
   cacheflx(){
    if(this.suble){
      this.Price= +this.Price-this.pricesuble;
      this.hide=false
      this.isbord=false;
      this.flex=true
      this.transft=false
      this.suble=false
      this.borderie=false
      this.serie=false
      this.hidechoice=true
      this.hidechildopt=false
      this.hidepersonopt=false
    }

    if(this.serie){
      if(this.nbcolorserie==1){
        this.Price= +this.Price-500;
        this.hide=false
        this.isbord=false;
        this.flex=true
        this.transft=false
        this.suble=false
        this.borderie=false
        this.serie=false
        this.hidechoice=true
        this.hidechildopt=false
        this.hidepersonopt=false
      }

      if(this.nbcolorserie>1){
        this.Price= +this.Price-1000;
        this.hide=false
        this.flex=true
        this.isbord=false;

        this.transft=false
        this.suble=false
        this.borderie=false
        this.serie=false
        this.hidechoice=true
        this.hidechildopt=false
        this.hidepersonopt=false

      }
    }

   if(this.borderie){
    this.Price= +this.Price-((+this.mybodr)*500);
    this.hide=false
    this.flex=true
    this.transft=false
    this.isbord=false;

    this.suble=false
    this.borderie=false
    this.serie=false
    this.hidechoice=true
    this.hidechildopt=false
    this.hidepersonopt=false
   }
    if(this.transft){
      this.Price= +this.Price-this.pricetransfert;
      this.hide=false
      this.flex=true
      this.transft=false
      this.isbord=false;

      this.suble=false
      this.borderie=false
      this.serie=false
      this.hidechoice=true
      this.hidechildopt=false
      this.hidepersonopt=false
    }



   if(!this.serie && !this.flex && !this.borderie && !this.transft && !this.suble){
    this.hide=false
    this.flex=true
    this.transft=false
    this.suble=false
    this.borderie=false
    this.serie=false
    this.hidechoice=true
    this.hidechildopt=false
    this.isbord=false;

    this.hidepersonopt=false
   }


   }
  
   Uplode(event:any){
    this.file2 =event.target.files[0]
    const reader = new FileReader();
  reader.onload = () => {
  
   this.imagepreview = reader.result;
   
   };
   
   reader.readAsDataURL(this.file2);
    console.log(event)
     
   }
  View(){
    this.visible=true;
    this.oui=!this.oui
  }
  view2(){
    this.visible=false
  }
  Uplade(event:any){
    this.file3 =event.target.files[0]
    const reader = new FileReader();
  reader.onload = () => {
  
   this.viewimage= reader.result;
   
   };
   
   reader.readAsDataURL(this.file3);
    console.log(event)
     
   }
   Upladeimage(event:any){
    this.file4 =event.target.files[0]
    const reader = new FileReader();
  reader.onload = () => {
  
   this.ViewImg= reader.result;
   
   };
   
   reader.readAsDataURL(this.file4);
    console.log(event)
     
   }
   //quantite de la taille M
   QtplusM(event:any){
     this.QtM =this.QtM +1;
    console.log(this.QtM)
     
   }
   QtminusM(event:any){
     if(this.QtM>0){
    this.QtM =this.QtM-1
   
     }else{
      this.QtM=+this.QtM
     
     }

   }
   //quantité de la taille S
   QtplusS(event:any){
    this.QtS =+this.QtS+1
    console.log(event)
  }
  QtminusS(event:any){
    if(this.QtS>0){
   this.QtS =+this.QtS-1
    }else{
     this.QtS=+this.QtS
    }
  }
   //quantité de la taille L
   QtplusL(event:any){
    this.QtL =+this.QtL+1
    console.log(event)
  }
  QtminusL(event:any){
    if(this.QtL>0){
   this.QtL =+this.QtL-1
    }else{
     this.QtL=+this.QtL
    }
  }
   //quantité de la taille XL
   QtplusXL(event:any){
    this.QtXL =+this.QtXL+1
    console.log(event)
  }
  QtminusXL(event:any){
    if(this.QtXL>0){
   this.QtXL =+this.QtXL-1
    }else{
     this.QtXL=+this.QtXL
    }
  }

  Addtocart =()=>{
    var cart:any;
    var qtys=this.QtM + this.QtL +this.QtS + this.QtXL
    
    if(this.oui && this.file2!=undefined && this.file4!=undefined){
       cart ={ 
        type_product:"crea",
        t:+this.Price*qtys,
        category:"vetement",
        face1:this.url,
        face2:this.imagepreview,
        f3:this.viewimage,
        f4:this.ViewImg,
       
        size:JSON.stringify({
          m:this.QtM,
          l:this.QtL,
          s:this.QtS,
          xl:this.QtXL,
          crea:true
        }),
        qty:qtys,
        price:this.Price
      }
      if(this.enfant){
        Object.assign(cart, {
          genre:JSON.stringify({
            nome:"Enfant"

          })
        })
      }
      if(this.grandperson){
        Object.assign(cart, {
          genre:JSON.stringify({
            nome:"Grande personne"
            
          })
        })
      }
      if(this.sansphotochild){
        Object.assign(cart, {
          photo:JSON.stringify({
           type_photo:"sans photo" ,
           pricechild:this.priceSPchild
          })
        })
      }
      if(this.avecphotochild){
        Object.assign(cart, {
          photo:JSON.stringify({
           type_photo:"avec photo",
           pricechild:this.priceAPchild 
          })
        })
      }
      if(this.avecphotogrand){
        Object.assign(cart, {
          photo:JSON.stringify({
           type_photo:"avec photo", 
           pricechild:this.priceAPgrand
          })
        })
      }
      if(this.sansphotogrand){
        Object.assign(cart, {
          photo:JSON.stringify({
           type_photo:"avec photo", 
           pricechild:this.priceSPgrand
          })
        })
      }
      if(this.serie){
        Object.assign(cart, {type:JSON.stringify({
          type:"Serigraphie",
          nbcolor:this.nbcolorserie
        })})
      }
      if(this.borderie){
        Object.assign(cart, {type:JSON.stringify({
          type:"Borderie",
          nbcolor:this.nbcolorserie
          
        })})
       
      }
      if(this.flex){
        Object.assign(cart, {type:JSON.stringify({
          type:"Flexographie",
           
        })})
      }
      if(this.suble){
        Object.assign(cart, {type:JSON.stringify({
          type:"Sublimation",
         prix:1000,

        })})
      }
      if(this.transft){
        Object.assign(cart, {type:JSON.stringify({
          type:"Transfert",
         prix:1000
        })})
      }
      //type tee-shirt
      if(this.shirtwhite){
        Object.assign(cart,{type_shirt:JSON.stringify({
          name:"t-hirt blanc",
          prix:this.prix
        })})
      }
      if(this.shirtcolor){
        Object.assign(cart,{type_shirt:JSON.stringify({
          name:"1/40 t-hirt couleur",
          prix:this.prix
        })})
      }
      if(this.polo){
        Object.assign(cart,{type_shirt:JSON.stringify({
          name:"1/40 Polo blanc",
          prix:this.pricepolo
        })})
      }
      if(this.shirtordinary){
        Object.assign(cart,{type_shirt:JSON.stringify({
          name:"t-shirt ordinaire",
          prix:this.pricetordinary
        })})
      }
      if(this.shirtpolycoton){
        Object.assign(cart,{type_shirt:JSON.stringify({
          name:"t-shirt polycoton",
          prix:this.pricetpolycoton
        })})
      }
  
      //this.localservice.
      console.log(qtys);
    }else{
      console.log(this.oui,this.serie)

       cart ={
        type_product:"crea",
        category:"vetement",

         t:+this.Price * qtys,

        face1:this.url,
        face2:null,
        f3:this.viewimage,
        f4:null,
        
        size:JSON.stringify({
          m:this.QtM,
          l:this.QtL,
          s:this.QtS,
          xl:this.QtXL
        }),
        qty:qtys,
        price:this.Price
      }
      if(this.enfant){
        Object.assign(cart, {
          genre:JSON.stringify({
            nome:"Enfant"

          })
        })
      }
      if(this.grandperson){
        Object.assign(cart, {
          genre:JSON.stringify({
            nome:"Grande personne"
            
          })
        })
      }
      if(this.sansphotochild){
        Object.assign(cart, {
          photo:JSON.stringify({
           type_photo:"sans photo" ,
           pricechild:this.priceSPchild
          })
        })
      }
      if(this.avecphotochild){
        Object.assign(cart, {
          photo:JSON.stringify({
           type_photo:"avec photo", 
           pricechild:this.priceAPchild
          })
        })
      }
      if(this.avecphotogrand){
        Object.assign(cart, {
          photo:JSON.stringify({
           type_photo:"avec photo", 
           pricechild:this.priceAPgrand
          })
        })
      }
      if(this.sansphotogrand){
        Object.assign(cart, {
          photo:JSON.stringify({
           type_photo:"avec photo", 
           pricechild:this.priceSPgrand
          })
        })
      }
      if(this.serie){
        Object.assign(cart, {type:JSON.stringify({
          type:"Serigraphie",
          nbcolor:this.nbcolorserie
        })})
       
     }
     if(this.borderie){
      Object.assign(cart, {type:JSON.stringify({
        type:"Borderie",
        nbcolor:this.nbcolorserie,
       
      })} )
    }
    if(this.flex){
      Object.assign(cart, {type:JSON.stringify({
        type:"Flexographie",
        
      })})
      
    }
    if(this.suble){
      Object.assign(cart, {type:JSON.stringify({
        type:"Sublimation",
        prix:this.pricesuble
      })})
    }
    if(this.transft){
      Object.assign(cart, {type:JSON.stringify({
        type:"Transfert",
        prix:this.pricetransfert
      })})
    }
    if(this.shirtwhite){
      Object.assign(cart,{type_shirt:JSON.stringify({
        name:"1/40 t-hirt blanc",
        prix:this.prix
      })})
    }
    if(this.shirtcolor){
      Object.assign(cart,{type_shirt:JSON.stringify({
        name:"1/40 t-hirt couleur",
        prix:this.prixtcolor
      })})
    }
    if(this.polo){
      Object.assign(cart,{type_shirt:JSON.stringify({
        name:"1/40 Polo blanc",
        prix:this.pricepolo
      })})
    }
    if(this.shirtordinary){
      Object.assign(cart,{type_shirt:JSON.stringify({
        name:"t-shirt ordinaire",
        prix:this.pricetordinary
      })})
    }
    if(this.shirtpolycoton){
      Object.assign(cart,{type_shirt:JSON.stringify({
        name:"t-shirt polycoton",
        prix:this.pricetpolycoton
      })})
    }

      }
      
      //this.localservice.
      console.log(cart)
     
    
    try{
     if(this.shirtwhite || this.shirtcolor || this.shirtordinary || this.shirtpolycoton || this.polo && this.file3!=undefined){
      this.localservice.adtocart(cart);
       location.href="cart/"
      
     }else{
      //Swal//.fire({
      //  text: "La transaction a echoué",
      //   button: "Ok"
      // // })
     }
       
    
    }catch(e:any){
      console.log(e)
    }
    //location.reload()
  }

 //prix fix
 affprice(event:any){
    this.hiderow=true 
   this.Price=this.prix
   this.shirtwhite=true
   this.shirtcolor=false
   this.polo=false
   this.shirtordinary=false
   this.shirtpolycoton=false
   this.ispricetcolor=false;
   this.isprix=true;
    this.ispricepolycoton=false;
    this.ispriceordinary=false;
    this.ispricepolo=false
    if(this.borderie){
      this.Price=+this.Price+((+this.mybodr)*500)
    }
    if(this.flex){
      if(this.avecphotochild){
        this.Price=parseInt(this.Price)+this.priceAPchild
      }

      if(this.avecphotogrand){
        this.Price=parseInt(this.Price)+this.priceAPgrand
      }


      if(this.sansphotochild){
        this.Price=parseInt(this.Price)+this.priceSPchild
      }

      if(this.sansphotogrand){
        this.Price=parseInt(this.Price)+this.priceSPgrand
      }
    }

   
    if(this.serie){
      // this.isprix=!this.isprix
      if(parseInt(this.nbcolorserie)>1){
        this.Price=+this.Price + 1000;

      }
      if(parseInt(this.nbcolorserie)==1){
        // this.isprix=!this.isprix
         this.Price=+this.Price + 500;
   
     }

  
   }

   if(this.suble){
    this.Price=+this.Price+this.pricesuble
  }

   if(this.transft){
    this.Price=this.Price+this.pricetransfert
  }
   
 }
 showpricetcolor(event:any){

  this.hiderow=true 
   this.Price=this.prixtcolor
   this.shirtwhite=false
   this.shirtcolor=true
   this.polo=false
   this.shirtordinary=false
   this.shirtpolycoton=false

    this.ispricetcolor=true;
    this.isprix=false;
    this.ispricepolycoton=false;
    this.ispriceordinary=false;
    this.ispricepolo=false

    if(this.flex){
      if(this.avecphotochild){
        this.Price=parseInt(this.Price)+this.priceAPchild
      }

      if(this.avecphotogrand){
        this.Price=parseInt(this.Price)+this.priceAPgrand
      }


      if(this.sansphotochild){
        this.Price=parseInt(this.Price)+this.priceSPchild
      }

      if(this.sansphotogrand){
        this.Price=parseInt(this.Price)+this.priceSPgrand
      }
    }

    if(this.serie){
      // this.isprix=!this.isprix
      if(parseInt(this.nbcolorserie)>1){
        this.Price=+this.Price + 1000;

      }
      if(parseInt(this.nbcolorserie)==1){
        // this.isprix=!this.isprix
         this.Price=+this.Price + 500;
   
     }

  
   }


 if(this.suble){
  this.Price=+this.Price+this.pricesuble
}

if(this.borderie){
  this.Price=+this.Price+((+this.mybodr)*500)
}
 if(this.transft){
  this.Price=this.Price+this.pricetransfert
}
  
 }
 showpricepolo(event:any){
  this.hiderow=true 
  this.Price=this.pricepolo 
  this.polo=true
  this.shirtordinary=false
  this.shirtpolycoton=false
  this.shirtwhite=false
  this.shirtcolor=false
    this.ispricetcolor=false;
    this.isprix=false;
    this.ispricepolycoton=false;
    this.ispriceordinary=false;
    this.ispricepolo=true
    if(parseInt(this.nbcolorserie)==1 && this.serie){
      // this.isprix=!this.isprix
       this.Price=+this.Price + 500;
 
   }

   if(this.borderie){
    this.Price=+this.Price+((+this.mybodr)*500)
  }

   if(this.flex){
    if(this.avecphotochild){
      this.Price=parseInt(this.Price)+this.priceAPchild
    }

    if(this.avecphotogrand){
      this.Price=parseInt(this.Price)+this.priceAPgrand
    }


    if(this.sansphotochild){
      this.Price=parseInt(this.Price)+this.priceSPchild
    }

    if(this.sansphotogrand){
      this.Price=parseInt(this.Price)+this.priceSPgrand
    }
  }

   if(parseInt(this.nbcolorserie)>1 &&  this.serie){
    // this.isprix=!this.isprix
     this.Price=+this.Price + 1000;


 }

 if(this.suble){
  this.Price=+this.Price+this.pricesuble
}
 if(this.transft){
  this.Price=this.Price+this.pricetransfert
}
   
 }
 showpricetordi(event:any){
  this.hiderow=true 
  this.Price=this.pricetordinary
  this.polo=true
  this.shirtordinary=true
  this.shirtpolycoton=false
  this.shirtwhite=false
  this.shirtcolor=false
  this.ispricetcolor=false;
  this.isprix=false;
  this.ispricepolycoton=false;
  this.ispriceordinary=true;
  this.ispricepolo=false

  if(this.borderie){
    this.Price=+this.Price+((+this.mybodr)*500)
  }
   

  if(this.serie){
    // this.isprix=!this.isprix
    if(parseInt(this.nbcolorserie)>1){
      this.Price=+this.Price + 1000;

    }
    if(parseInt(this.nbcolorserie)==1){
      // this.isprix=!this.isprix
       this.Price=+this.Price + 500;
 
   }


 }



   if(this.suble){
     this.Price=+this.Price+this.pricesuble
   }

   if(this.flex){
    if(this.avecphotochild){
      this.Price=parseInt(this.Price)+this.priceAPchild
    }

    if(this.avecphotogrand){
      this.Price=parseInt(this.Price)+this.priceAPgrand
    }


    if(this.sansphotochild){
      this.Price=parseInt(this.Price)+this.priceSPchild
    }

    if(this.sansphotogrand){
      this.Price=parseInt(this.Price)+this.priceSPgrand
    }
  }


 if(this.transft){
  this.Price=this.Price+this.pricetransfert
}

   
 }


 showpricecoton(event:any){

  this.hiderow=true 
  this.Price=this.pricetpolycoton
  this.polo=true
  this.shirtordinary=false
  this.shirtpolycoton=true
  this.shirtwhite=false
  this.shirtcolor=false
    this.ispricetcolor=false;
    this.isprix=false;
    this.ispricepolycoton=true;
    this.ispriceordinary=false;
    this.ispricepolo=false;


    if(this.suble){
      this.Price=+this.Price+this.pricesuble;
    }


    if(this.borderie){
      this.Price=+this.Price+((+this.mybodr)*500)
    }

    if(this.flex){
      if(this.avecphotochild){
        this.Price=parseInt(this.Price)+this.priceAPchild;
      }

      if(this.avecphotogrand){
        this.Price=parseInt(this.Price)+this.priceAPgrand;
      }


      if(this.sansphotochild){
        this.Price=parseInt(this.Price)+this.priceSPchild;
      }

      if(this.sansphotogrand){
        this.Price=parseInt(this.Price)+this.priceSPgrand;
      }
    }
    

   


   if(this.transft){
     this.Price=this.Price+this.pricetransfert
   }



   if(this.serie){
    if(parseInt(this.nbcolorserie)>1){
      this.Price=+this.Price + 1000;

    }
    if(parseInt(this.nbcolorserie)==1){
       this.Price=+this.Price + 500;
 
   }


 }


   
 }
 //prix de choix avec photo ou sans
 showpriceAPchild(event:any){
  
   if(this.cmpte==0 && !this.avecphotochild){
    this.cmpte=this.cmpte+1
    this.avecphotochild=true
    if(this.sansphotochild){
      this.Price= (parseInt(this.Price)-this.priceSPchild) + this.priceAPchild 
      this.sansphotochild=false
    }else{

      this.Price= parseInt(this.Price) + this.priceAPchild 
     }
   }

   if(this.cpt>=1&& this.sansphotochild){
    this.cmpte=this.cmpte+1
     this.avecphotochild=true
     this.Price= (parseInt(this.Price)-this.priceSPchild) + this.priceAPchild
     this.sansphotochild=false
     
  }

 
 }

 showpriceSPchild(event:any){
   if(this.cpt==0 && !this.sansphotochild){
      this.cpt=this.cpt+1
      this.sansphotochild=true
      if(this.avecphotochild){
        this.Price= (parseInt(this.Price)-this.priceAPchild) + this.priceSPchild 
        this.avecphotochild=false
      }else{
  
        this.Price= parseInt(this.Price) + this.priceSPchild ;
       }
    
   }

   if(this.cmpte>=1&&this.avecphotochild){
    this.cpt=this.cpt+1;
     this.sansphotochild=true
     this.Price= (parseInt(this.Price)-this.priceAPchild) + this.priceSPchild
     this.avecphotochild=false
     
  }
  
 }


 //prix de choix avec photo ou sans
 showpriceAPGperson(event:any){
   if(this.cptwithg==0 && !this.avecphotogrand){
     this.cptwithg=this.cptwithg+1
     this.avecphotogrand=true
    if(this.sansphotogrand){
      this.Price= (parseInt(this.Price)-this.priceSPgrand) + this.priceAPgrand;
      this.sansphotogrand=false;
     }else{

      this.Price= parseInt(this.Price)+ this.priceAPgrand;
     }
   }

   if(this.cptwthng>=1&& this.sansphotogrand){
    this.cptwithg= this.cptwithg+1;
     this.avecphotogrand=true;
     this.Price= (parseInt(this.Price)-this.priceSPgrand) + this.priceAPgrand;
     this.sansphotogrand=false;   
  }

 }


 showpriceSPGperson(event:any){
   if(this.cptwthng==0 && !this.sansphotogrand){
    this.cptwthng=this.cptwthng+1
    this.sansphotogrand=true
    if(this.avecphotogrand){
     this.Price= (+this.Price-this.priceAPgrand) + this.priceSPgrand
     this.avecphotogrand=false
    }else{
     this.Price=parseInt(this.Price) + this.priceSPgrand
    }
   
   }
 
   if(this.cptwithg>=1 && this.avecphotogrand){
    this.cptwthng=this.cptwthng+1;
    this.sansphotogrand=true
    this.Price= (parseInt(this.Price)-this.priceAPgrand) + this.priceSPgrand
     this.avecphotogrand=false
   
   
   }
 
 }


 //type dimpression
showtransfert(){

    if(!this.flex && !this.serie && !this.borderie && !this.suble && !this.transft){ 
      this.Price=this.Price + this.pricetransfert    
      this.transft=true
      this.suble=false
      this.hide=false
      this.serie=false
      this.isbord=false
      this.hidechoice=false
      this.flex=false
      this.borderie=false
      this.hidechildopt=false
      this.hidepersonopt=false


    }
    

    if(this.suble){
      if(this.isprix){
        this.Price= +this.prix + this.pricetransfert    
        this.transft=true
        this.suble=false
        this.hide=false
        this.isbord=false

        this.serie=false
        this.hidechoice=false
        this.flex=false
        this.borderie=false
        this.hidechildopt=false
       this.hidepersonopt=false
      }
  

      if(this.ispricetcolor){
        this.Price=+this.prixtcolor + this.pricetransfert    
        this.transft=true
        this.suble=false
        this.hide=false
        this.serie=false
        this.hidechoice=false
        this.isbord=false

        this.flex=false
        this.borderie=false
        this.hidechildopt=false
        this.hidepersonopt=false

      }

      if(this.ispricepolo){
        this.Price=+this.pricepolo + this.pricetransfert    
        this.transft=true
        this.suble=false
        this.hide=false
        this.serie=false
        this.isbord=false

        this.hidechoice=false
        this.flex=false
        this.borderie=false
        this.hidechildopt=false
        this.hidepersonopt=false

      }

      if(this.ispricepolycoton){
        this.Price= +this.pricetpolycoton + this.pricetransfert    
        this.transft=true
        this.suble=false
        this.hide=false
        this.serie=false
        this.hidechoice=false
        this.isbord=false

        this.flex=false
        this.borderie=false
        this.hidechildopt=false
       this.hidepersonopt=false

      }
      

      if(this.ispriceordinary){
        this.Price=+this.pricetordinary + this.pricetransfert;
        this.transft=true
        this.suble=false
        this.hide=false
        this.serie=false
        this.isbord=false

        this.hidechoice=false
        this.flex=false
        this.borderie=false 
        this.hidechildopt=false
         this.hidepersonopt=false

      }


    }
    
    if(this.flex){
      if(this.isprix){
        this.Price= +this.prix + this.pricetransfert    
        this.transft=true
        this.suble=false
        this.hide=false
        this.serie=false
        this.hidechoice=false
        this.flex=false
        this.borderie=false
        this.hidechildopt=false
       this.hidepersonopt=false
       this.isbord=false

      }
  

      if(this.ispricetcolor){
        this.Price=+this.prixtcolor + this.pricetransfert    
        this.transft=true
        this.suble=false
        this.hide=false
        this.serie=false
        this.hidechoice=false
        this.flex=false
        this.borderie=false
        this.hidechildopt=false
        this.hidepersonopt=false
        this.isbord=false


      }

      if(this.ispricepolo){
        this.Price=+this.pricepolo + this.pricetransfert    
        this.transft=true
        this.suble=false
        this.hide=false
        this.serie=false
        this.hidechoice=false
        this.flex=false
        this.borderie=false
        this.hidechildopt=false
        this.hidepersonopt=false
        this.isbord=false


      }

      if(this.ispricepolycoton){
        this.Price= +this.pricetpolycoton + this.pricetransfert    
        this.transft=true
        this.suble=false
        this.hide=false
        this.serie=false
        this.hidechoice=false
        this.flex=false
        this.borderie=false
        this.hidechildopt=false
       this.hidepersonopt=false
       this.isbord=false


      }  

      if(this.ispriceordinary){
        this.Price=+this.pricetordinary + this.pricetransfert;
        this.transft=true
        this.suble=false
        this.hide=false
        this.serie=false
        this.hidechoice=false
        this.flex=false
        this.borderie=false 
        this.hidechildopt=false
         this.hidepersonopt=false
         this.isbord=false


      }

    }

  

   if(this.serie){
      if(this.nbcolorserie>1){
        this.Price= (parseInt(this.Price)-1000) + this.pricetransfert; 
        this.transft=true;
        this.suble=false;
        this.hide=false;
        this.serie=false;
        this.hidechoice=false;
        this.flex=false;
        this.borderie=false;
        this.isbord=false

      }

      if(this.nbcolorserie==1){
        this.Price= (parseInt(this.Price)-500) + this.pricetransfert;
        this.transft=true;
        this.suble=false;
        this.hide=false;
        this.serie=false;
        this.hidechoice=false;
        this.flex=false;
        this.isbord=false
        this.borderie=false;
      }
    
    }
    
    if(this.borderie){
      this.Price= (parseInt(this.Price)-((+this.mybodr)*500)) + this.pricetransfert;
        this.transft=true;
        this.suble=false;
        this.hide=false;
        this.serie=false;
        this.hidechoice=false;
        this.flex=false;
        this.isbord=false
        this.borderie=false;

    }

  
}


ChangeComponent(value:boolean){
  this.changecomponent.emit(value)

}

showsublime(){

  if(this.ispriceordinary){
    this.Price=+this.pricetordinary + this.pricesuble
    this.transft=false
    this.suble=true
    this.hidechoice=false
    this.serie=false
    this.flex=false,
    this.hide=false
    this.borderie=false
    this.hidepersonopt=false;
    this.hidechildopt=false;
    this.isbord=false

  }


  if(this.ispricepolo){
    this.Price=+this.pricepolo+ this.pricesuble
    this.transft=false
    this.suble=true
    this.hidechoice=false
    this.serie=false
    this.flex=false,
    this.hide=false
    this.borderie=false
    this.hidepersonopt=false;
    this.hidechildopt=false;
    this.isbord=false

  }


  if(this.isprix){
    this.Price=+this.prix+ this.pricesuble
    this.transft=false
    this.suble=true
    this.hidechoice=false
    this.serie=false
    this.flex=false,
    this.hide=false
    this.borderie=false
    this.hidepersonopt=false;
    this.hidechildopt=false;
    this.isbord=false

  }



  if(this.ispricetcolor){
    this.Price=+this.prixtcolor+ this.pricesuble
    this.transft=false
    this.suble=true
    this.hidechoice=false
    this.serie=false
    this.flex=false,
    this.hide=false
    this.borderie=false
    this.hidepersonopt=false;
    this.hidechildopt=false;
    this.isbord=false

  }


  if(this.ispricepolycoton){
    this.Price=+this.pricetpolycoton+ this.pricesuble
    this.transft=false
    this.suble=true
    this.hidechoice=false
    this.serie=false
    this.flex=false,
    this.hide=false
    this.borderie=false
    this.hidepersonopt=false;
    this.hidechildopt=false;
    this.isbord=false

  }
  
}

}
