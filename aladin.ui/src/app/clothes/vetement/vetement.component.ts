import { createAttribute } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { charAtIndex } from 'pdf-lib';
import { ListService ,LocalService} from 'src/app/core';
declare  var require:any;
var myalert=require('sweetalert2');
var $ = require("jquery");
@Component({
  selector: 'app-vetement',
  templateUrl: './vetement.component.html',
  styleUrls: ['./vetement.component.scss']
})
export class VetementComponent implements OnInit {
 cltobj:any=[]
 file:any
 imagepreview:any
 CacheCrea=false
 cacheClothes=true;
 details:any={};
 showdetail=false;

 url="/editor/cloth/"
 obj:any=[]
 cart=false;
 data="Vêtement : Aladin vous propose des vêtements personnalisés de très bonne qualité avec des finitions qui respectent vos goûts. Vous serez fière de les porter partout quel que soit le lieu et l’événement."
  constructor(private p:ListService,private local:LocalService) { }

  ngOnInit(): void {
    //$("body").children().first().before($(".modal"));


    this.p.getcloths().subscribe(
      res=>{
      this.cltobj=res;
      console.log(this.cltobj)
      this.cltobj=this.cltobj.data;
      for(let item of this.cltobj){
        let id= item.clt_id;
        for(let i =1;i<this.cltobj.length;i++){
          if(id==this.cltobj[i].clt_id){
            this.obj.push({id:id})
            
            
          }
        }
          
        

      }
      
      
        },
      err=>{
        console.log(err)

      }

    )
    
  }

 Upload(event:any){
   let file =event.target.files[0]
   const reader = new FileReader();
 reader.onload = () => {
 
  this.imagepreview = reader.result;
  
  };
  
  reader.readAsDataURL(file);
  this.affichecrea()
  //let modal=document.getElementById('modalbtn')
  //this.p.triggerMouse(modal)
   console.log(event)

 }


showDetails(data:any){
  console.log(data)

  Object.assign(this.details,{url:data.lib_img,comment:data.comment,price:data.price,size:JSON.parse(data.size),type:JSON.parse(data.type_imp),name:data.name_cloths,show:true,back:data.lib_back})
  console.log(this.details)
  this.CacheCrea=false
  this.cacheClothes=false;

}

affichecrea(){
  this.CacheCrea=true
  this.cacheClothes=false
}


getComponent(value:boolean){
  this.cacheClothes=value;
  this.CacheCrea=!this.CacheCrea;


}

ChangeComponent(value:boolean){
  this.cacheClothes=value
  
}

}
