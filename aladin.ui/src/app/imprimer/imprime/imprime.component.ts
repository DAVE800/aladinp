import { Component, OnInit } from '@angular/core';
import { ListService } from 'src/app/core';
@Component({
  selector: 'app-imprime',
  templateUrl: './imprime.component.html',
  styleUrls: ['./imprime.component.scss']
})
export class ImprimeComponent implements OnInit {
  printed:any;
  cacheprinted=true
  cacheimport=false
  url='/editor/prints/'
  imagepreview :any
  constructor(private l :ListService) { }

  ngOnInit(): void {
    this.l.getPrints().subscribe(
      res=>{
        this.printed=res
      },
      error=>{
        console.log(error);
      }
    )
  }
  showcacheimport(){
    this.cacheprinted=false
    this.cacheimport=true
  }
  View(){
    let view = document.getElementById('view');
    view?.scrollIntoView({behavior:"smooth"})
   }

   Upload(event:any){
    let file =event.target.files[0]
    const reader = new FileReader();
     reader.onload = () => {
  
   this.imagepreview = reader.result;
   
   };
   
   reader.readAsDataURL(file);
   //this.showimportcrea()
   this.showcacheimport()
    console.log(file)
 
  }
}
