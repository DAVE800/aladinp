import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConditionuComponent } from './conditionu.component';

describe('ConditionuComponent', () => {
  let component: ConditionuComponent;
  let fixture: ComponentFixture<ConditionuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConditionuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConditionuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
