import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConditionuComponent } from './conditionu/conditionu.component';
import { SharedModule } from '../shared';



@NgModule({
  declarations: [
    ConditionuComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class CUModule { }
